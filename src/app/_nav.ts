interface NavAttributes {
    [propName: string]: any;
  }
  interface NavWrapper {
    attributes: NavAttributes;
    element: string;
  }
  interface NavBadge {
    text: string;
    variant: string;
  }
  interface NavLabel {
    class?: string;
    variant: string;
  }
  
  export interface NavData {
    name?: string;
    url?: string;
    icon?: string;
    badge?: NavBadge;
    title?: boolean;
    children?: NavData[];
    variant?: string;
    attributes?: NavAttributes;
    divider?: boolean;
    class?: string;
    label?: NavLabel;
    wrapper?: NavWrapper;
  }
  
  export const navItems: NavData[] = [
    {
      name: 'Home',
      url: '/panel/home',
      icon: 'icon-speedometer'
    },
    {
      title: true,
      name: 'Administrador'
    },
    {
      name: 'Usuarios',
      url: '/panel/usuarios',
      icon: 'icon-user'
    },
    {
      title: true,
      name: 'Empresa'
    },
    {
      name: 'Clientes',
      url: '/panel/clientes',
      icon: 'icon-people',
      children: [
        {
          name: 'Lista de clientes',
          url: '/panel/clientes/list',
          icon: 'icon-people'
        },
        {
          name: 'Recibos cliente',
          url: '/panel/clientes/recibos',
          icon: 'icon-people'
        },
        {
          name: 'Cuenta Corriente',
          url: '/panel/clientes/cuentaCorriente',
          icon: 'cui-dollar'
        },
        {
          name: 'Cumpleaños',
          url: '/panel/clientes/avisos',
          icon: 'cui-calendar'
        },
        {
          name: 'Historias Clínicas',
          url: '/panel/clientes/historias',
          icon: 'fa fa-address-book-o'
        },
      ]
    },
    {
      name: 'Turnos',
      url: '/panel/turnos',
      icon: 'fa fa-file-text',
      children: [
        {
          name: 'Listado de turnos',
          url: '/panel/turnos/list',
          icon: 'fa fa-file-text'
        },
        {
          name: 'Calendario',
          url: '/panel/turnos/calendario',
          icon: 'cui-calendar'
        },
        {
          name: 'Historial por día',
          url: '/panel/turnos/historial',
          icon: 'fa fa-file-text'
        },
      ]
    },
    {
      name: 'Productos',
      url: '/panel/productos',
      icon: 'icon-layers',
      children: [
        {
          name: 'Lista de Productos',
          url: '/panel/productos/list',
          icon: 'icon-layers',
        },
        {
          name: 'Gestión Stock',
          url: '/panel/productos/stock',
          icon: 'icon-layers'
        },
        {
          name: 'Mov. de Stock',
          url: '/panel/productos/historial',
          icon: 'icon-layers'
        },
        {
          name: 'Actualizar Precios',
          url: '/panel/productos/precios',
          icon: 'icon-layers'
        }

      ]
    },
    {
      name: 'Ventas',
      url: '/panel/ventas/list',
      icon: 'icon-basket-loaded'
    },
    {
      name: 'Pedidos',
      url: '/panel/pedidos',
      icon: 'icon-basket-loaded'
    },
    {
      name: 'Proveedores',
      url: '/panel/proveedores',
      icon: 'fa fa-truck'
    },
    {
      name: 'Tareas',
      url: '/panel/tareas',
      icon: 'fa fa-book'
    },
    {
      name: 'Feriados',
      url: '/panel/feriados/list',
      icon: 'cui-calendar '
    },
    {
      name: 'Salas',
      url: '/panel/salas/list',
      icon: 'cui-home' 
    },
    {
      name: 'Fichas Médicas',
      url: '/panel/fichas/list',
      icon: 'fa fa-plus-circle' 
    },
  
    {
      name: 'Tratamientos',
      url: '/panel/tratamientos',
      icon: 'fa fa-folder'
    },
    {
      name: 'Estadisticas',
      url: '/panel/estadisticas',
      icon: 'fa fa-bar-chart',
      children: [
        {
          name: 'Ventas(CMV)',
          url: '/panel/estadisticas/ventas',
          icon: 'fa fa-bar-chart'
        },
        {
          name: 'Activas/Bajas',
          url: '/panel/estadisticas/activasBajas',
          icon: 'fa fa-bar-chart'
        },
        {
          name: 'Turnos',
          url: '/panel/estadisticas/turnos',
          icon: 'fa fa-bar-chart'
        }
      ]
    },
    {
      name: 'Contable',
      url: '/panel/contable',
      icon: 'cui-dollar',
      children: [
        {
          name: 'Caja',
          url: '/panel/contable/personal',
          icon: 'cui-dollar',
        },
        {
          name: 'Caja Maestra',
          url: '/panel/contable/maestro',
          icon: 'cui-dollar'
        },
        {
          name: 'Registros de Cajas',
          url: '/panel/contable/historial',
          icon: 'cui-dollar'
        },
        {
          name: 'Liquidaciones',
          url: '/panel/contable/liquidaciones',
          icon: 'cui-dollar'
        },
      ]
    },
    {
      name: 'Configuraciones',
      url: '/panel/configuraciones',
      icon: 'fa fa-cog'
    }
  ];
  