import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfipWSComponent } from './afip-ws.component';

describe('AfipWSComponent', () => {
  let component: AfipWSComponent;
  let fixture: ComponentFixture<AfipWSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfipWSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfipWSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
