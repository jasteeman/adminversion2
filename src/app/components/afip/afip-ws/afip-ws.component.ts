import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from 'rxjs/operators';
import { ConfiguracionService } from 'src/app/services/configuracion.service';

@Component({
  selector: 'app-afip-ws',
  templateUrl: './afip-ws.component.html',
  styleUrls: ['./afip-ws.component.scss']
})
export class AfipWSComponent implements OnInit {

  //GENERALES
  loading = false;
  listaConfigs = [];
  configId = null;
  load = false;
  default=false;
  abrirFormAfip = false;
  afipWS = {
    certificadoPath: null,
    clave: null,
    completado: null,
    destDN: "CN=cyc, O=FLORES PATRICIO RAMON, C=AR, SERIALNUMBER=CUIT 20268290000",
    nombre: null,
    signer: null,
    ticketTime: 36000,
    tipo: null,
    url: null,
    urlServicios: null,
    configuracion_id: {id:123,testingWS:true}
  }

  constructor(private configuracionService: ConfiguracionService, private modalService: BsModalService, private toastr: ToastrService) { }

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;

  ngOnInit() {
    this.loadConfiguraciones();
    this.onOptionsSelected("0")
  }

  onOptionsSelected(value){
    if(value=="0"){
      this.afipWS.tipo=0;
      this.afipWS.url="https://wsaahomo.afip.gov.ar/ws/services/LoginCms";
      this.afipWS.urlServicios="https://wswhomo.afip.gov.ar/wsfev1/service.asmx?wsdl"
    }else{
      this.afipWS.tipo=1;
      this.afipWS.url="https://wsaa.afip.gov.ar/ws/services/LoginCms";
      this.afipWS.urlServicios="https://servicios1.afip.gov.ar/wsfev1/service.asmx?WSDL"
    }
}

  //1.1---Traer lista
  loadConfiguraciones() {
    this.loading = true;
    this.configuracionService.getConfiguraciones().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.loading = false;
        console.log(data)
        this.listaConfigs = data.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  openModal(template: TemplateRef<any>, data) {
    this.configId = data;
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalCreate() {
    this.abrirFormAfip = true;
  }
  cerrarForm() {
    this.abrirFormAfip = false;
  }
  create(){
    this.load=true;
    this.configuracionService.saveOrUpdate(this.afipWS).subscribe(data=>{
      if(data.type=="OK"){
      console.log(data)
      this.load=false;
      this.abrirFormAfip=false;
      this.toastr.success("Configuración creada con éxito")
      }
    })
  }
}
