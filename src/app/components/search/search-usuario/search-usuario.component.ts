import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { UsuarioService } from 'src/app/services/usuario.service';
import { map, debounceTime, distinctUntilChanged, switchMap, tap, first } from "rxjs/operators";
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-search-usuario',
  templateUrl: './search-usuario.component.html',
  styleUrls: ['./search-usuario.component.scss']
})
export class SearchUsuarioComponent implements OnInit {

  @Output() myClick = new EventEmitter();
  
  // Variables
  asyncSelected: string;
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean;
  dataSource: Observable<any>;
  public clienteSearch:any;
  
  model: any;
  searching = false;
  searchFailed = false;

  constructor( private usuarioService: UsuarioService,private route: Router) {
    // Typehead
 this.dataSource = Observable.create((observer: any) => {
  // Runs on every search
  observer.next(this.asyncSelected);
  }).pipe(
    switchMap((token: string) => {  
      return this.usuarioService.search(token).pipe(r => r);              
  }));
   }

  ngOnInit() {
  }

  //TypeHead
search = (text$: Observable<any>) =>
text$.pipe(
debounceTime(300),
distinctUntilChanged(),
tap(() => this.searching = true,
switchMap(term =>
this.usuarioService.search(term),
      tap(() => this.searchFailed = false)
      ),),
  tap(() => this.searching = false)
)
changeTypeaheadLoading(e: boolean): void {
this.typeaheadLoading = e;
}

changeTypeaheadNoResults(e: boolean): void {
this.typeaheadNoResults = e;
}

public typeaheadOnSelect(e: TypeaheadMatch): void {
  this.myClick.emit(e.item.id);
  this.asyncSelected="";
  // this.route.navigate(['/panel/usuarios/details/'+e.item.id]);
}

}
