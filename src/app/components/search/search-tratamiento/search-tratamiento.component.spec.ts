import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTratamientoComponent } from './search-tratamiento.component';

describe('SearchTratamientoComponent', () => {
  let component: SearchTratamientoComponent;
  let fixture: ComponentFixture<SearchTratamientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTratamientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTratamientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
