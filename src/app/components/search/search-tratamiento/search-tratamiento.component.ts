import { Component, OnInit,EventEmitter ,Output, Input } from '@angular/core';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { TratamientosService } from 'src/app/services/tratamientos.service';
import { map, debounceTime, distinctUntilChanged, switchMap, tap, first } from "rxjs/operators";
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-search-tratamiento',
  templateUrl: './search-tratamiento.component.html',
  styleUrls: ['./search-tratamiento.component.scss']
})
export class SearchTratamientoComponent implements OnInit {
  @Output() myClick = new EventEmitter();
  estado:boolean = false;
  // Para crear el search- ng generate component search-cliente
 //Typehead
 asyncSelected: string;
 typeaheadLoading: boolean;
 typeaheadNoResults: boolean;
 dataSource: Observable<any>;
 
 model: any;
 searching = false;
 searchFailed = false;
constructor( private tratamientoService: TratamientosService,private route: Router) {
 // Typehead
 this.dataSource = Observable.create((observer: any) => {
   // Runs on every search
   observer.next(this.asyncSelected);
   }).pipe(
     switchMap((token: string) => {  
       return this.tratamientoService.search(token).pipe(r => r);              
   }));
}

ngOnInit() {
}
//TypeHead
search = (text$: Observable<any>) =>
text$.pipe(
debounceTime(300),
distinctUntilChanged(),
tap(() => this.searching = true,
switchMap(term =>
this.tratamientoService.search(term),
      tap(() => this.searchFailed = false)
      ),),
  tap(() => this.searching = false)
)
changeTypeaheadLoading(e: boolean): void {
this.typeaheadLoading = e;
}

changeTypeaheadNoResults(e: boolean): void {
this.typeaheadNoResults = e;
}

public typeaheadOnSelect(e: TypeaheadMatch): void {
  this.myClick.emit(e.item.id);
  this.asyncSelected=""
  // this.route.navigate(['/panel/clientes/details/'+e.item.id]);
}
}
