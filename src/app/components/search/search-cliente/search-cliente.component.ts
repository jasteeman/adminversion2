import { Component, OnInit,EventEmitter ,Output, Input } from '@angular/core';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { ClienteService } from 'src/app/services/cliente.service';
import { map, debounceTime, distinctUntilChanged, switchMap, tap, first } from "rxjs/operators";
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-search-cliente',
  templateUrl: './search-cliente.component.html',
  styleUrls: ['./search-cliente.component.scss']
})
export class SearchClienteComponent implements OnInit {
  @Output() myClick = new EventEmitter();
  estado:boolean = false;
  // Para crear el search- ng generate component search-cliente
 //Typehead
 asyncSelected: string;
 typeaheadLoading: boolean;
 typeaheadNoResults: boolean;
 dataSource: Observable<any>;
 
 model: any;
 searching = false;
 searchFailed = false;
constructor( private clienteService: ClienteService,private route: Router) {
 // Typehead
 this.dataSource = Observable.create((observer: any) => {
   // Runs on every search
   observer.next(this.asyncSelected);
   }).pipe(
     switchMap((token: string) => {  
       return this.clienteService.search(token).pipe(r => r);              
   }));
}

ngOnInit() {
}
//TypeHead
search = (text$: Observable<any>) =>
text$.pipe(
debounceTime(300),
distinctUntilChanged(),
tap(() => this.searching = true,
switchMap(term =>
this.clienteService.search(term),
      tap(() => this.searchFailed = false)
      ),),
  tap(() => this.searching = false)
)
changeTypeaheadLoading(e: boolean): void {
this.typeaheadLoading = e;
}

changeTypeaheadNoResults(e: boolean): void {
this.typeaheadNoResults = e;
}

public typeaheadOnSelect(e: TypeaheadMatch): void {
  this.myClick.emit(e.item.id);
  this.asyncSelected="";
  // this.route.navigate(['/panel/clientes/details/'+e.item.id]);
}

}
