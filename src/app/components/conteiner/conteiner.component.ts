import { Component, OnInit, Inject   } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from '../../_nav';
import { map, first, timeout } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-conteiner',
  templateUrl: './conteiner.component.html',
  styleUrls: ['./conteiner.component.scss']
})
export class ConteinerComponent implements OnInit {
  public navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  user:any;
  
  constructor(private auth: AuthService,private router: Router,@Inject(DOCUMENT) _document?: any) {
    // verificar los items por los permisos de usuario logeado
    this.navItems = navItems;
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
   }

  ngOnInit() {
            this.auth.getLoggedUser().pipe(first()).subscribe(
          data => {
            if (data.type == "OK") {
             this.user=data.content
            }
          })
  }


  ngOnDestroy(): void {
    this.changes.disconnect();
    this.user=null;
  }
  logout(){
    setTimeout(() => { 
      this.auth.logOut();
      this.user=null; 
    }, 500);
    
  }

}
