import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private toastr: ToastrService,
  ) {
    // validar que el tipo está logeado y enviarlo al home
    // usar animación de login para esto
    var token = localStorage.getItem("token");
    if (token != null) {
      this.router.navigate(['/']);
    }
  }


  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/panel/home';
  }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;

    this.authService.loginWithUsernameAndPassword(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          // console.log(data);
          if (data.type == "OK") {
            // guardar token
            var token = data.content.split(" ")[1];
            localStorage.setItem('token', token);
            setTimeout(() => { 
              this.router.navigate([this.returnUrl]);
            }, 500);
           
          }
          // redireccionar al home
        },
        error => {
          this.toastr.info("ERROR N°"+error.status+":Usuario no autorizado. Intente nuevamente");
          // mostrar error de login (usuario o password incorrecto)
          this.loading = false;
        });
  }

}
