import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { map, first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loader-full-screen',
  templateUrl: './loader-full-screen.component.html',
  styleUrls: ['./loader-full-screen.component.scss']
})
export class LoaderFullScreenComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {

    // al ingresar al root verificar si está logeado para
    // redireccionar al login o al home
    var token = localStorage.getItem("token");
    setTimeout(() => { 
      if (token != null) {
        this.auth.getLoggedUser().pipe(first()).subscribe(
          data => {
            if (data.type == "OK") {
              this.router.navigate(['/panel/home']);
            }
          },
          error => {
            localStorage.removeItem('token');
            this.router.navigate(['/login']);
          })
       
      } else {
        this.router.navigate(['/login']);
      }
    }, 800);
  }

}
