import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { ImageCompressService, ResizeOptions, ImageUtilityService, IImage, SourceImage } from  'ng2-image-compress';
import { UploadService } from 'src/app/services/upload.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  title = 'app';
  selectedImage: any;
  processedImages: any = [];
  showTitle: boolean = false;
  files=[];
  @Output() myClick = new EventEmitter();

  constructor(private imgCompressService: ImageCompressService,private uploadService:UploadService) { }

  ngOnInit() {
  }
  onChange(fileInput: any) {
    let fileList: FileList;
    let images: Array<IImage> = [];
    
    ImageCompressService.filesToCompressedImageSource(fileInput.target.files).then(observableImages => {
      observableImages.subscribe((image) => {
        images.push(image);
        this.submitImages(image.compressedImage.imageDataUrl)
      }, (error) => {
        console.log("Error while converting");
      }, () => {
                this.processedImages = images;      
                this.showTitle = true;             
      });
    })
  }

  submitImages(param){
      this.uploadService.producto(param).subscribe(data => {
        if (data.type == "OK") {
          this.myClick.emit(data.content);
        }
      },error => console.log(error))
    // });
  }

}
