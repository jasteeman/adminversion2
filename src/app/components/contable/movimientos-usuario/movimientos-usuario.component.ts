import { Component, OnInit, TemplateRef, Output,EventEmitter} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-movimientos-usuario',
  templateUrl: './movimientos-usuario.component.html',
  styleUrls: ['./movimientos-usuario.component.scss']
})
export class MovimientosUsuarioComponent implements OnInit {
  movimiento=null;
  mov={monto:0,motivo:null,formaPago:null};
  formaDePagos=["EFECTIVO","DEBITO","CREDITO","CHEQUE","OTRO"];

  @Output() myClickMovimientoUser =new EventEmitter();
   //  Modal
   config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
  constructor( private toastr: ToastrService, private modalService: BsModalService) { }

  ngOnInit() {
  }
  openModal(template: TemplateRef<any>) {
    if (this.mov.monto == 0) {
      this.toastr.error("Ingrese una monto mayor a 0")
      return
    }
    if (this.movimiento== null) {
      this.toastr.error("Ingrese un tipo de movimiento")
      return
    }
    if(this.mov.formaPago==null){
      this.toastr.error("Debe ingresar una forma de pago para continuar");
      return;
    }
    if(this.mov.motivo==null){
      this.toastr.error("Debe ingresar un motivo para continuar");
      return;
    }
    if(isNaN(this.mov.monto)){
      this.toastr.error("Ingrese un valor numérico en el monto")
      return
     }
     if(this.movimiento=="INGRESO"){
       let temp=this.mov.monto*-1;
       this.mov.monto=temp;
     }
    this.modalRef = this.modalService.show(template,this.config);
  }

  create() {
    this.modalRef.hide()
    this.myClickMovimientoUser.emit(this.mov);
    this.movimiento=null;
    this.mov={monto:0,motivo:null,formaPago:null};
  }
}
