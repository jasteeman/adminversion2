import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimientosClienteComponent } from './movimientos-cliente.component';

describe('MovimientosClienteComponent', () => {
  let component: MovimientosClienteComponent;
  let fixture: ComponentFixture<MovimientosClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimientosClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimientosClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
