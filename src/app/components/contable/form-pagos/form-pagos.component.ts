import { Component, OnInit, TemplateRef, Output,EventEmitter} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-form-pagos',
  templateUrl: './form-pagos.component.html',
  styleUrls: ['./form-pagos.component.scss']
})
export class FormPagosComponent implements OnInit {
  formaDePagos=["EFECTIVO","DEBITO","CREDITO","CHEQUE","OTRO"];
  pago={monto:0,tipoPago:"EFECTIVO"};
  tablaPagos=[];
  tempPago:any;
  @Output() myClickPago =new EventEmitter();
   //  Modal
   config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
  constructor( private toastr: ToastrService, private modalService: BsModalService) { }

  ngOnInit() {
  }

      //Operaciones de tablas
      cargarPago() {
        if (this.pago.monto == 0) {
          this.toastr.error("Ingrese una monto mayor a 0")
          return
        }
        if (this.pago.tipoPago== null) {
          this.toastr.error("Ingrese un tipo de pago")
          return
        }
        if(this.pago.tipoPago==null){
          this.toastr.error("Debe ingresar un cliente para continuar");
          return;
        }
        if(isNaN(this.pago.monto)){
          this.toastr.error("Ingrese un valor numérico en el monto")
          return
         }
        this.tablaPagos.push(this.pago);
        this.pago={monto:0,tipoPago:"EFECTIVO"};
        this.myClickPago.emit(this.tablaPagos);
      }

      eliminarItemPago() {
        for (let i = 0; i < this.tablaPagos.length; ++i) {
          if (this.tablaPagos[i] === this.tempPago) {
            this.tablaPagos.splice(i, 1);
            this.toastr.info("Monto borrado")
            this.modalRef.hide()
          }
        }
        this.myClickPago.emit(this.tablaPagos);
      }

      openModalPago(template: TemplateRef<any>, data) {
        this.tempPago = data
        this.modalRef = this.modalService.show(template, this.config);
      }

}
