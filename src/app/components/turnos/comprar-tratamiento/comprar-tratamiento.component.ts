import { TratamientosService } from 'src/app/services/tratamientos.service';
import { first } from "rxjs/operators";
import { Component, OnInit, TemplateRef, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-comprar-tratamiento',
  templateUrl: './comprar-tratamiento.component.html',
  styleUrls: ['./comprar-tratamiento.component.scss']
})

export class ComprarTratamientoComponent implements OnInit {
  tempTratamiento = { sesionesTratamiento: 0, totalTratamiento: 0, costoTratamiento: 0, descuentoTratamiento: 0 }
  tratamiento = { nombre: "", sesiones: null, precio: null, habitaciones: [], precioFinal: 0 };
  descuento = 0;
  tablaTratamientos = [];
  tempPago: any;
  @Output() myClickComprarTratamiento = new EventEmitter();
  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;

  constructor(private tratamientoService: TratamientosService, private toastr: ToastrService, private modalService: BsModalService) { }

  ngOnInit() {
  }
  TransportarTratamiento(event: any) {
    this.tratamientoService.getFullTratamiento(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.tratamiento = data.content;
        this.tempTratamiento.costoTratamiento=this.tratamiento.precio;
      }
    })
  }

  limpiarForm() {
    this.tempTratamiento = { sesionesTratamiento: 0, totalTratamiento: 0, costoTratamiento: 0, descuentoTratamiento: 0 }
    this.tratamiento = { nombre: "", sesiones: null, precio: null, habitaciones: [], precioFinal: 0 };
  }

  calcularTratamiento() {
    var total = 0;
    var total2 = 0;
    var total3 = 0;
    this.descuento = 0;
    total = (this.tempTratamiento.costoTratamiento * this.tempTratamiento.sesionesTratamiento);
    total2 = Math.round(total * 100) / 100;
    this.tempTratamiento.totalTratamiento = total2;
    this.descuento = this.tempTratamiento.descuentoTratamiento;
    total3 = total2 - this.descuento;
    return total3;
  }
  //Operaciones de tablas
  addTratamiento() {
    if (this.tempTratamiento.sesionesTratamiento == 0) {
      this.toastr.error("Ingrese un número mayor a 0 en el en la cantidad de sesiones")
      return
    }
    this.tratamiento.precio = this.tempTratamiento.totalTratamiento;
    this.tratamiento.sesiones = this.tempTratamiento.sesionesTratamiento;
    this.tratamiento.precioFinal = this.calcularTratamiento()
    this.tablaTratamientos.push(this.tratamiento);
    this.tempTratamiento = { sesionesTratamiento: 0, totalTratamiento: 0, costoTratamiento: 0, descuentoTratamiento: 0 }
    this.tratamiento = { nombre: "", sesiones: null, precio: null, habitaciones: [], precioFinal: 0 };
    this.myClickComprarTratamiento.emit(this.tablaTratamientos);
  }

  eliminarItemTratamiento() {
    for (let i = 0; i < this.tablaTratamientos.length; ++i) {
      if (this.tablaTratamientos[i] === this.tempPago) {
        this.tablaTratamientos.splice(i, 1);
        this.toastr.info("tratamiento borrado")
        this.modalRef.hide()
      }
    }
    this.myClickComprarTratamiento.emit(this.tablaTratamientos);
  }

  precioTotal = function (x) {
      var total2 = 0;
      total2 = x.precio - x.precioFinal;
      return total2;
  };

  openModalTratamiento(template: TemplateRef<any>, data) {
    this.tempPago = data
    this.modalRef = this.modalService.show(template, this.config);
  }

}
