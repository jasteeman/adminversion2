import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComprarTratamientoComponent } from './comprar-tratamiento.component';

describe('ComprarTratamientoComponent', () => {
  let component: ComprarTratamientoComponent;
  let fixture: ComponentFixture<ComprarTratamientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComprarTratamientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComprarTratamientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
