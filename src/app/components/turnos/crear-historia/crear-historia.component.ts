import { Component, OnInit, TemplateRef, Output, EventEmitter } from '@angular/core';
import { UploadService } from 'src/app/services/upload.service';
import { FichasService } from 'src/app/services/fichas.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-crear-historia',
  templateUrl: './crear-historia.component.html',
  styleUrls: ['./crear-historia.component.scss']
})
export class CrearHistoriaComponent implements OnInit {

  @Output() myClickCrearFicha = new EventEmitter();

  //Historia Clinica
  historiaClinica = {
    fecha: new Date(),
    fotos: [],
    items: [],
    paciente: null,
    indicaciones: null
  }

  load = false;
  listaDeFichasClinicas = [];
  loadTablaHistoriaClinica = false;
  respuestaObservacion: any;
  fichaHistoriaClinica = [];
  sesionTemp = [];
  tempRespuesta = { pregunta: undefined, respuesta: undefined };
  imageTemp: any;
  imageList = [];

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;

  constructor(private modalService: BsModalService, private uploadService: UploadService, private fichasService: FichasService, private toastr: ToastrService) { }

  ngOnInit() {
    this.traerHistoriasClinicas();
    this.autoObservacion();
  }

  // 1.1 Historias Clinicas
  traerHistoriasClinicas() {
    var temp = "historiaClinica";
    this.fichasService.getTipoDePlantilla(temp).subscribe(data => {
      this.listaDeFichasClinicas = data.content
    })
  }

  //1.2 Seleccion de ficha
  cargarPreguntas(x) {
    var temp = {}
    this.loadTablaHistoriaClinica = true;
    this.fichasService.getPlantilla(x).subscribe(data => {
      data.content.items.forEach(a => {
        if (a.pregunta == "Observaciones:") {
          temp = { pregunta: a.pregunta, respuesta: this.respuestaObservacion }
          this.fichaHistoriaClinica.push(temp)
          temp = {}
        } else {
          temp = { pregunta: a.pregunta, respuesta: null }
          this.fichaHistoriaClinica.push(temp)
          temp = {}
        }
        this.fichaHistoriaClinica = this.eliminarObjetosDuplicados(this.fichaHistoriaClinica, 'pregunta')
      });

      //Cargo lo q estaba guardado
      this.sesionTemp.forEach(x => {
        temp = { pregunta: x.pregunta, respuesta: x.respuesta }
        this.fichaHistoriaClinica.push(temp)
        temp = {}
      })
      this.sesionTemp = [];
      this.loadTablaHistoriaClinica = false;
    })
  }

  //1.3 Carga automatica de las observaciones
  autoObservacion() {
    this.listaDeFichasClinicas.forEach(x => {
      if (x.nombre == "Observación") {
        this.cargarPreguntas(x.id)
      }
    })
  }

  //1.4 Eliminar duplicados de array
  eliminarObjetosDuplicados(arr, prop) {
    var nuevoArray = [];
    var lookup = {};
    for (var i in arr) {
      lookup[arr[i][prop]] = arr[i];
    }
    for (i in lookup) {
      nuevoArray.push(lookup[i]);
    }
    return nuevoArray;
  }

  //1.5 Modal de respuestas de historias clinicas
  openModalRespuestas(template: TemplateRef<any>, data) {
    this.tempRespuesta = data;
    this.modalRef = this.modalService.show(template, this.config);
  }
  cancelarRespuesta() {
    this.tempRespuesta = { pregunta: undefined, respuesta: undefined };
    this.modalRef.hide()
  }

  // 1.6 Cancelar carga de imagenes popup
  cancelarImages() {
    this.imageList = [];
    this.modalRef.hide();
  }

  // 1.7 Actualizar y cargar respuesta
  actualizarRespuesta(data) {
    var temp = this.fichaHistoriaClinica;
    this.fichaHistoriaClinica = [];
    this.loadTablaHistoriaClinica = true;
    temp.forEach(x => {
      if (x.pregunta == data.pregunta) {
        x.respuesta = data.respuesta
      }
      this.fichaHistoriaClinica.push(x);
      this.loadTablaHistoriaClinica = false;
    })
    this.modalRef.hide()
  }

  // 5.8 IMAGENES

  // 5.8.1 Modal de  imagenes
  openModalBorrarImage(template: TemplateRef<any>, data) {
    this.imageTemp = null;
    this.imageTemp = data;
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalImg(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  // 5.8.2 Binding imagenes
  TransportarImages(event: any) {
    this.load = true;
    this.imageList = []
    this.imageList = event;
    if (this.imageList.length > 0) {
      this.load = false;
    }
  }

  // 5.8.3 Eliminar imagen antes de subir
  deleteImage() {
    this.load = true;
    for (let i = 0; i < this.imageList.length; ++i) {
      if (this.imageList[i] === this.imageTemp) {
        this.imageList.splice(i, 1);
        this.modalRef.hide();
        this.load = false;
      }
    }
  }

  //5.8.4 Uploads Imagenes
  recursivaImages = (index, array) => new Promise((resolve, reject) => {
    if (index < array.length) {
      this.uploadService.histociaClinicas(array[index].compressedImage.imageDataUrl).subscribe(data => {
        if (data.type == "OK") {
          this.historiaClinica.fotos.push(data.content);
          resolve(this.recursivaImages(index + 1, array));
        }

      })
    } else {
      return resolve("OK");
    }
  });

  //Crear ficha Medica
  crearFicha() {
    if (this.fichaHistoriaClinica.length == 0) {
      this.toastr.error('Debe tener al menos una observación del turno', 'ERROR EN HISTORIA CLINICA');
      return;
    }
    this.historiaClinica.items = this.fichaHistoriaClinica;
    this.load = true;
    if (navigator.onLine) {
      var estado = true;
      if (this.imageList.length == 0) {
        this.myClickCrearFicha.emit(this.historiaClinica);
      } else {
        this.recursivaImages(0, this.imageList).then(data => {
          if (data == "OK") {
            this.myClickCrearFicha.emit(this.historiaClinica);
          }
        })
      }
    } else {
      this.load = false;
      this.toastr.error('Sin Conexion a internet. Intente nuevamente')
    }
  }

}
