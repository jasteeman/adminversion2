import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadClinicComponent } from './file-upload-clinic.component';

describe('FileUploadClinicComponent', () => {
  let component: FileUploadClinicComponent;
  let fixture: ComponentFixture<FileUploadClinicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileUploadClinicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadClinicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
