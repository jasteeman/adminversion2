import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConteinerComponent } from './components/conteiner/conteiner.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { LoaderFullScreenComponent } from './components/loader-full-screen/loader-full-screen.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { UsuariosListComponent } from './views/usuarios/usuarios-list/usuarios-list.component';
import {ClientesListComponent} from './views/clientes/clientes-list/clientes-list.component';
import {ClientesDetailsComponent} from './views/clientes/clientes-details/clientes-details.component';
import{ClientesCreateComponent} from './views/clientes/clientes-create/clientes-create.component';
import { UsuariosCreateComponent } from './views/usuarios/usuarios-create/usuarios-create.component';
import { UsuariosDetailsComponent } from './views/usuarios/usuarios-details/usuarios-details.component';
import { ProductosListComponent } from './views/productos/productos-list/productos-list.component';
import { ProductosDetailsComponent } from './views/productos/productos-details/productos-details.component';
import { ProductosCreateComponent } from './views/productos/productos-create/productos-create.component';
import { ProveedoresListComponent } from './views/proveedores/proveedores-list/proveedores-list.component';
import { ProveedoresDetailComponent } from './views/proveedores/proveedores-detail/proveedores-detail.component';
import { ProveedoresCreateComponent } from './views/proveedores/proveedores-create/proveedores-create.component';
import { GestionStockComponent } from './views/productos/gestion-stock/gestion-stock.component';
import { MovimientosStockComponent } from './views/productos/movimientos-stock/movimientos-stock.component';
import { ProductosPrecioComponent } from './views/productos/productos-precio/productos-precio.component';
import { PedidosCreateComponent } from './views/pedidos/pedidos-create/pedidos-create.component';
import { PedidosListComponent } from './views/pedidos/pedidos-list/pedidos-list.component';
import { PedidosDetailsComponent } from './views/pedidos/pedidos-details/pedidos-details.component';
import { VentasCreateComponent } from './views/ventas/ventas-create/ventas-create.component';
import { VentasListComponent } from './views/ventas/ventas-list/ventas-list.component';
import { VentasDetailsComponent } from './views/ventas/ventas-details/ventas-details.component';
import { ClienteContableComponent } from './views/clientes/cliente-contable/cliente-contable.component';
import { ClienteRecibosComponent } from './views/clientes/cliente-recibos/cliente-recibos.component';
import { CajaPersonalComponent } from './views/contable/caja-personal/caja-personal.component';
import { CajaMaestraComponent } from './views/contable/caja-maestra/caja-maestra.component';
import { CajaHistorialComponent } from './views/contable/caja-historial/caja-historial.component';
import { CajaDetailsComponent } from './views/contable/caja-details/caja-details.component';
import { CajaCerradaComponent } from './views/contable/caja-cerrada/caja-cerrada.component';
import { FeriadoDetailsComponent } from './views/feriados/feriado-details/feriado-details.component';
import { FeriadoListComponent } from './views/feriados/feriado-list/feriado-list.component';
import { FeriadoCreateComponent } from './views/feriados/feriado-create/feriado-create.component';
import { SalasDetailsComponent } from './views/salas/salas-details/salas-details.component';
import { SalasListComponent } from './views/salas/salas-list/salas-list.component';
import { SalasCreateComponent } from './views/salas/salas-create/salas-create.component';
import { FichasCreateComponent } from './views/fichas/fichas-create/fichas-create.component';
import { FichasListComponent } from './views/fichas/fichas-list/fichas-list.component';
import { FichasDetailsComponent } from './views/fichas/fichas-details/fichas-details.component';
import { TareasCreateComponent } from './views/tareas/tareas-create/tareas-create.component';
import { TareasListComponent } from './views/tareas/tareas-list/tareas-list.component';
import { TareasDetailsComponent } from './views/tareas/tareas-details/tareas-details.component';
import { TareasPersonalComponent } from './views/tareas/tareas-personal/tareas-personal.component';
import { CategoriaListComponent } from './views/tratamientos/categoria-list/categoria-list.component';
import { CategoriaDetailsComponent } from './views/tratamientos/categoria-details/categoria-details.component';
import { CategoriaCreateComponent } from './views/tratamientos/categoria-create/categoria-create.component';
import { TurnosListComponent } from './views/turnos/turnos-list/turnos-list.component';
import { TurnosCreateComponent } from './views/turnos/turnos-create/turnos-create.component';
import { TurnosCalendarioComponent } from './views/turnos/turnos-calendario/turnos-calendario.component';
import { ClientesHistoriaClinicaComponent } from './views/clientes/clientes-historia-clinica/clientes-historia-clinica.component';
import { EstadisticasVentasComponent } from './views/estadisticas/estadisticas-ventas/estadisticas-ventas.component';
import { EstadisticasActivaBajaComponent } from './views/estadisticas/estadisticas-activa-baja/estadisticas-activa-baja.component';
import { ClientesCumplesComponent } from './views/clientes/clientes-cumples/clientes-cumples.component';
import { ConfiguracionesListComponent } from './views/configuraciones/configuraciones-list/configuraciones-list.component';
import { TurnosHistorialComponent } from './views/turnos/turnos-historial/turnos-historial.component';
import { EstadisticasTurnosComponent } from './views/estadisticas/estadisticas-turnos/estadisticas-turnos.component';



const routes: Routes = [
  { path: '', component: LoaderFullScreenComponent },
  { path: 'login', component: LoginComponent },
  {path: 'panel', component: ConteinerComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'usuarios', component: UsuariosListComponent },
      { path: 'usuarios/details/:id', component: UsuariosDetailsComponent},
      { path: 'usuarios/create', component: UsuariosCreateComponent},
      { path: 'clientes/list', component: ClientesListComponent},
      { path: 'clientes/details/:id', component: ClientesDetailsComponent},
      { path: 'clientes/create', component: ClientesCreateComponent},
      { path: 'clientes/cuentaCorriente', component: ClienteContableComponent},
      { path: 'clientes/recibos', component: ClienteRecibosComponent},
      { path: 'clientes/historias', component: ClientesHistoriaClinicaComponent},
      { path: 'clientes/avisos', component: ClientesCumplesComponent},
      { path: 'productos/list', component: ProductosListComponent},
      { path: 'productos/details/:id', component: ProductosDetailsComponent},
      { path: 'productos/create', component: ProductosCreateComponent},
      { path: 'productos/stock', component: GestionStockComponent},
      { path: 'productos/historial', component: MovimientosStockComponent},
      { path: 'productos/precios', component: ProductosPrecioComponent},
      { path: 'proveedores', component: ProveedoresListComponent},
      { path: 'proveedores/details/:id', component: ProveedoresDetailComponent},
      { path: 'proveedores/create', component: ProveedoresCreateComponent},
      { path: 'pedidos/create', component: PedidosCreateComponent},
      { path: 'pedidos', component: PedidosListComponent},
      { path: 'pedidos/details/:id', component: PedidosDetailsComponent},
      { path: 'ventas/create', component: VentasCreateComponent},
      { path: 'ventas/list', component: VentasListComponent},
      { path: 'ventas/details/:id', component: VentasDetailsComponent},
      { path: 'contable/personal', component: CajaPersonalComponent},
      { path: 'contable/maestro', component: CajaMaestraComponent},
      { path: 'contable/historial', component: CajaHistorialComponent},
      { path: 'contable/details/:id', component: CajaDetailsComponent},
      { path: 'contable/cierre/:id', component: CajaCerradaComponent},
      { path: 'feriados/create', component: FeriadoCreateComponent},
      { path: 'feriados/list', component: FeriadoListComponent},
      { path: 'feriados/details/:id', component: FeriadoDetailsComponent},
      { path: 'salas/create', component: SalasCreateComponent},
      { path: 'salas/list', component: SalasListComponent},
      { path: 'salas/details/:id', component: SalasDetailsComponent},
      { path: 'fichas/create', component: FichasCreateComponent},
      { path: 'fichas/list', component: FichasListComponent},
      { path: 'fichas/details/:id', component: FichasDetailsComponent},
      { path: 'tareas/create', component: TareasCreateComponent},
      { path: 'tareas', component: TareasListComponent},
      { path: 'tareas/details/:id', component: TareasDetailsComponent},
      { path: 'tareas/personal', component: TareasPersonalComponent},
      { path: 'tratamientos', component: CategoriaListComponent},
      { path: 'tratamientos/details/:id', component: CategoriaDetailsComponent},
      { path: 'tratamientos/create', component: CategoriaCreateComponent},
      { path: 'turnos/list', component: TurnosListComponent},
      { path: 'turnos/create', component: TurnosCreateComponent},
      { path: 'turnos/calendario', component: TurnosCalendarioComponent},
      { path: 'turnos/historial', component: TurnosHistorialComponent},
      {path:'estadisticas/ventas',component:EstadisticasVentasComponent},
      {path:'estadisticas/activasBajas',component:EstadisticasActivaBajaComponent},
      {path:'estadisticas/turnos',component:EstadisticasTurnosComponent},
      {path:'configuraciones',component:ConfiguracionesListComponent},
    ]
  },
  { path: 'not-found', component: NotFoundComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
