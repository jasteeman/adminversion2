import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TurnosService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }
  consultarDisponibilidad(tratamientoId,turno) {
    return this.http.post<any>(`${environment.apiUrl}/api/turno/consultarDisponibilidad/`+tratamientoId,turno, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  ocuparTurno(sesionId,habitacionId,profesionalId,turno) {
    return this.http.post<any>(`${environment.apiUrl}/api/turno/ocuparTurno/`+sesionId+'/'+habitacionId+'/'+profesionalId,turno, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  cancelarTurnos(turno) {
    return this.http.post<any>(`${environment.apiUrl}/api/turno/cancelar`,turno, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  search(page,size,turno) {
    return this.http.post<any>(`${environment.apiUrl}/api/turno/search/`+page+'/'+size,turno, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
