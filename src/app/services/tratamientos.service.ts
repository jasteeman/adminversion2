import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TratamientosService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }
  // Categorias 
  getCategorias(page,size) {
    return this.http.get<any>(`${environment.apiUrl}/api/tratamiento/categoria/getPage/`+page+'/'+size+'/id/true', { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getFullCategoriaTratamiento(id) {
    return this.http.get<any>(`${environment.apiUrl}/api/tratamiento/categoria/getFull/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdateCategoria(sala) {
    return this.http.post<any>(`${environment.apiUrl}/api/tratamiento/categoria/saveOrUpdate/`, sala, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  deleteCategoria(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/tratamiento/categoria/delete/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  //Tratamientos
  getTratamientos(page,size) {
    return this.http.get<any>(`${environment.apiUrl}/api/tratamiento/getPage/`+page+'/'+size+'/id/true', { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getTratamientosPorCategoria(id) {
    return this.http.get<any>(`${environment.apiUrl}/api/tratamiento/getByCategoria/`+id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getFullTratamiento(id) {
    return this.http.get<any>(`${environment.apiUrl}/api/tratamiento/getFull/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdateTratamiento(categoriaId,tratamiento) {
    return this.http.post<any>(`${environment.apiUrl}/api/tratamiento/saveOrUpdate/`+categoriaId, tratamiento, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  deleteTratamiento(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/tratamiento/delete/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  search(text){
    return this.http.get<any>(`${environment.apiUrl}/api/tratamiento/search/` + text, { headers: this.header })
    .pipe(
      map(data => {
        return data.content
      }
      )
    )
}
//Comprar tratamiento
comprarTratamiento(tratamientoId,sesiones, clienteId, precio) {
  return this.http.get<any>(`${environment.apiUrl}/api/tratamiento/comprarTratamiento/`+ tratamientoId + '/'+clienteId+'/' + sesiones + '/' + precio, { headers: this.header }).pipe(
    map(data => {
      return data;
    })
  )
}
borrarTratamientoComprado(tratamientoCompradoId,motivo) {
  return this.http.post<any>(`${environment.apiUrl}/api/tratamiento/darDeAlta/`+ tratamientoCompradoId,motivo, { headers: this.header }).pipe(
    map(data => {
      return data;
    })
  )
}
tratamientosComprados(clienteId) {
  return this.http.get<any>(`${environment.apiUrl}/api/tratamiento/getTratamientosComprados/` + clienteId, { headers: this.header }).pipe(
    map(data => {
      return data;
    })
  )
}
marcarSesionCompleta(sesionId,estado,ficha) {
  return this.http.post<any>(`${environment.apiUrl}/api/tratamiento/marcarSesionCompletaOPonerFicha/`+ sesionId+'/'+estado,ficha, { headers: this.header }).pipe(
    map(data => {
      return data;
    })
  )
}
sesiones(search) {
  return this.http.post<any>(`${environment.apiUrl}/api/tratamiento/searchSesiones/`,search, { headers: this.header }).pipe(
    map(data => {
      return data;
    })
  )
}
}
