import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }

  getClientes(pageNumber: Number, pageSize: Number) {
    return this.http.get<any>(`${environment.apiUrl}/api/cliente/getPage/` + pageNumber + '/' + pageSize + '/id/true', { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getFull(id: Number) {
    return this.http.get<any>(`${environment.apiUrl}/api/cliente/getFull/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getCumples() {
    return this.http.get<any>(`${environment.apiUrl}/api/cliente/getCumpleanos/`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdate(cliente) {
    return this.http.post<any>(`${environment.apiUrl}/api/cliente/saveOrUpdate/`, cliente, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  delete(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/cliente/delete/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  search(text){
      return this.http.get<any>(`${environment.apiUrl}/api/cliente/search/` + text, { headers: this.header })
      .pipe(
        map(data => {
          return data.content
        }
        )
      )
  }

}
