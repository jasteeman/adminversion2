import { TestBed } from '@angular/core/testing';

import { TratamientosService } from './tratamientos.service';

describe('TratamientosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TratamientosService = TestBed.get(TratamientosService);
    expect(service).toBeTruthy();
  });
});
