import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AfipService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }

  getCondicionDeIva() {
    return this.http.get<any>(`${environment.apiUrl}/api/condicioniva/getPage`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getPuntoDeVentas() {
    return this.http.get<any>(`${environment.apiUrl}/api/puntosdeventas/getPage`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  saveOrUpdatePuntoDeVenta(cliente) {
    return this.http.post<any>(`${environment.apiUrl}/api/puntosdeventas/saveOrUpdate/`, cliente, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  deletePuntoDeVenta(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/puntosdeventas/delete/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getFullPuntoDeVenta(id: Number) {
    return this.http.get<any>(`${environment.apiUrl}/api/puntosdeventas/getFull/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getCodigoDeOperacion() {
    return this.http.get<any>(`${environment.apiUrl}/api/codigoOperacion/getPage`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getDuenioSistema() {
    return this.http.get<any>(`${environment.apiUrl}/api/sistema/getPage`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getTipoDeDocumentos() {
    return this.http.get<any>(`${environment.apiUrl}/api/tipodocumento/getPage`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getTipoMonedas() {
    return this.http.get<any>(`${environment.apiUrl}/api/tipomoneda/getPage`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getTiposDeIvas() {
    return this.http.get<any>(`${environment.apiUrl}/api/tipoiva/getPage`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getTiposDeFacturas() {
    return this.http.get<any>(`${environment.apiUrl}/api/tipofactura/getPage`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

}
