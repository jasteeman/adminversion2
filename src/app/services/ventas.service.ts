import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VentasService {

  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }
  getVentas(filtro) {
    return this.http.post<any>(`${environment.apiUrl}/api/ventas/search`, filtro, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  
  saveOrUpdate(venta) {
    return this.http.post<any>(`${environment.apiUrl}/api/ventas/vender/`, venta, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  cmv(desde,hasta){
    return this.http.get<any>(`${environment.apiUrl}/api/ventas/findCMV/`+desde+'/'+hasta, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  listadoMensual(desde,hasta){
    return this.http.get<any>(`${environment.apiUrl}/api/ventas/listadoMensual/`+desde+'/'+hasta, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
