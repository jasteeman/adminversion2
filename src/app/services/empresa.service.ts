import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }

  getEmpresas() {
    return this.http.get<any>(`${environment.apiUrl}/api/empresa/getPage/`+0+'/'+20+'/id/true', { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getFull(id) {
    return this.http.get<any>(`${environment.apiUrl}/api/empresa/getFull/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdate(data) {
    return this.http.post<any>(`${environment.apiUrl}/api/empresa/saveOrUpdate/`, data, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  delete(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/empresa/delete/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
