import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse} from '@angular/common/http';
import { map, } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { Observable,throwError  } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements HttpInterceptor {
  header: HttpHeaders;

    constructor(private http: HttpClient,
    private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  
    const token: string = localStorage.getItem('token');

    let request = req;

    if (token) {
      request = req.clone({
        setHeaders: {
          authorization: `Bearer ${ token }`
        }
      });
    }

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {

        if (err.status === 401|| err.status===-1|| err.status===403) {
          this.router.navigate(['/']);
        }

        return throwError( err );

      })
    );
  }
  
  loginWithUsernameAndPassword(username, password) {
    return this.http.post<any>(`${environment.apiUrl}/login`, { username, password }).pipe(
      map(data => {
        return data;
      })
    )
  }

  logOut() {
    localStorage.removeItem('token');
    this.router.navigate(['/']);
  }

  getLoggedUser() {
    return this.http.get<any>(`${environment.apiUrl}/api/usuario/getLoggedUser`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
// export class AuthService  {
//   header: HttpHeaders;

//   constructor(private http: HttpClient,
//     private router: Router) {
//     this.header = new HttpHeaders({
//       'Content-Type': 'application/json',
//       'Authorization': 'Bearer ' + localStorage.getItem('token')
//     });
//   }

//   public loginWithUsernameAndPassword(username, password) {
//     return this.http.post<any>(`${environment.apiUrl}/login`, { username, password }).pipe(
//       map(data => {
//         return data;
//       })
//     )
//   }

//   public logOut() {
//     localStorage.removeItem('token');
//     this.router.navigate(['/']);
//   }

//   public getLoggedUser() {
//     return this.http.get<any>(`${environment.apiUrl}/api/usuario/getLoggedUser`, { headers: this.header }).pipe(
//       map(data => {
//         return data;
//       })
//     )
//   }
// }
