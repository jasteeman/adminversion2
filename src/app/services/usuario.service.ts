import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }

  getUsuarios(pageNumber: Number, pageSize: Number) {
    return this.http.get<any>(`${environment.apiUrl}/api/usuario/getPage/` + pageNumber + '/' + pageSize + '/id/true', { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getFull(id:Number){
    return this.http.get<any>(`${environment.apiUrl}/api/usuario/getFull/` +id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  saveOrUpdate(usuario){
    return this.http.post<any>(`${environment.apiUrl}/api/usuario/saveOrUpdate/` ,usuario, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  delete(id){
    return this.http.delete<any>(`${environment.apiUrl}/api/usuario/delete/`+id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  roles(){
    return this.http.get<any>(`${environment.apiUrl}/api/utiles/getPosibleRoles/`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  search(text){
    return this.http.get<any>(`${environment.apiUrl}/api/usuario/search/` +text, { headers: this.header }).pipe(
      map(data => {
        return data.content;
      })
    )
  }
  
  updatePassword(usuario){
    return this.http.post<any>(`${environment.apiUrl}/api/usuario/changePassword/` ,usuario, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  updateRoles(usuarioId,nuevoRol){
    return this.http.get<any>(`${environment.apiUrl}/api/usuario/updateRol/` +usuarioId+'/'+nuevoRol, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
