import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FichasService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }

  getTipoDePlantilla(page) {
    return this.http.get<any>(`${environment.apiUrl}/api/historiaclinica/getTemplatesByTipo/`+page, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getPlantilla(page) {
    return this.http.get<any>(`${environment.apiUrl}/api/historiaclinica/getTemplate/`+page, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdatePlantilla(page) {
    return this.http.post<any>(`${environment.apiUrl}/api/historiaclinica/saveOrUpdateTemplate/`,page, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  deletePlantilla(page) {
    return this.http.delete<any>(`${environment.apiUrl}/api/historiaclinica/deleteTemplate/`+page, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  // *********************************************************************************************
  // Historias clinicas
  getHistoriasClinicas(page,size,clienteId) {
    return this.http.get<any>(`${environment.apiUrl}/api/historiaclinica/searchHistoriaClinica/`+page+'/'+size+'/id/true/'+clienteId, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getFullHistoriaClinica(id: Number) {
    return this.http.get<any>(`${environment.apiUrl}/api/historiaclinica/get/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdateHistoriaClinica(feriado) {
    return this.http.post<any>(`${environment.apiUrl}/api/historiaclinica/saveOrUpdate/`, feriado, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  saveOrUpdateHistoriaClinicaManual(feriado) {
    return this.http.post<any>(`${environment.apiUrl}/api/historiaclinica/saveOrUpdateManual/`, feriado, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  deleteHistoriaClinica(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/historiaclinica/deleteHistoriaClinica/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
