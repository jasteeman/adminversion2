import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }
  getPedidos(filtro) {
    return this.http.post<any>(`${environment.apiUrl}/api/pedido/search`, filtro, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getFull(id: Number) {
    return this.http.get<any>(`${environment.apiUrl}/api/pedido/get/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdate(pedido) {
    return this.http.post<any>(`${environment.apiUrl}/api/pedido/saveOrUpdate/`, pedido, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
