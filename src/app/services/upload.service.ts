import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  header: HttpHeaders;

  constructor(private http: HttpClient, private router: Router) {
    this.header= new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    })
  }

  producto(data) {
    var dataURI = data
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    var blob = new Blob([ia], {
        type: 'image/jpeg'
    });
    var imagen = new File([blob], "image.jpg");
    
    var reader = new FileReader();
    reader.readAsDataURL(imagen);
    // GUARDO IMAGEN Y DATOS ACTUALIZADOS
    const formData: FormData = new FormData();
    formData.append('file',imagen)  

    return this.http.post<any>(`${environment.apiUrl}/upload/producto`, formData,{headers:this.header}).pipe(
      map(data => {    
        return data;
      })
    )
  }

  histociaClinicas(data) {
    var dataURI = data
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    var blob = new Blob([ia], {
        type: 'image/jpeg'
    });
    var imagen = new File([blob], "image.jpg");
    
    var reader = new FileReader();
    reader.readAsDataURL(imagen);
    // GUARDO IMAGEN Y DATOS ACTUALIZADOS
    const formData: FormData = new FormData();
    formData.append('file',imagen)  

    return this.http.post<any>(`${environment.apiUrl}/upload/historia`, formData,{headers:this.header}).pipe(
      map(data => {    
        return data;
      })
    )
  }
}
