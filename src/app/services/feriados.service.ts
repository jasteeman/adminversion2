import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FeriadosService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }

  getFeriados(page,size) {
    return this.http.get<any>(`${environment.apiUrl}/api/feriado/getPage/`+page+'/'+size+'/id/true', { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getFull(id: Number) {
    return this.http.get<any>(`${environment.apiUrl}/api/feriado/getFull/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdate(feriado) {
    return this.http.post<any>(`${environment.apiUrl}/api/feriado/saveOrUpdate/`, feriado, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  delete(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/feriado/delete/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

}
