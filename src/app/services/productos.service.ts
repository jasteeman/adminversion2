import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }

  getProductos(filtro) {
    return this.http.post<any>(`${environment.apiUrl}/api/producto/getPage`, filtro, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getFull(id: Number) {
    return this.http.get<any>(`${environment.apiUrl}/api/producto/getFull/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdate(cliente) {
    return this.http.post<any>(`${environment.apiUrl}/api/producto/saveOrUpdate/`, cliente, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  bajaProducto(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/producto/delete/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  altaProducto(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/producto/habilitar/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  search(text) {
    return this.http.get<any>(`${environment.apiUrl}/api/producto/search/` + text, { headers: this.header })
      .pipe(
        map(data => {
          return data.content
        }
        )
      )
  }

  moveStock(producto) {
    return this.http.post<any>(`${environment.apiUrl}/api/producto/stock/move`, producto, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  findAllStock(id, page, size) {
    return this.http.get<any>(`${environment.apiUrl}/api/producto/stock/list/` + id + `/getPage/` + page + `/` + size + `/id/true`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
