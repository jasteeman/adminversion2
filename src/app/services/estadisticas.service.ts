import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class EstadisticasService {

  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }
  activasBajas(search){
    return this.http.post<any>(`${environment.apiUrl}/api/estadisticas/altasBajas/`,search, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  turnos(search){
    return this.http.post<any>(`${environment.apiUrl}/api/estadisticas/turnos/`,search, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
