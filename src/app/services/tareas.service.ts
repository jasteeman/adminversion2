import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TareasService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }
  bajaTarea(id) {
    return this.http.get<any>(`${environment.apiUrl}/api/tarea/darDeBaja/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  completarTarea(id) {
    return this.http.get<any>(`${environment.apiUrl}/api/tarea/completar/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  getFull(id) {
    return this.http.get<any>(`${environment.apiUrl}/api/tarea/getFull/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  searchTareas(search) {
    return this.http.post<any>(`${environment.apiUrl}/api/tarea/search`,search, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdate(idUser,sala) {
    return this.http.post<any>(`${environment.apiUrl}/api/tarea/saveOrUpdate/`+idUser, sala, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  delete(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/tarea/delete/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
