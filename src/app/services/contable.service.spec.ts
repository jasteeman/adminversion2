import { TestBed } from '@angular/core/testing';

import { ContableService } from './contable.service';

describe('ContableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContableService = TestBed.get(ContableService);
    expect(service).toBeTruthy();
  });
});
