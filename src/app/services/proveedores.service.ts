import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProveedoresService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }

  getProveedores(filtro) {
    return this.http.post<any>(`${environment.apiUrl}/api/proveedor/search/` ,filtro, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getFull(id: Number) {
    return this.http.get<any>(`${environment.apiUrl}/api/proveedor/getFull/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdate(proveedor) {
    return this.http.post<any>(`${environment.apiUrl}/api/proveedor/saveOrUpdate/`, proveedor, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  delete(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/proveedor/delete/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

}
