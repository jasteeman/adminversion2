import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SalasService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }

  getSalas(page,size) {
    return this.http.get<any>(`${environment.apiUrl}/api/habitacion/getPage/`+page+'/'+size+'/id/true', { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  getFull(id) {
    return this.http.get<any>(`${environment.apiUrl}/api/habitacion/getFull/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  saveOrUpdate(sala) {
    return this.http.post<any>(`${environment.apiUrl}/api/habitacion/saveOrUpdate/`, sala, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  delete(id) {
    return this.http.delete<any>(`${environment.apiUrl}/api/habitacion/delete/` + id, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
}
