import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContableService {
  header: HttpHeaders;
  constructor(private http: HttpClient, private router: Router) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
  }
  saldoCliente(cliente) {
    return this.http.get<any>(`${environment.apiUrl}/api/contable/getCuentaCorriente/` + cliente, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  movimientosCliente(clienteId, page, size) {
    return this.http.get<any>(`${environment.apiUrl}/api/contable/getCuentaCorriente/movimietos/` + clienteId + '/' + page + '/' + size, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  ListaReciboCliente(page, size, clienteId) {
    return this.http.get<any>(`${environment.apiUrl}/api/contable/getRecibos/` + page + '/' + size + '/id/true/' + clienteId, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  generarMovCliente(mov) {
    return this.http.post<any>(`${environment.apiUrl}/api/contable/hacerMovCliente/`, mov, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  generarMovUserPersonal(mov) {
    return this.http.post<any>(`${environment.apiUrl}/api/contable/hacerMovimiento/`, mov, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  generarMovDeOtroUser(cajaId,mov) {
    return this.http.post<any>(`${environment.apiUrl}/api/contable/hacerMovimiento/`+cajaId, mov, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  generarPagoCliente(cliente, pago) {
    return this.http.post<any>(`${environment.apiUrl}/api/contable/ingresarPagoCliente/` + cliente, pago, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  traerCajaUsuario() {
    return this.http.get<any>(`${environment.apiUrl}/api/contable/getCajaAbierta`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  cerrarCajaIndividual(username) {
    return this.http.get<any>(`${environment.apiUrl}/api/contable/cerrarCajas/`+username, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  cerrarTodasLasCajas() {
    return this.http.get<any>(`${environment.apiUrl}/api/contable/cerrarCajas`, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  traerCajaDeOtroUsuario(username) {
    return this.http.get<any>(`${environment.apiUrl}/api/contable/getCajaOtroUser/`+username, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }
  listCajas(mov) {
    return this.http.post<any>(`${environment.apiUrl}/api/contable/searchCajas/`, mov, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

  traerCajaCerrada(username) {
    return this.http.get<any>(`${environment.apiUrl}/api/contable/getCajaDetalle/`+username, { headers: this.header }).pipe(
      map(data => {
        return data;
      })
    )
  }

}
