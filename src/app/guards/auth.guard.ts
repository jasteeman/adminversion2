import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  authService: AuthService
  
  constructor(public auth: AuthService, public router: Router) {
    this.authService = auth;
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) : boolean {
    var token = localStorage.getItem("token");
    if (token != null) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }

  }
}
