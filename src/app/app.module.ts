import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { PaginationModule,DatepickerModule, BsDatepickerModule,TypeaheadModule,ModalModule,TooltipModule,TimepickerModule  } from 'ngx-bootstrap';
import { ImageCompressService , ResizeOptions , ImageUtilityService } from 'ng2-image-compress';

//Calendario
import  { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

// Interceptors
import { AuthService } from './services/auth.service';

//Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ConteinerComponent } from './components/conteiner/conteiner.component';
import { HomeComponent } from './components/home/home.component';
import { LoaderFullScreenComponent } from './components/loader-full-screen/loader-full-screen.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { UsuariosListComponent } from './views/usuarios/usuarios-list/usuarios-list.component';
import { ClientesListComponent } from './views/clientes/clientes-list/clientes-list.component';
import { ClientesDetailsComponent } from './views/clientes/clientes-details/clientes-details.component';
import { ClientesCreateComponent } from './views/clientes/clientes-create/clientes-create.component';
import { UsuariosCreateComponent } from './views/usuarios/usuarios-create/usuarios-create.component';
import { UsuariosDetailsComponent } from './views/usuarios/usuarios-details/usuarios-details.component';
import { SearchClienteComponent } from './components/search/search-cliente/search-cliente.component';
import { SearchUsuarioComponent } from './components/search/search-usuario/search-usuario.component';
import { ProductosListComponent } from './views/productos/productos-list/productos-list.component';
import { ProductosCreateComponent } from './views/productos/productos-create/productos-create.component';
import { ProductosDetailsComponent } from './views/productos/productos-details/productos-details.component';
import { SearchProductoComponent } from './components/search/search-producto/search-producto.component';
import { ProveedoresListComponent } from './views/proveedores/proveedores-list/proveedores-list.component';
import { ProveedoresCreateComponent } from './views/proveedores/proveedores-create/proveedores-create.component';
import { ProveedoresDetailComponent } from './views/proveedores/proveedores-detail/proveedores-detail.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { GestionStockComponent } from './views/productos/gestion-stock/gestion-stock.component';
import { MovimientosStockComponent } from './views/productos/movimientos-stock/movimientos-stock.component';
import { ProductosPrecioComponent } from './views/productos/productos-precio/productos-precio.component';
import { PedidosCreateComponent } from './views/pedidos/pedidos-create/pedidos-create.component';
import { PedidosListComponent } from './views/pedidos/pedidos-list/pedidos-list.component';
import { PedidosDetailsComponent } from './views/pedidos/pedidos-details/pedidos-details.component';
import { FormPagosComponent } from './components/contable/form-pagos/form-pagos.component';
import { VentasCreateComponent } from './views/ventas/ventas-create/ventas-create.component';
import { VentasDetailsComponent } from './views/ventas/ventas-details/ventas-details.component';
import { VentasListComponent } from './views/ventas/ventas-list/ventas-list.component';
import { ClienteContableComponent } from './views/clientes/cliente-contable/cliente-contable.component';
import { ClienteRecibosComponent } from './views/clientes/cliente-recibos/cliente-recibos.component';
import { MovimientosClienteComponent } from './components/contable/movimientos-cliente/movimientos-cliente.component';
import { CajaPersonalComponent } from './views/contable/caja-personal/caja-personal.component';
import { CajaMaestraComponent } from './views/contable/caja-maestra/caja-maestra.component';
import { CajaHistorialComponent } from './views/contable/caja-historial/caja-historial.component';
import { MovimientosUsuarioComponent } from './components/contable/movimientos-usuario/movimientos-usuario.component';
import { CajaDetailsComponent } from './views/contable/caja-details/caja-details.component';
import { CajaCerradaComponent } from './views/contable/caja-cerrada/caja-cerrada.component';
import { FeriadoCreateComponent } from './views/feriados/feriado-create/feriado-create.component';
import { FeriadoListComponent } from './views/feriados/feriado-list/feriado-list.component';
import { FeriadoDetailsComponent } from './views/feriados/feriado-details/feriado-details.component';
import { SalasCreateComponent } from './views/salas/salas-create/salas-create.component';
import { SalasDetailsComponent } from './views/salas/salas-details/salas-details.component';
import { SalasListComponent } from './views/salas/salas-list/salas-list.component';
import { FichasCreateComponent } from './views/fichas/fichas-create/fichas-create.component';
import { FichasListComponent } from './views/fichas/fichas-list/fichas-list.component';
import { FichasDetailsComponent } from './views/fichas/fichas-details/fichas-details.component';
import { TareasCreateComponent } from './views/tareas/tareas-create/tareas-create.component';
import { TareasDetailsComponent } from './views/tareas/tareas-details/tareas-details.component';
import { TareasListComponent } from './views/tareas/tareas-list/tareas-list.component';
import { TareasPersonalComponent } from './views/tareas/tareas-personal/tareas-personal.component';
import { CategoriaCreateComponent } from './views/tratamientos/categoria-create/categoria-create.component';
import { CategoriaDetailsComponent } from './views/tratamientos/categoria-details/categoria-details.component';
import { CategoriaListComponent } from './views/tratamientos/categoria-list/categoria-list.component';
import { SearchTratamientoComponent } from './components/search/search-tratamiento/search-tratamiento.component';
import { TurnosListComponent } from './views/turnos/turnos-list/turnos-list.component';
import { TurnosCreateComponent } from './views/turnos/turnos-create/turnos-create.component';
import { ComprarTratamientoComponent } from './components/turnos/comprar-tratamiento/comprar-tratamiento.component';
import { FileUploadClinicComponent } from './components/file-upload-clinic/file-upload-clinic.component';
import { ClientesCreateModalComponent } from './views/clientes/clientes-create-modal/clientes-create-modal.component';
import { TurnosCalendarioComponent } from './views/turnos/turnos-calendario/turnos-calendario.component';
import { ClientesHistoriaClinicaComponent } from './views/clientes/clientes-historia-clinica/clientes-historia-clinica.component';
import { EstadisticasVentasComponent } from './views/estadisticas/estadisticas-ventas/estadisticas-ventas.component';
import { EstadisticasActivaBajaComponent } from './views/estadisticas/estadisticas-activa-baja/estadisticas-activa-baja.component';
import { ClientesCumplesComponent } from './views/clientes/clientes-cumples/clientes-cumples.component';
import { ConfiguracionesListComponent } from './views/configuraciones/configuraciones-list/configuraciones-list.component';
import { TurnosHistorialComponent } from './views/turnos/turnos-historial/turnos-historial.component';
import { AfipWSComponent } from './components/afip/afip-ws/afip-ws.component';
import { EstadisticasTurnosComponent } from './views/estadisticas/estadisticas-turnos/estadisticas-turnos.component';
import { CrearHistoriaComponent } from './components/turnos/crear-historia/crear-historia.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ConteinerComponent,
    HomeComponent,
    LoaderFullScreenComponent,
    NotFoundComponent,
    UsuariosListComponent,
    ClientesListComponent,
    ClientesDetailsComponent,
    ClientesCreateComponent,
    UsuariosCreateComponent,
    UsuariosDetailsComponent,
    SearchClienteComponent,
    SearchUsuarioComponent,
    ProductosListComponent,
    ProductosCreateComponent,
    ProductosDetailsComponent,
    SearchProductoComponent,
    ProveedoresListComponent,
    ProveedoresCreateComponent,
    ProveedoresDetailComponent,
    FileUploadComponent,
    GestionStockComponent,
    MovimientosStockComponent,
    ProductosPrecioComponent,
    PedidosCreateComponent,
    PedidosListComponent,
    PedidosDetailsComponent,
    FormPagosComponent,
    VentasCreateComponent,
    VentasDetailsComponent,
    VentasListComponent,
    ClienteContableComponent,
    ClienteRecibosComponent,
    MovimientosClienteComponent,
    CajaPersonalComponent,
    CajaMaestraComponent,
    CajaHistorialComponent,
    MovimientosUsuarioComponent,
    CajaDetailsComponent,
    CajaCerradaComponent,
    FeriadoCreateComponent,
    FeriadoListComponent,
    FeriadoDetailsComponent,
    SalasCreateComponent,
    SalasDetailsComponent,
    SalasListComponent,
    FichasCreateComponent,
    FichasListComponent,
    FichasDetailsComponent,
    TareasCreateComponent,
    TareasDetailsComponent,
    TareasListComponent,
    TareasPersonalComponent,
    CategoriaCreateComponent,
    CategoriaDetailsComponent,
    CategoriaListComponent,
    SearchTratamientoComponent,
    TurnosListComponent,
    TurnosCreateComponent,
    ComprarTratamientoComponent,
    FileUploadClinicComponent,
    ClientesCreateModalComponent,
    TurnosCalendarioComponent,
    ClientesHistoriaClinicaComponent,
    EstadisticasVentasComponent,
    EstadisticasActivaBajaComponent,
    ClientesCumplesComponent,
    ConfiguracionesListComponent,
    TurnosHistorialComponent,
    AfipWSComponent,
    EstadisticasTurnosComponent,
    CrearHistoriaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    AppBreadcrumbModule.forRoot(),
    PaginationModule.forRoot(),
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    TypeaheadModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    TimepickerModule.forRoot(),
    
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    ToastrModule.forRoot(),
    CarouselModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    BrowserAnimationsModule
  ],

  providers: [ImageCompressService,ResizeOptions,  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthService,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
