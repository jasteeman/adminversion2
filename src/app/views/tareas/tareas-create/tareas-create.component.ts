import { Component, OnInit } from '@angular/core';
import {  TareasService } from 'src/app/services/tareas.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute,Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-tareas-create',
  templateUrl: './tareas-create.component.html',
  styleUrls: ['./tareas-create.component.scss']
})
export class TareasCreateComponent implements OnInit {
  tarea={texto:null,fechaObjetivo:new Date()}
  load=false;
  usuarioId=null
  constructor(private route: ActivatedRoute, private tareaService: TareasService,private toastr: ToastrService,private router:Router,private formBuilder: FormBuilder) { }

  ngOnInit() {
  }
  TransportarUser(event: any) {
      this.usuarioId=event
  }
   //1.3-- Guardo los datos del cliente
   createTarea(){
    this.load=true
    // stop here if form is invalid
    if (this.tarea.texto==null) {
      this.load=false;
      this.toastr.info("Campos incompletos. Complete y vuelve a intentar");
        return;
    }
    if(navigator.onLine){
    //Guardo
    this.tareaService.saveOrUpdate(this.usuarioId,this.tarea).subscribe(data=>{
      if (data.type == "OK") {
         this.toastr.success('Tarea creada con éxito');
         this.load=false;
         this.router.navigate(['/panel/tareas/']);
       } else {
         console.log(data)
         this.load=false;
        this.toastr.info("ERROR N°"+data.errorNumber+":"+data.message);
        // mostrar error
      }
    })
    }else{
      this.load=false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }

  }
}
