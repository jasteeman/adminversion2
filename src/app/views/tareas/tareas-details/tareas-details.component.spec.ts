import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TareasDetailsComponent } from './tareas-details.component';

describe('TareasDetailsComponent', () => {
  let component: TareasDetailsComponent;
  let fixture: ComponentFixture<TareasDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TareasDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TareasDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
