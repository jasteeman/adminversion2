import { Component, OnInit, TemplateRef } from '@angular/core';
import { TareasService } from 'src/app/services/tareas.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-tareas-details',
  templateUrl: './tareas-details.component.html',
  styleUrls: ['./tareas-details.component.scss']
})
export class TareasDetailsComponent implements OnInit {
  tarea = { texto: null, fechaObjetivo: new Date() }
  id: number;
  sub: any;
  editMode = false;
  load = false;
  usuarioId = null;
  user = ""
  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;

  constructor(private modalService: BsModalService, private route: ActivatedRoute, private tareaService: TareasService, private toastr: ToastrService, private router: Router) { }


  ngOnInit() {
    this.editMode = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.loadTareaDetails();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  //1.1--Traigo el cliente 
  loadTareaDetails() {
    this.tareaService.getFull(this.id).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.tarea = data.content;
        this.user = "" + data.content.usuarioCompletado.nombre + "," + data.content.usuarioCompletado.apellido + "(" + data.content.usuarioCompletado.username + ")";
        this.tarea.fechaObjetivo = new Date(this.tarea.fechaObjetivo)
        this.usuarioId = data.content.usuarioCompletado.id
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.2--Habilitar y desabilitar form
  unlockEditMode() {
    this.editMode = true;
  }
  lockEditMode() {
    this.editMode = false;
  }
  TransportarUser(event: any) {
    this.usuarioId = event
    this.user = ""
  }

  //1.3-- Guardo los datos actualizados del cliente
  updateTarea() {
    this.load = true;
    if (navigator.onLine) {
      this.tareaService.saveOrUpdate(this.usuarioId, this.tarea).subscribe(data => {
        if (data.type == "OK") {
          this.editMode = false;
          this.load = false;
          this.toastr.success('Tarea actualizada con éxito');
          this.router.navigate(['/panel/tareas/']);
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
  //1.4-- Guardo los datos actualizados del cliente
  bajaTarea() {
    this.load = true;
    if (navigator.onLine) {
      this.tareaService.bajaTarea(this.id).subscribe(data => {
        if (data.type == "OK") {
          this.editMode = false;
          this.load = false;
          this.modalRef.hide()
          this.toastr.success('Tarea dada de baja con éxito');
          this.router.navigate(['/panel/tareas/']);
        } else {
          this.modalRef.hide()
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.modalRef.hide()
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
  //1.5-- Guardo los datos actualizados del cliente
  tareaCompleta() {
    this.load = true;
    if (navigator.onLine) {
      this.tareaService.completarTarea(this.id).subscribe(data => {
        if (data.type == "OK") {
          this.modalRef.hide()
          this.editMode = false;
          this.load = false;
          this.toastr.success('Tarea completada con éxito');
          this.router.navigate(['/panel/tareas/']);
        } else {
          this.modalRef.hide()
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.modalRef.hide()
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
  //1.6 modal
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,this.config);
  }
}
