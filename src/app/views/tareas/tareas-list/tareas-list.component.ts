import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { TareasService } from 'src/app/services/tareas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tareas-list',
  templateUrl: './tareas-list.component.html',
  styleUrls: ['./tareas-list.component.scss']
})
export class TareasListComponent implements OnInit {
  filtroPagination = {
    size: 20,
    page: 0,
    usuarioId: null,
    estado: null,
    desde: null,
    hasta: null
  }
  tempCliente = "";
  loading: Boolean;
  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
  tempTarea = null
  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0

  constructor(private router: Router, private tareaService: TareasService, private toastr: ToastrService, private modalService: BsModalService) {
  }
  ngOnInit() {
    this.loading = false;
    this.loadTareas();

  }

  //Lista de pedidos
  loadTareas() {
    this.loading = true;
    this.tareaService.searchTareas(this.filtroPagination).pipe(first()).subscribe(data => {
      this.loading = false;
      if (data.type == "OK") {
        this.items = data.content.content;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
        // this.resetFiltro()
      }
    })
  }
  //Paginacion
  pageChanged(event: any): void {
    this.filtroPagination.page = event.page - 1;
    this.items = [];
    this.loadTareas()
  }

  //Refresh lista
  refresh() {
    this.items = [];
    this.resetFiltro();
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.tempCliente = ""
    this.loadTareas();
  }

  //Busqueda Personalizada
  buscador() {
    this.filtroPagination.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadTareas();

  }

  //Limpiar campos de filtro
  resetFiltro() {
    this.filtroPagination = {
      size: 20,
      page: 0,
      usuarioId: null,
      estado: null,
      desde: null,
      hasta: null
    }
  }

  //Capturo estado de pedidos
  onPercentChange() {
    this.buscador()
  }

  //Borrado del pedido
  openModal(template: TemplateRef<any>, data) {
    this.tempTarea = data
    this.modalRef = this.modalService.show(template,this.config);
  }
  delete() {
    this.modalRef.hide()
    if (navigator.onLine) {
      this.tareaService.delete(this.tempTarea.id).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Tarea borrada con éxito ');
          this.loadTareas()
        } else {
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
  //Binding user
  TransportarUser(event: any) {
    this.filtroPagination.usuarioId = event;
    this.buscador()
  }

}
