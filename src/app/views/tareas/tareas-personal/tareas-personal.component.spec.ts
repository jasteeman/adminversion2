import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TareasPersonalComponent } from './tareas-personal.component';

describe('TareasPersonalComponent', () => {
  let component: TareasPersonalComponent;
  let fixture: ComponentFixture<TareasPersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TareasPersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TareasPersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
