import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { TareasService } from 'src/app/services/tareas.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-tareas-personal',
  templateUrl: './tareas-personal.component.html',
  styleUrls: ['./tareas-personal.component.scss']
})
export class TareasPersonalComponent implements OnInit {
  filtroPagination = {
    size: 20,
    page: 0,
    usuarioId: null,
    estado: "NUEVO",
    desde: null,
    hasta: null
  }
  load=false;
  tempCliente = null;
  loading: Boolean;
  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
  tempTarea = null
  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0

  constructor(private auth: AuthService,private router: Router, private tareaService: TareasService, private toastr: ToastrService, private modalService: BsModalService) {
  }
  ngOnInit() {
    this.loading = false;
    this.auth.getLoggedUser().pipe(first()).subscribe(
      data => {
        if (data.type == "OK") {
         this.tempCliente=data.content
         this.filtroPagination.usuarioId=this.tempCliente.id
         this.loadTareas();
        }
      })

  }

  //Lista de pedidos
  loadTareas() {
    this.loading = true;
    this.tareaService.searchTareas(this.filtroPagination).pipe(first()).subscribe(data => {
      this.loading = false;
      if (data.type == "OK") {
        this.items = data.content.content;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
        // this.resetFiltro()
      }
    })
  }
  //Paginacion
  pageChanged(event: any): void {
    this.filtroPagination.page = event.page - 1;
    this.items = [];
    this.loadTareas()
  }

  //Refresh lista
  refresh() {
    this.items = [];
    this.resetFiltro();
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.tempCliente = ""
    this.loadTareas();
  }

  //Busqueda Personalizada
  buscador() {
    this.filtroPagination.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadTareas();

  }

  //Limpiar campos de filtro
  resetFiltro() {
    this.filtroPagination = {
      size: 20,
      page: 0,
      usuarioId: null,
      estado:null,
      desde: null,
      hasta: null
    }
    this.filtroPagination.usuarioId=this.tempCliente.id
  }

  //Capturo estado de pedidos
  onPercentChange() {
    this.buscador()
  }

  //Borrado del pedido
  openModal(template: TemplateRef<any>, data) {
    this.tempTarea = data
    this.modalRef = this.modalService.show(template,this.config);
  }
  delete() {
    this.modalRef.hide()
    if (navigator.onLine) {
      this.tareaService.delete(this.tempTarea.id).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Tarea borrada con éxito ');
          this.loadTareas()
        } else {
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  //1.5-- Guardo los datos actualizados del cliente
  tareaCompleta() {
    this.load = true;
    if (navigator.onLine) {
      this.tareaService.completarTarea(this.tempTarea.id).subscribe(data => {
        if (data.type == "OK") {
          this.modalRef.hide()
          this.load = false;
          this.toastr.success('Tarea completada con éxito');
          this.refresh()
        } else {
          this.modalRef.hide()
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.modalRef.hide()
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
  //1.6 modal
  openModalOpciones(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,this.config);
  }

}
