import { Component, OnInit } from '@angular/core';
import { SalasService } from 'src/app/services/salas.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-salas-details',
  templateUrl: './salas-details.component.html',
  styleUrls: ['./salas-details.component.scss']
})
export class SalasDetailsComponent implements OnInit {
  sala = { nombre: null }
  id: number;
  sub: any;
  editMode = false;
  load = false;

  constructor(private route: ActivatedRoute, private salaService: SalasService, private toastr: ToastrService, private router: Router) { }


  ngOnInit() {
    this.editMode = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.loadSalaDetail();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  //1.1--Traigo el cliente 
  loadSalaDetail() {
    this.salaService.getFull(this.id).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.sala = data.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.2--Habilitar y desabilitar form
  unlockEditMode() {
    this.editMode = true;
  }
  lockEditMode() {
    this.editMode = false;
  }

  //1.3-- Guardo los datos actualizados del cliente
  updateSala() {
    this.load = true;
    if (navigator.onLine) {
      this.salaService.saveOrUpdate(this.sala).subscribe(data => {
        if (data.type == "OK") {
          this.editMode = false;
          this.load = false;
          this.toastr.success('Sala actualizada con éxito');
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
}
