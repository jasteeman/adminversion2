import { Component, OnInit } from '@angular/core';
import {  SalasService } from 'src/app/services/salas.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute,Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-salas-create',
  templateUrl: './salas-create.component.html',
  styleUrls: ['./salas-create.component.scss']
})
export class SalasCreateComponent implements OnInit {
  sala={nombre:null}
  load=false;
  constructor(private route: ActivatedRoute, private salaService: SalasService,private toastr: ToastrService,private router:Router,private formBuilder: FormBuilder) { }

  ngOnInit() {
  }
   //1.3-- Guardo los datos del cliente
   createSala(){
    this.load=true

    // stop here if form is invalid
    if (this.sala.nombre==null) {
      this.load=false;
      this.toastr.info("Campos incompletos. Complete y vuelve a intentar");
        return;
    }
    if(navigator.onLine){
    //Guardo
    this.salaService.saveOrUpdate(this.sala).subscribe(data=>{
      if (data.type == "OK") {
         this.toastr.success('Sala creada con éxito');
         this.load=false;
         this.router.navigate(['/panel/salas/list/']);
       } else {
         console.log(data)
         this.load=false;
        this.toastr.info("ERROR N°"+data.errorNumber+":"+data.message);
        // mostrar error
      }
    })
    }else{
      this.load=false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }

  }

}
