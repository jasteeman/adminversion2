import { Component, OnInit, TemplateRef,Input, Output } from '@angular/core';
import { SalasService } from 'src/app/services/salas.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router} from '@angular/router';

@Component({
  selector: 'app-salas-list',
  templateUrl: './salas-list.component.html',
  styleUrls: ['./salas-list.component.scss']
})
export class SalasListComponent implements OnInit {

   // Variables
   loading: Boolean;
   abrirForm: Boolean;
   salaId: any;
   salas : [];
 
   //Paginacion
   items = [];
   page = 0;
   pageSize = 20;
   totalElements = 0;
   totalPages = 0;
 
   //  Modal
   config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
 
   constructor(private route: Router,private salasService: SalasService, private toastr: ToastrService, private modalService: BsModalService) {
    }

  ngOnInit() {
    this.salas=[]
    this.loadsalas();
  }

  //1.1---Traer lista
  loadsalas() {
    this.loading = true;
    this.salasService.getSalas(this.page, this.pageSize).pipe(first()).subscribe(data => {
     if (data.type == "OK") {
        this.loading = false;
        this.salas = data.content.content;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  // 1.2 Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.salas = [];
    this.loadsalas()
  }

  //1.3 Refresh lista
  refresh() {
    this.salas = [];
    //Paginacion
    this.items = [];
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;

    this.loadsalas();
  }

  // ********************************************************************************************************
  //1.4 BORRAR CLIENTE
  confirm(data) {
    this.salaId = data
    this.abrirForm = true
  }
  volver() {
    this.modalRef.hide()
    this.abrirForm = false;
  }

  //Borrar id
  delete() {
    this.salasService.delete(this.salaId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.salas = [];
        //Paginacion
        this.items = [];
        this.page = 0;
        this.pageSize = 20;
        this.totalElements = 0;
        this.totalPages = 0;

        this.loadsalas();
        this.abrirForm = false;
        this.toastr.success('Sala eliminada con éxito');
        this.modalRef.hide()
      } else {
        // mostrar error
      }
    });
  }

  openModal(template: TemplateRef<any>, data) {
    this.salaId = data
    this.modalRef = this.modalService.show(template,this.config);
  }

}
