import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { EstadisticasService } from 'src/app/services/estadisticas.service';
import { Router } from '@angular/router';
import { TypeaheadMatch, PageChangedEvent } from 'ngx-bootstrap';
import { ClienteService } from 'src/app/services/cliente.service';
import * as moment from 'moment';

@Component({
  selector: 'app-estadisticas-turnos',
  templateUrl: './estadisticas-turnos.component.html',
  styleUrls: ['./estadisticas-turnos.component.scss']
})
export class EstadisticasTurnosComponent implements OnInit {

  fecha = {
    desde: new Date(),
    hasta: new Date()
  }
  items = [];
  load = false;
  loading = false;
  movimientos = [];

  filtro = {
    cancelados: [],
    pendientes: [],
    terminados: []
  }


  //Paginacion
  page = 0;
  pageSize = 3;
  totalElements = 0;
  totalPages = 0;
  constructor(private router: Router, private estadisticasService: EstadisticasService, private clienteService: ClienteService, private toastr: ToastrService, private modalService: BsModalService) { }

  ngOnInit() {
  }

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;
  tempArray = [];
  //Lista de turnos
  buscador() {
    this.loading = true;
    this.items = [];
    this.movimientos = [];
    var items = [];
    var list = [];
    this.estadisticasService.turnos(this.fecha).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.movimientos = data.content;
        this.items = this.movimientos.slice(this.page, 10);
        this.loading = false;
      }
    })
  }


  //Paginacion
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.items = this.movimientos.slice(startItem, endItem);
  }
  // **************************************************************************************************************
  // RETORNOS DE TOTALES
  calculo = (data) => {
    var nacimiento = moment(data);
    var hoy = moment();
    var anios = hoy.diff(nacimiento, "years");
    return anios
  }

  openModal(template: TemplateRef<any>, data) {
    console.log(data);
    this.filtro.cancelados = data;
    this.modalRef = this.modalService.show(template, this.config);
  }

}
