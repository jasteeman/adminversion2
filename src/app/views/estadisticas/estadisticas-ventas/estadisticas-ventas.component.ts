import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { ClienteService } from 'src/app/services/cliente.service';
import { VentasService } from 'src/app/services/ventas.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-estadisticas-ventas',
  templateUrl: './estadisticas-ventas.component.html',
  styleUrls: ['./estadisticas-ventas.component.scss']
})
export class EstadisticasVentasComponent implements OnInit {
  //Variables
  filtroPagination = {
    desde: null,
    hasta: null
  }
  resultados = { precioCosto: 0, venta: 0 };
  loading: Boolean;

  //Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;
  tempArray = [];

  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  chart = []
  filteredCategories = [];

  //Charts
  pieChartLabels: string[] = [];
  pieChartData: number[] = [];
  pieChartType: string = 'pie';
  pieChartOptions: any = {
    'backgroundColor': [
      "#FF6384",
      "#4BC0C0",
      "#FFCE56",
      "#E7E9ED",
      "#36A2EB"
    ]
  }

  constructor(private router: Router, private ventaService: VentasService, private clienteService: ClienteService, private toastr: ToastrService, private modalService: BsModalService) {
  }

  ngOnInit() {
    this.loading = false;
  }
refresh(){
  this.filtroPagination.hasta=null;
  this.filtroPagination.desde=null;
}

  //Lista de cmv
  cmv() {
    this.loading = true;
    if (this.filtroPagination.desde == null || this.filtroPagination.hasta == null) {
      this.toastr.error("Debe ingresar un tramo de fecha para continuar. De lo contrario no podrá avanzar.")
      return;
    }
    this.resultados.precioCosto = 0;
    this.resultados.venta = 0;
    this.listadoMensual()
    this.ventaService.cmv(this.filtroPagination.desde, this.filtroPagination.hasta).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.tempArray = data.content
        this.loading = false;
      }
      this.calcularCMV()
    })
  }

  //Calcular CMV
  calcularCMV() {
    this.tempArray.forEach(element => {
      element.items.forEach(u => {
        var costo = (u.producto.precioCosto * u.producto.iva / 100) + u.producto.precioCosto;
        this.resultados.precioCosto += u.cantidad * costo;
      });
      this.resultados.venta += element.totalNeto
    })
  }

  //Listado mensual de ventas
  listadoMensual() {
    this.ventaService.listadoMensual(this.filtroPagination.desde, this.filtroPagination.hasta).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        var filtro = [];
        filtro = data.content;
        this.chart=filtro;
        filtro.forEach(element => {
          if (element != null) {
            this.pieChartLabels.push("" + element.mes + "/" + element.ANIO)
            this.pieChartData.push(element.total)
          }
        });
      }
    })
  }

}
