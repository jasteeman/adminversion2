import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadisticasActivaBajaComponent } from './estadisticas-activa-baja.component';

describe('EstadisticasActivaBajaComponent', () => {
  let component: EstadisticasActivaBajaComponent;
  let fixture: ComponentFixture<EstadisticasActivaBajaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadisticasActivaBajaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadisticasActivaBajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
