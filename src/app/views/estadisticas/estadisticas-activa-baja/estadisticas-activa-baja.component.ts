import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { EstadisticasService } from 'src/app/services/estadisticas.service';
import { Router } from '@angular/router';
import { TypeaheadMatch, PageChangedEvent } from 'ngx-bootstrap';
import { ClienteService } from 'src/app/services/cliente.service';
import * as moment from 'moment';

@Component({
  selector: 'app-estadisticas-activa-baja',
  templateUrl: './estadisticas-activa-baja.component.html',
  styleUrls: ['./estadisticas-activa-baja.component.scss']
})
export class EstadisticasActivaBajaComponent implements OnInit {

  fecha = {
    desde: new Date(),
    hasta: new Date(),
    tipo: "ACTIVAS",
    clienteId: null,
    tratamientoId: null
  }
  totalTerminadas = false;
  mostrartabla = false;
  items = [];
  load = false;
  loading = false;
  sesionesItems=[];
  movimientos = [];

   //limpiar todo
   refresh(){
   this.fecha = {
    desde: new Date(),
    hasta: new Date(),
    tipo: "ACTIVAS",
    clienteId: null,
    tratamientoId: null
  }
    this.items = []
}

    //Paginacion
    page = 0;
    pageSize = 3;
    totalElements = 0;
    totalPages = 0;

  constructor(private router: Router, private estadisticasService: EstadisticasService, private clienteService: ClienteService, private toastr: ToastrService, private modalService: BsModalService) { }

  ngOnInit() {
  }
    //  Modal
    config = {
      ignoreBackdropClick: true
    };                                 
   modalRef: BsModalRef;
    tempArray=[];

  //Cambios de estados
  onOptionsSelected(value) {
    this.fecha.tipo = value;
    this.buscador()
  }

  onOptionsSelectedValue(value) {
    if (value == "true") {
      this.totalTerminadas = true;
    } else {
      this.totalTerminadas = false;
    }
    this.buscador();
  }

  //Binding Cliente
  TransportarCliente(event: any) {
    this.fecha.clienteId = event;
    this.buscador();
  }

  //Binding Tratamiento
  TransportarTratamiento(event: any) {
    this.fecha.tratamientoId = event;
    this.buscador();
  }

  //Lista de turnos
  buscador() {
    this.loading = true;
    this.items = [];
    this.movimientos=[];
    var items = [];
    var list=[];
    this.estadisticasService.activasBajas(this.fecha).pipe(first()).subscribe(data => {
      // 
      if (data.type == "OK") {
        var temp = {}
        if (this.fecha.tipo == "ACTIVAS") {
          data.content.forEach(x => {
            x.tratamientosComprados.forEach(y => {
              if (y.enabled != false) {
                temp = { cliente: x.cliente, saldo: x.saldo, tratamientosComprados: y }
                items.push(temp)
                temp = {};
              }
            });
          });

          // Filtros
          if (this.totalTerminadas != false) {
            console.log("activas con sesiones terminadas")
            var contarTerminadas = 0;
            items.forEach(x => {
              contarTerminadas = ((x.tratamientosComprados.sesiones.length) - (this.calcularBajaSesiones(x.tratamientosComprados.sesiones)) - (this.calculoSesionesSinTurno(x.tratamientosComprados.sesiones)) - (this.calculoSesiones(x.tratamientosComprados.sesiones)))
              if (x.tratamientosComprados.sesiones.length == contarTerminadas) {
                list.push(x)
                contarTerminadas = 0
              }
            })
            this.movimientos = list;
            this.items = this.movimientos.slice(this.page, 10);
            this.loading = false;
          } else {
            console.log("activas con sesiones")
            var contarTerminadas = 0;
            items.forEach(x => {
              contarTerminadas = ((x.tratamientosComprados.sesiones.length) - (this.calcularBajaSesiones(x.tratamientosComprados.sesiones)) - (this.calculoSesionesSinTurno(x.tratamientosComprados.sesiones)) - (this.calculoSesiones(x.tratamientosComprados.sesiones)))
              if (x.tratamientosComprados.sesiones.length != contarTerminadas) {
                list.push(x)
                contarTerminadas = 0
              }
            })
            this.movimientos = list;
            this.items = this.movimientos.slice(this.page, 10);
            this.loading = false;
          }
        }
        if (this.fecha.tipo == "BAJAS") {
          data.content.forEach(x => {
            x.tratamientosComprados.forEach(y => {
              if (y.enabled != true) {
                temp = { cliente: x.cliente, saldo: x.saldo, tratamientosComprados: y }
                list.push(temp)
                temp = {};
              }
            });
            this.movimientos = list;
            this.items = this.movimientos.slice(this.page, 10);
            this.loading = false;
          });
         
        }
      }
    })
  }

  
  //Paginacion
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.items = this.movimientos.slice(startItem, endItem);
  }


  // **************************************************************************************************************
  // RETORNOS DE TOTALES
  calculoSesiones = (data) => {
    var tempCantidad = 0;
    data.forEach(x => {
      if (x.enabled == true && x.completa == false && x.turnoId != null) {
        tempCantidad += 1
      }
    })
    return tempCantidad;
  }

  calcularBajaSesiones = (data) => {
    var tempCantidad = 0;
    data.forEach(x => {
      if (x.enabled == false && x.completa == false) {
        tempCantidad += 1
      }
    })
    return tempCantidad;
  }

  calculoSesionesSinTurno = (data) => {
    var tempCantidad = 0;
    data.forEach(x => {
      if (x.enabled == true && x.completa == false && x.turnoId == null) {
        tempCantidad += 1
      }
    })
    return tempCantidad;
  }

  calculoGanancia = (data) => {
    var tempCantidad = 0;
    data.forEach(x => {
      if (x.enabled != true && x.completa != false) {
        tempCantidad += x.precio
      }
    })
    return tempCantidad;
  }

  calculoSesionesBaja = (data) => {
    var tempCantidad = 0;
    data.forEach(x => {
      if (x.enabled == false && x.completa == false) {
        tempCantidad += 1
      }
    })
    return tempCantidad;
  }

  // **************************************************************************************************************
  // RETORNOS DE TOTALES
  calculo = (data) => {
    var nacimiento = moment(data);
    var hoy = moment();
    var anios = hoy.diff(nacimiento, "years");
    return anios
  }

  openModal(template: TemplateRef<any>, data) {
    this.sesionesItems = data
    this.modalRef = this.modalService.show(template,this.config);
  }
}
