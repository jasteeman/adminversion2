import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CajaPersonalComponent } from './caja-personal.component';

describe('CajaPersonalComponent', () => {
  let component: CajaPersonalComponent;
  let fixture: ComponentFixture<CajaPersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CajaPersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaPersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
