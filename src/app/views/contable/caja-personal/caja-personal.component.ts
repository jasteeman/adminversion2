import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ContableService } from 'src/app/services/contable.service';
import { first } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-caja-personal',
  templateUrl: './caja-personal.component.html',
  styleUrls: ['./caja-personal.component.scss']
})
export class CajaPersonalComponent implements OnInit {
  movimientos = [];
  loading: Boolean;
  load = false;
  listaPago: any;
  desde = new Date();
  hasta = new Date();
  usuario = { saldo: null, data: null };
  cuentas = { EFECTIVO: 0, DEBITO: 0, CREDITO: 0, CHEQUE: 0, OTRO: 0 }
  cuentasFiltradas = { EFECTIVO: 0, DEBITO: 0, CREDITO: 0, CHEQUE: 0, OTRO: 0 }
  resultados = { ingresos: 0, egresos: 0 }
  resultadosFiltrados = { ingresos: 0, egresos: 0 }


  //Paginacion
  items = [];
  page = 0;
  pageSize = 3;
  totalElements = 0;
  totalPages = 0;

  //Filtros de fecha
  greaterTen = [];
  items2=[];
  tablamov = false;

  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;

  constructor(private toastr: ToastrService, private contableService: ContableService, private modalService: BsModalService) { }

  ngOnInit() {
    this.loadmovimientos()
  }

  //Filtro de fechas
  filtrar() {
    this.tablamov = true;
    this.greaterTen = []
    var a = this.desde;
    var b = this.hasta;
    this.items2=this.movimientos
    for (let i = 0; i < this.items2.length; i++) {
      var currentNumber = this.items2[i].fecha;
      if (currentNumber >= a && currentNumber <= b) {
        this.greaterTen.push(this.items2[i])
      }
    }
    // *****************************************************************************************
    this.cuentasFiltradas.CHEQUE=0;
    this.cuentasFiltradas.CREDITO=0;
    this.cuentasFiltradas.DEBITO=0;
    this.cuentasFiltradas.EFECTIVO=0;
    this.cuentasFiltradas.OTRO=0;
    this.resultadosFiltrados.egresos=0;
    this.resultadosFiltrados.ingresos=0;
    // LOGISTICA
    this.greaterTen.forEach(x => {
      if (x.formaPago == "OTRO") {
       this.cuentasFiltradas.OTRO += x.monto
        if (x.monto > 0) {
          this.resultadosFiltrados.ingresos += x.monto
        } else
        this.resultadosFiltrados.egresos += x.monto
    }
    if (x.formaPago == "EFECTIVO") {
      this.cuentasFiltradas.EFECTIVO += x.monto
        if (x.monto > 0) {
          this.resultadosFiltrados.ingresos += x.monto
        } else
        this.resultadosFiltrados.egresos += x.monto
    }
    if (x.formaPago == "DEBITO") {
      this.cuentasFiltradas.DEBITO += x.monto
        if (x.monto > 0) {
          this.resultadosFiltrados.ingresos += x.monto
        } else
        this.resultadosFiltrados.egresos += x.monto
    }
    if (x.formaPago == "CHEQUE") {
      this.cuentasFiltradas.CHEQUE += x.monto
        if (x.monto > 0) {
          this.resultadosFiltrados.ingresos += x.monto
        } else
        this.resultadosFiltrados.egresos += x.monto
    }
    if (x.formaPago == "CREDITO") {
      this.cuentasFiltradas.CREDITO += x.monto
        if (x.monto > 0) {
          this.resultadosFiltrados.ingresos += x.monto
        } else
        this.resultadosFiltrados.egresos += x.monto
    }
    })
    this.items2 = this.greaterTen.slice(0, 10);
  }

  refresh() {
    this.tablamov = false;
    this.greaterTen = []
    this.desde = null;
    this.hasta = null;
    this.loadmovimientos()
  }

  //Lista de caja
  loadmovimientos() {
    this.loading = true;
    this.contableService.traerCajaUsuario().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.usuario.data = data.content.usuario;
        this.usuario.saldo = data.content.saldo
        this.loading = false;
        this.movimientos = data.content.movimientos
        this.items = this.movimientos.slice(this.page, 10);
         // *****************************************************************************************
        this.cuentas.CHEQUE=0;
        this.cuentas.CREDITO=0;
        this.cuentas.DEBITO=0;
        this.cuentas.EFECTIVO=0;
        this.cuentas.OTRO=0;
        this.resultados.egresos=0;
        this.resultados.ingresos=0;
        // LOGISTICA
        this.movimientos.forEach(x => {
          if (x.formaPago == "OTRO") {
          this.cuentas.OTRO += x.monto
            if (x.monto > 0) {
              this.resultados.ingresos += x.monto
            } else
            this.resultados.egresos += x.monto
        }
        if (x.formaPago == "EFECTIVO") {
          this.cuentas.EFECTIVO += x.monto
            if (x.monto > 0) {
              this.resultados.ingresos += x.monto
            } else
            this.resultados.egresos += x.monto
        }
        if (x.formaPago == "DEBITO") {
          this.cuentas.DEBITO += x.monto
            if (x.monto > 0) {
              this.resultados.ingresos += x.monto
            } else
            this.resultados.egresos += x.monto
        }
        if (x.formaPago == "CHEQUE") {
          this.cuentas.CHEQUE += x.monto
            if (x.monto > 0) {
              this.resultados.ingresos += x.monto
            } else
            this.resultados.egresos += x.monto
        }
        if (x.formaPago == "CREDITO") {
          this.cuentas.CREDITO += x.monto
            if (x.monto > 0) {
              this.resultados.ingresos += x.monto
            } else
            this.resultados.egresos += x.monto
        }
        })

      } else {
        this.loading = false;
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    
    });
  }

  //Paginacion
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.items = this.movimientos.slice(startItem, endItem);
  }
  pageChangedFecha(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.items = this.greaterTen.slice(startItem, endItem);
  }

  // Bindings
  TransportarMovimiento(event) {
    this.load = true;
    var temp = event;
    if (navigator.onLine) {
      this.contableService.generarMovUserPersonal(temp).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Movimiento generado con éxito');
          this.load = false;
          this.modalRef.hide();
          this.page = 0;
          this.pageSize = 3;
          this.totalElements = 0;
          this.totalPages = 0;
          this.movimientos = [];
          this.usuario.saldo = 0;
          this.loadmovimientos()
        } else {
          this.load = false;
          this.modalRef.hide();
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }

  }

  // Modales
  openModalMovimiento(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,this.config);
  }

}
