import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CajaMaestraComponent } from './caja-maestra.component';

describe('CajaMaestraComponent', () => {
  let component: CajaMaestraComponent;
  let fixture: ComponentFixture<CajaMaestraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CajaMaestraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaMaestraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
