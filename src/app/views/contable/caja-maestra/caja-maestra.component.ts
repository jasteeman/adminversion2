import { Component, OnInit, TemplateRef } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { AuthService } from 'src/app/services/auth.service';
import { ContableService } from 'src/app/services/contable.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Usuario } from 'src/app/models/usuario';
import { first } from "rxjs/operators";
import { Router } from '@angular/router';

@Component({
  selector: 'app-caja-maestra',
  templateUrl: './caja-maestra.component.html',
  styleUrls: ['./caja-maestra.component.scss']
})
export class CajaMaestraComponent implements OnInit {
  usuarios: Array<Usuario> = []
  loading: Boolean;
  usuarioId: any;
  abrirForm: Boolean;
  searchText: Boolean;
  user: any;
  usuarioTemp: any
  load = false;
  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0

  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;

  constructor(private contableservice: ContableService, private route: Router, private authService: AuthService, private usuarioService: UsuarioService, private toastr: ToastrService, private modalService: BsModalService) {
  }

  ngOnInit() {
    this.searchText = false;
    this.userLogueado();
    this.loadUsers();
  }

  userLogueado() {
    this.authService.getLoggedUser().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.user = data.content
      }
    })
  }

  loadUsers() {
    this.loading = true;
    this.usuarioService.getUsuarios(this.page, this.pageSize).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.usuarios = data.content.content;
        this.loading = false;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  //Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.usuarios = [];
    this.loadUsers()
  }

  //1.3 Refresh lista
  refresh() {
    this.usuarios = [];
    //Paginacion
    this.items = [];
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;

    this.loadUsers();
  }

  //Caja de texto busqueda
  mostrar() {
    this.searchText = true
  }
  ocultar() {
    this.searchText = false;
  }

  // ********************************************************************************************************
  //1.4 BORRAR CLIENTE
  confirm(data) {
    this.usuarioId = data
    this.abrirForm = true
  }
  volver() {
    this.modalRef.hide()
    this.abrirForm = false;
  }

  //Cerrar caja individual
  delete() {
    this.load = true
    if (navigator.onLine) {
      // Traigo el usuario
      this.usuarioService.getFull(this.usuarioId).pipe(first()).subscribe(data => {
        if (data.type == "OK") {
          this.usuarioTemp = data.content;
          this.contableservice.cerrarCajaIndividual(data.content.username).pipe(first()).subscribe(data => {
            if (data.type == "OK") {
              this.load = false;
              this.usuarios = [];
              this.items = [];
              this.page = 0;
              this.pageSize = 20;
              this.totalElements = 0;
              this.totalPages = 0;
              this.loadUsers();
              this.abrirForm = false;
              this.modalRef.hide()
              this.toastr.success("Caja transferida correctamente");
            } else {
              this.abrirForm = false;
              this.modalRef.hide()
              this.load = false;
              this.toastr.info("" + data.type + "." + data.message);
            }
          }, error => {
            // mostrar error
            if (error.status == 401) {
              this.load = false
              this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
            }
          });
        } else {
          // mostrar error
        }
      }, error => {
        // mostrar error
        if (error.status == 401) {
          this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
        }
      });

    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  //Cerrar caja de todos los users
  deleteAll() {
    this.load = true
    if (navigator.onLine) {
      this.contableservice.cerrarTodasLasCajas().pipe(first()).subscribe(data => {
        if (data.type == "OK") {
          this.load = false;
          this.usuarios = [];
          this.items = [];
          this.page = 0;
          this.pageSize = 20;
          this.totalElements = 0;
          this.totalPages = 0;
          this.loadUsers();
          this.abrirForm = false;
          this.modalRef.hide()
          this.toastr.success("Cajas transferidas correctamente");
        } else {
          this.abrirForm = false;
          this.modalRef.hide()
          this.load = false;
          this.toastr.info("" + data.type + "." + data.message);
        }
      }, error => {
        // mostrar error
        if (error.status == 401) {
          this.load = false
          this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
        }
      });
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  openModal(template: TemplateRef<any>, data) {
    this.usuarioId = data
    this.modalRef = this.modalService.show(template,this.config);
  }

  openModalAll(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,this.config);
  }

  Transportar(event: any) {
    // Traigo el usuario
        this.route.navigate(['/panel/contable/details/' + event]);
  }
}
