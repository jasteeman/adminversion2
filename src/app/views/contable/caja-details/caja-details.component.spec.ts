import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CajaDetailsComponent } from './caja-details.component';

describe('CajaDetailsComponent', () => {
  let component: CajaDetailsComponent;
  let fixture: ComponentFixture<CajaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CajaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
