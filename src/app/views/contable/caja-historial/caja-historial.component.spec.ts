import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CajaHistorialComponent } from './caja-historial.component';

describe('CajaHistorialComponent', () => {
  let component: CajaHistorialComponent;
  let fixture: ComponentFixture<CajaHistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CajaHistorialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaHistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
