import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ContableService } from 'src/app/services/contable.service';
import { first } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-caja-historial',
  templateUrl: './caja-historial.component.html',
  styleUrls: ['./caja-historial.component.scss']
})
export class CajaHistorialComponent implements OnInit {
  filtroPagination = {
    usuarioId: null,
    page:0,
    size:20,
    estado:null,
    desde:null,
    hasta:null
  }
  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0
  loading=false;
constructor(private toastr: ToastrService, private contableService: ContableService, private modalService: BsModalService) { }

  ngOnInit() {
    this.loadCajas()
  }

  loadCajas(){
    this.loading = true;
    this.contableService.listCajas(this.filtroPagination).pipe(first()).subscribe(data => {
       if (data.type == "OK") {
         this.loading = false;
         this.items = data.content.content;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages

      } else {
        this.loading = false;
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    
    });
  }

  //Paginacion
  pageChanged(event: any): void {
    this.filtroPagination.page = event.page - 1;
    this.items = [];
    this.loadCajas()
  }

  //Refresh lista
  refresh() {
    this.items = [];
    this.resetFiltro();
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadCajas();
  }

  //Busqueda Personalizada
  buscador() {
    this.filtroPagination.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadCajas();

  }

  //Limpiar campos de filtro
  resetFiltro() {
    this.filtroPagination = {
      usuarioId: null,
      page:0,
      size:20,
      estado:null,
      desde:null,
      hasta:null
    }
  }

  Transportar(event: any) {
    // Traigo el usuario
        this.filtroPagination.usuarioId=event;
        this.buscador()
  }

}
