import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ContableService } from 'src/app/services/contable.service';
import { first } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-cliente-contable',
  templateUrl: './cliente-contable.component.html',
  styleUrls: ['./cliente-contable.component.scss']
})
export class ClienteContableComponent implements OnInit {
  movimientos=[];
  loading: Boolean;
  clienteId: any;
  load=false;
  listaPago:any;

  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0;
  saldoValor=null;
  temp=""

    //  Modal
    config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;

  constructor(private toastr: ToastrService,private contableService:ContableService, private modalService: BsModalService) { }

  ngOnInit() {
  }
  report = () => {
    if(this.temp==''){
      this.toastr.error("Debe seleccionar un cliente para continuar");
      return;
    }
    window.open(`${environment.apiUrl}` + '/report/resumenCtaCte/' + this.clienteId, '_blank');
}

reportDeudas = () => {
  window.open(`${environment.apiUrl}` + '/report/resumenCtaCteTotales/', '_blank');
}

  loadmovimientos() {
    this.loading = true;
    this.contableService.movimientosCliente(this.clienteId,this.page,this.pageSize).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.movimientos = data.content.content;
        this.saldoCliente()
        this.loading = false;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        this.loading = false;
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  //Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.movimientos = [];
    this.loadmovimientos()
  }

  Transportar(event: any) {
  this.clienteId=event;
  this.movimientos=[];
  this.temp=""
  this.page = 0;
  this.pageSize = 20;
  this.totalElements = 0;
  this.totalPages = 0;
  this.saldoCliente()
  this.loadmovimientos();
  }

  saldoCliente(){
    this.contableService.saldoCliente(this.clienteId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
       this.saldoValor=data.content;
       this.temp=""+this.saldoValor.cliente.nombre+" "+this.saldoValor.cliente.apellido+"(SALDO:$"+this.saldoValor.saldo+")";
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  TransportarMovimiento(event) {
    this.load=true;
    event.clienteId=this.clienteId;
    var temp=event;
    if (navigator.onLine) {
      this.contableService.generarMovCliente(temp).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Movimiento generado con éxito');
          this.load=false;
          this.modalRef.hide();
          this.temp=""
          this.page = 0;
          this.pageSize = 20;
          this.totalElements = 0;
          this.totalPages = 0;
          this.movimientos = [];
          this.loadmovimientos()
        } else {
          this.load = false;
          this.modalRef.hide();
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }

}
TransportarPago(event) {
this.listaPago=event
}

createPago(){
  this.load=true;
  if (navigator.onLine) {
    console.log(this.listaPago)
    this.contableService.generarPagoCliente(this.clienteId,this.listaPago).subscribe(data => {
      if (data.type == "OK") {
        this.toastr.success('Pago generado con éxito');
        this.load=false;
        this.modalRef.hide()
      } else {
        this.load = false;
        this.modalRef.hide();
        this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
        // mostrar error
      }
    })
    this.temp=""
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.movimientos = [];
    this.loadmovimientos()
  }
  else {
    this.load = false;
    this.toastr.info("Sin Conexion a internet. Intente nuevamente");
  }
}

  // Modales
  openModalMovimiento(template: TemplateRef<any>) {
    // this.productos = data
    if(this.temp==''){
      this.toastr.error("Debe seleccionar un cliente para continuar");
      return;
    }
    this.modalRef = this.modalService.show(template,this.config);
  }
  openModalPago(template: TemplateRef<any>) {
    if(this.temp==''){
      this.toastr.error("Debe seleccionar un cliente para continuar");
      return;
    }
    this.modalRef = this.modalService.show(template,this.config);
  }
}
