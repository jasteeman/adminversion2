import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteContableComponent } from './cliente-contable.component';

describe('ClienteContableComponent', () => {
  let component: ClienteContableComponent;
  let fixture: ComponentFixture<ClienteContableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteContableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteContableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
