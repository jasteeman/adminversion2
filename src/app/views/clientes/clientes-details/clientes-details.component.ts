import { Component, OnInit, OnDestroy } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import data from 'src/app/models/localidades';
import {ClienteCreate } from 'src/app/models/cliente';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { AfipService } from 'src/app/services/afip.service';


@Component({
  selector: 'app-clientes-details',
  templateUrl: './clientes-details.component.html',
  styleUrls: ['./clientes-details.component.scss']
})
export class ClientesDetailsComponent implements OnInit {
  cliente=new ClienteCreate();
  id: number;
  sub: any;
  editMode = false;
  localidades=data.data;
  estadosCiviles= [{nombre:"Soltero/a"},{nombre:"Casado/a"},{nombre:"Divorciado/a"},{nombre:"Viudo/a"}];
  ciudades:any;
  load=false;
  
  //AFIP
  tipoDeDocumentos=[];
  condicionesFrenteAlIva=[];
  tipoDeFacturas=[];
  tipoFactura=null;
  tipoDocumento=null;
  tipoIVA=null;
  condicionFiscal=null;

  constructor( private afipService:AfipService,private route: ActivatedRoute, private clienteService: ClienteService,private toastr: ToastrService) { }

  ngOnInit() {
    this.editMode=false;
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    // this.traerCondicionFrenteAlIva();
    // this.traerTipoDeDocumentos();
    // this.traerTipoDeFacturas();
    // this.traerTipoDeIvas();
    this.loadClienteDetail();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  //1.1--Traigo el cliente 
  loadClienteDetail() {
    this.clienteService.getFull(this.id).pipe(first()).subscribe(data => {
       if (data.type == "OK") {
        var temp= data.content.fechaNacimiento;
         data.content.fechaNacimiento=new Date(temp)
        this.cliente = data.content;
        if(data.content.tipoFactura!=null){
          this.tipoFactura=data.content.tipoFactura.descrWs;
        }
        if(data.content.tipoDocumento!=null){
          this.tipoDocumento=data.content.tipoDocumento.nombreTipoDocumento;
        }
        if(data.content.condicionFrenteAlIva!=null){
          this.condicionFiscal=data.content.condicionFrenteAlIva.nombre;
        }
        this.traerLocalidades(this.cliente.provincia);
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
 
  //1.2--Habilitar y desabilitar form
  unlockEditMode() {
    this.editMode = true;
  }
  lockEditMode() {
    this.editMode = false;
  }

  //1.3-- Guardo los datos actualizados del cliente
  updateCliente(){
    this.load=true;
    if (navigator.onLine) {
      // this.buscarTipoFactura()
      // this.buscarTipoDocumento()
      // this.buscarCondicionFiscal()
    this.clienteService.saveOrUpdate(this.cliente).subscribe(data=>{
      if (data.type == "OK") {
        this.editMode=false;
        this.load=false;
        this.toastr.success('Cliente editado con éxito');
       } else {
        this.load=false;
        this.toastr.info("ERROR N°"+data.errorNumber+":"+data.message);
        // mostrar error
      }
    })}
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

   //1.4--Traigo las ciudades de la provincia
  traerLocalidades(data){
    this.localidades.forEach(element => {
      if(element.nombre==data){
        this.ciudades=element.ciudades;
      }
    });
  }
  onSelect(event: TypeaheadMatch): void {
    this.ciudades = event.item.ciudades;
  }
  onSelectFactura(event: TypeaheadMatch): void {
    this.tipoFactura = event.value;
  }


// ******************************************************************************************************
  // AFIP
//   traerTipoDeDocumentos(){
//    this.afipService.getTipoDeDocumentos().subscribe(data=>{
//     this.tipoDeDocumentos=data.content.content;
//    })
//   }
//   traerTipoDeIvas(){
//     this.afipService.getTiposDeIvas().subscribe(data=>{
//      this.traerTipoDeIvas=data.content.content;
//     })
//    }
//    traerTipoDeFacturas(){
//     this.afipService.getTiposDeFacturas().subscribe(data=>{
//       this.tipoDeFacturas=data.content.content;
//      })
//    }
//    traerCondicionFrenteAlIva(){
//     this.afipService.getCondicionDeIva().subscribe(data=>{
//       this.condicionesFrenteAlIva=data.content.content;
//      })
//    }

//    //Retornar objetos para afip
//    buscarTipoFactura(){
//      if(this.tipoFactura!=null){
//       this.tipoDeFacturas.forEach(x=>{
//         if(this.tipoFactura==x.descrWs){
//           this.cliente.tipoFactura=x;
//         }
//       })
//      }
//   }
//   buscarTipoDocumento(){
//     if(this.tipoDocumento!=null){
//      this.tipoDeDocumentos.forEach(x=>{
//        if(this.tipoDocumento==x.descrWs){
//          this.cliente.tipoDocumento=x;
//        }
//      })
//     }
//  }
//  buscarCondicionFiscal(){
//    if(this.condicionFiscal!=null){
//      this.condicionesFrenteAlIva.forEach(x=>{
//        if(this.condicionFiscal==x.nombre){
//          this.cliente.condicionFrenteAlIva=x;
//        }
//      })
//    }
//  }

}
