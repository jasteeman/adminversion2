import { Component, OnInit, TemplateRef } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { FichasService } from 'src/app/services/fichas.service';
import { first } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ContableService } from 'src/app/services/contable.service';
import { UploadService } from 'src/app/services/upload.service';

@Component({
  selector: 'app-clientes-historia-clinica',
  templateUrl: './clientes-historia-clinica.component.html',
  styleUrls: ['./clientes-historia-clinica.component.scss']
})
export class ClientesHistoriaClinicaComponent implements OnInit {
  temp = { fotos: [], items: [], usuarioCreador: {}, paciente: null, indicaciones: null, fecha: null,fechaTurno:null, id: null };
  imagenesClinica = [];
  detallesPaciente = []
  url = `${environment.apiUrl}`;
  items = [];
  loading: Boolean;
  clienteId: any;
  cliente: any;
  tempCliente = "";
  tempRespuesta = "";
  fichaHistoriaClinica = [];
  loadTablaHistoriaClinica = false;
  imageTemp = null;
  imageList = [];
  load = false;
  loadHistoria = false;

  //Historia Clinica
  historiaClinica = {
    fecha: new Date(),
    fotos: [],
    items: [],
    paciente: null,
  }

  //Paginacion
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0;

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;

  constructor(private uploadService: UploadService, private contableService: ContableService, private toastr: ToastrService, private fichaService: FichasService, private modalService: BsModalService) { }

  ngOnInit() {
  }

   //Reporte
   imprimirHistoria = (id) => {
    window.open(`${environment.apiUrl}` + '/report/resumenClinico/'+this.clienteId+'/' + id, '_blank');
  }

  //1.1 Cargo historial clinico
  loaditems() {
    this.loading = true;
    this.fichaService.getHistoriasClinicas(this.page, this.pageSize, this.clienteId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.items = data.content.content;
        this.loading = false;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        this.loading = false;
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //2- Traigo el saldo cliente
  saldoCliente() {
    this.contableService.saldoCliente(this.clienteId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.cliente = data.content;
        this.tempCliente = "" + this.cliente.cliente.nombre + " " + this.cliente.cliente.apellido + "(SALDO:$" + this.cliente.saldo + ")";
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //3.Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.items = [];
    this.loaditems()
  }

  //4.Binding Cliente
  Transportar(event: any) {
    this.clienteId = event;
    this.items = [];
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.saldoCliente()
    this.loaditems();
  }

  //5.Modales
  openModalNuevoCliente(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalNuevaHistoria(template: TemplateRef<any>) {
    if (this.clienteId == null) {
      this.toastr.error("Debe seleccionar un paciente para continuar")
    } else {
      this.modalRef = this.modalService.show(template, this.config);
    }
  }

  //6.Binding Historia medica
  TransportarHistoriaMedica(event: any) {
    if (event != null) {
      this.historiaClinica = event;
      this.historiaClinica.paciente = this.cliente.cliente;
      this.fichaService.saveOrUpdateHistoriaClinicaManual(this.historiaClinica).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Historia clinica creada con éxito');
          this.load = false;
          this.items = [];
          this.page = 0;
          this.pageSize = 20;
          this.totalElements = 0;
          this.totalPages = 0;
          this.modalRef.hide()
          this.saldoCliente()
          this.loaditems();
        } else {
          this.load = false;
          this.toastr.error('' + data.message)
        }
      })
    }
  }

  //7.Binding nuevo cliente y carga
  TransportarClienteNuevo(event: any) {
    if (event != null) {
      this.clienteId = event;
      this.items = [];
      this.page = 0;
      this.pageSize = 20;
      this.totalElements = 0;
      this.totalPages = 0;
      this.saldoCliente()
      this.loaditems();
    }
  }

  //8.1 Retornar Observacion por sesion
  observaciones(data) {
    var total;
    if (data) {
      data.forEach(x => {
        if (x.pregunta == "Observaciones") {
          total = x.respuesta;
        }
      });
    }
    return total;
  };

   //8.2 Retornar medidas por sesion
  values(data) {
    var total;
    if (data) {
      data.forEach(x => {
        if (x.pregunta == "Pierna Derecha Inferior") {
          total = x.respuesta;
        }
        if (x.pregunta == "Pierna Izquierda Inferior") {
          total = x.respuesta;
        }
        if (x.pregunta == "Pierna Izquierda Superior") {
          total = x.respuesta;
        }
        if (x.pregunta == "Pierna Derecha Superior") {
          total = x.respuesta;
        }
        if (x.pregunta == "Cintura") {
          total = x.respuesta;
        }
        if (x.pregunta == "Abdomen") {
          total = x.respuesta;
        }
      });

    }
    return total;
  };

  //9.Modal de historia clinica detalles
  openModalClinica(data) {
    this.imageList = []
    this.temp = { fotos: [], items: [], usuarioCreador: {}, paciente: null, indicaciones: null, fecha: null,fechaTurno:null, id: null };
    this.fichaService.getFullHistoriaClinica(data).subscribe(data => {
      this.temp = data.content;
    })
    this.loadHistoria = true;
  }

  cancelarModal() {
    this.imageList = []
    this.loadHistoria = false;
  }

  //9.1 Modal de respuestas de historias clinicas
  openModalRespuestas(template: TemplateRef<any>, data2) {
    this.tempRespuesta = data2;
    this.modalRef = this.modalService.show(template, this.config);
  }
  cancelarRespuesta() {
    this.modalRef.hide()
  }
  // 9.2 Actualizar y cargar respuesta
  actualizarRespuesta(data) {
    var temp = [];
    this.temp.items.forEach(x => {
      if (x.pregunta == data.pregunta) {
        x.respuesta = data.respuesta
      }
      temp.push(x);

    })
    this.temp.items = [];
    this.temp.items = temp
    this.loadTablaHistoriaClinica = false;
    this.modalRef.hide()
  }

  // 9.8 IMAGENES

  // 9.8.1 Modal de  imagenes
  openModalBorrarImage(template: TemplateRef<any>, data) {
    this.imageTemp = null;
    this.imageTemp = data;
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalBorrarImageTemp(template: TemplateRef<any>, data) {
    this.imageTemp = null;
    this.imageTemp = data;
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalImg(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  // 9.8.2 Binding imagenes
  TransportarImages(event: any) {
    this.load = true;
    this.imageList = []
    this.imageList = event;
    if (this.imageList.length > 0) {
      this.load = false;
    }
  }

  // 9.8.3 Eliminar imagen antes de subir
  deleteImage() {
    this.load = true;
    for (let i = 0; i < this.temp.fotos.length; ++i) {
      if (this.temp.fotos[i] === this.imageTemp) {
        this.temp.fotos.splice(i, 1);
        this.modalRef.hide();
        this.load = false;
      }
    }
  }

  deleteImageTemp() {
    this.load = true;
    for (let i = 0; i < this.imageList.length; ++i) {
      if (this.imageList[i] === this.imageTemp) {
        this.imageList.splice(i, 1);
        this.modalRef.hide();
        this.load = false;
      }
    }
  }

  //9.8.4 Uploads Imagenes
  recursivaImages = (index, array) => new Promise((resolve, reject) => {
    if (index < array.length) {
      this.uploadService.histociaClinicas(array[index].compressedImage.imageDataUrl).subscribe(data => {
        if (data.type == "OK") {
          this.temp.fotos.push(data.content);
          resolve(this.recursivaImages(index + 1, array));
        }

      })
    } else {
      return resolve("OK");
    }
  });

  // 9.9 Terminar actualizacion de historias
  updateClinica() {
    this.load = true;
    if (navigator.onLine) {
      this.recursivaImages(0, this.imageList).then(data => {
        if (data == "OK") {
          this.fichaService.saveOrUpdateHistoriaClinica(this.temp).subscribe(data => {
            if (data.type == "OK") {
              this.toastr.success("Guardado con éxito")
              this.load = false;
              this.loadHistoria = false;
              this.items = [];
              this.page = 0;
              this.pageSize = 20;
              this.totalElements = 0;
              this.totalPages = 0;
              this.saldoCliente()
              this.loaditems();
            } else {
              this.toastr.error("" + data.type + ":" + data.message);
              this.load=false;
            }
          })
        }
      })

    } else {
      this.load = false;
      this.toastr.error('Sin Conexion a internet. Intente nuevamente')
    }

  }

}
