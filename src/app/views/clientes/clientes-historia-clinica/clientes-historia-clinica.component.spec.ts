import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesHistoriaClinicaComponent } from './clientes-historia-clinica.component';

describe('ClientesHistoriaClinicaComponent', () => {
  let component: ClientesHistoriaClinicaComponent;
  let fixture: ComponentFixture<ClientesHistoriaClinicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesHistoriaClinicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesHistoriaClinicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
