import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteRecibosComponent } from './cliente-recibos.component';

describe('ClienteRecibosComponent', () => {
  let component: ClienteRecibosComponent;
  let fixture: ComponentFixture<ClienteRecibosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteRecibosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteRecibosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
