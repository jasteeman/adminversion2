import { Component, OnInit, TemplateRef } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { ContableService } from 'src/app/services/contable.service';
import { first } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-cliente-recibos',
  templateUrl: './cliente-recibos.component.html',
  styleUrls: ['./cliente-recibos.component.scss']
})
export class ClienteRecibosComponent implements OnInit {
  movimientos = [];
  loading: Boolean;
  clienteId: any;
  load = false;

  //Paginacion
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0;
  temp = "";
  saldoValor = null;
  constructor(private toastr: ToastrService, private contableService: ContableService, private modalService: BsModalService) { }

  ngOnInit() {
  }

  reportList = () => {
    if (this.temp == '') {
      this.toastr.error("Debe seleccionar un cliente para continuar");
      return;
    }
    window.open(`${environment.apiUrl}` + '/report/resumenCtaCte/' + this.clienteId, '_blank');
  }

  report = (id) => {
    window.open(`${environment.apiUrl}` + '/report/reciboDePago/' + id, '_blank');
  }

  loadmovimientos() {
    this.loading = true;
    this.contableService.ListaReciboCliente(this.page, this.pageSize, this.clienteId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.movimientos = data.content.content;
        this.saldoCliente()
        this.loading = false;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        this.loading = false;
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  //Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.movimientos = [];
    this.loadmovimientos()
  }

  Transportar(event: any) {
    this.clienteId = event;
    this.movimientos = [];
    this.temp = ""
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.saldoCliente()
    this.loadmovimientos();
  }

  saldoCliente() {
    this.contableService.saldoCliente(this.clienteId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.saldoValor = data.content;
        this.temp = "" + this.saldoValor.cliente.nombre + " " + this.saldoValor.cliente.apellido + "(SALDO:$" + this.saldoValor.saldo + ")";
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

}
