import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesCreateModalComponent } from './clientes-create-modal.component';

describe('ClientesCreateModalComponent', () => {
  let component: ClientesCreateModalComponent;
  let fixture: ComponentFixture<ClientesCreateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesCreateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesCreateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
