import { Component, OnInit,EventEmitter ,Output, Input } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClienteCreate } from 'src/app/models/cliente';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import data from 'src/app/models/localidades';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { AfipService } from 'src/app/services/afip.service';


@Component({
  selector: 'app-clientes-create-modal',
  templateUrl: './clientes-create-modal.component.html',
  styleUrls: ['./clientes-create-modal.component.scss']
})
export class ClientesCreateModalComponent implements OnInit {
  constructor(private afipService: AfipService, private route: ActivatedRoute, private clienteService: ClienteService, private toastr: ToastrService, private router: Router, private formBuilder: FormBuilder) { }
  @Output() myClick = new EventEmitter();
  cliente = new ClienteCreate();
  localidades = data.data;
  ciudades: any;
  clienteForm: FormGroup;
  submitted = false;
  load = false;

  //AFIP
  tipoDeDocumentos = [];
  condicionesFrenteAlIva = [];
  tipoDeFacturas = [];
  tipoFactura = null;
  tipoDocumento = null;
  tipoIVA = null;
  condicionFiscal = null;

  ngOnInit() {
    //Validate Form
    this.clienteForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      email: [''],
      // direccion: ['', [Validators.required, Validators.minLength(6)]],
      localidad: ['', Validators.required],
      celular1: ['', Validators.required],
      dni: ['', Validators.required],
      estadoCivil: ['', Validators.required],
      provincia: ['', Validators.required],
      fechaNacimiento: ['', Validators.required],
      direccion: ['', Validators.required]
    }
    );
    // this.traerCondicionFrenteAlIva();
    // this.traerTipoDeDocumentos();
    // this.traerTipoDeFacturas();
    // this.traerTipoDeIvas();
  }

  //1.1-- convenience getter for easy access to form fields
  get f() { return this.clienteForm.controls; }

  //1.2--Traigo las localidades de un provincia
  onSelect(event: TypeaheadMatch): void {
    this.ciudades = event.item.ciudades;
  }
  onSelectFactura(event: TypeaheadMatch): void {
    this.tipoFactura = event.value;
  }

  //1.3-- Guardo los datos del cliente
  createCliente() {
    // this.buscarTipoFactura()
    // this.buscarTipoDocumento()
    // this.buscarCondicionFiscal()
    this.submitted = true;
    this.load = true

    // stop here if form is invalid
    if (this.clienteForm.invalid) {
      this.load = false;
      return;
    }

    //Get atributos del form validado
    this.cliente.nombre = this.clienteForm.value.nombre;
    this.cliente.apellido = this.clienteForm.value.apellido;
    this.cliente.direccion = this.clienteForm.value.direccion;
    this.cliente.localidad = this.clienteForm.value.localidad;
    this.cliente.provincia = this.clienteForm.value.provincia;
    this.cliente.fechaNacimiento = this.clienteForm.value.fechaNacimiento;
    this.cliente.dni = this.clienteForm.value.dni;
    this.cliente.email = this.clienteForm.value.email;
    this.cliente.celular1 = this.clienteForm.value.celular1;
    this.cliente.estadoCivil = this.clienteForm.value.estadoCivil;
    // if (this.cliente.tipoDocumento == null) {
    //   this.toastr.info("Debe seleccionar un tipo de Documento para continuar");
    //   this.load=false;
    //   return;
    // }
    //Guardo
    if (navigator.onLine) {
      this.clienteService.saveOrUpdate(this.cliente).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Cliente Guardado con éxito');
          this.load = false;
          this.myClick.emit(data.content.id);
        } else {
          console.log(data)
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
  // ******************************************************************************************************
  // AFIP
  // traerTipoDeDocumentos() {
  //   this.afipService.getTipoDeDocumentos().subscribe(data => {
  //     this.tipoDeDocumentos = data.content.content;
  //   })
  // }
  // traerTipoDeIvas() {
  //   this.afipService.getTiposDeIvas().subscribe(data => {
  //     this.traerTipoDeIvas = data.content.content;
  //   })
  // }
  // traerTipoDeFacturas() {
  //   this.afipService.getTiposDeFacturas().subscribe(data => {
  //     this.tipoDeFacturas = data.content.content;
  //   })
  // }
  // traerCondicionFrenteAlIva() {
  //   this.afipService.getCondicionDeIva().subscribe(data => {
  //     this.condicionesFrenteAlIva = data.content.content;
  //   })
  // }

  // //Retornar objetos para afip
  // buscarTipoFactura() {
  //   if (this.tipoFactura != null) {
  //     this.tipoDeFacturas.forEach(x => {
  //       if (this.tipoFactura == x.descrWs) {
  //         this.cliente.tipoFactura = x;
  //       }
  //     })
  //   }
  // }
  // buscarTipoDocumento() {
  //   if (this.tipoDocumento != null) {
  //     this.tipoDeDocumentos.forEach(x => {
  //       if (this.tipoDocumento == x.descrWs) {
  //         this.cliente.tipoDocumento = x;
  //       }
  //     })
  //   }
  // }
  // buscarCondicionFiscal() {
  //   if (this.condicionFiscal != null) {
  //     this.condicionesFrenteAlIva.forEach(x => {
  //       if (this.condicionFiscal == x.nombre) {
  //         this.cliente.condicionFrenteAlIva = x;
  //       }
  //     })
  //   }
  // }
}
