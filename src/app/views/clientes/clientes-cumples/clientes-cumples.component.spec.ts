import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesCumplesComponent } from './clientes-cumples.component';

describe('ClientesCumplesComponent', () => {
  let component: ClientesCumplesComponent;
  let fixture: ComponentFixture<ClientesCumplesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesCumplesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesCumplesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
