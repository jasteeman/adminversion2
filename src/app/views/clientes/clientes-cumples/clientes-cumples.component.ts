import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ClienteService } from 'src/app/services/cliente.service';
import { first } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-clientes-cumples',
  templateUrl: './clientes-cumples.component.html',
  styleUrls: ['./clientes-cumples.component.scss']
})
export class ClientesCumplesComponent implements OnInit {
  movimientos = [];
  loading: Boolean;
  items = [];
  page=0;
  pageSize=20;

  constructor(private toastr: ToastrService, private clienteService: ClienteService, private modalService: BsModalService) { }

  ngOnInit() {
    this.loadmovimientos()
  }

  loadmovimientos() {
    this.loading = true;
    this.clienteService.getCumples().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.movimientos = data.content;
        this.items = this.movimientos.slice(this.page, 10);
        this.loading = false;
      } else {
        this.loading = false;
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //Paginacion
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.items = this.movimientos.slice(startItem, endItem);
  }

  precioTotal(data) {
    var resultado = 0;
    //Estaticos
    var fecha = new Date();
    var fechaHoy = moment(fecha, 'YYYY/MM/DD');
    var month = fechaHoy.format('M');
    var day = fechaHoy.format('D');

    //Dinamico
    var fecha1 = new Date(data)
    var check = moment(fecha1, 'YYYY/MM/DD');
    var monthTraido = check.format('M');
    var dayTraido = check.format('D');

    //Operaciones
    var suma = Number(day) - Number(dayTraido)
    if (suma == 0) {
      resultado = 0
    } else if (suma == -2 || suma == 2) {
      resultado = 2
    } else if (suma == -1 || suma == 1) {
      resultado = 1
    } else if (suma == -3 || suma == 3) {
      resultado = 3
    } else if (suma == -4 || suma == 4) {
      resultado = 4
    }
    else if (suma == -5 || suma == 5) {
      resultado = 5
    } else {
      resultado = 6
    }
    return resultado;
  };
}
