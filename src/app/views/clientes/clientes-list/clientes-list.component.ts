import { Component, OnInit, TemplateRef,Input, Output } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { first } from 'rxjs/operators';
import { Cliente } from 'src/app/models/cliente';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router} from '@angular/router';

@Component({
  selector: 'app-clientes-list',
  templateUrl: './clientes-list.component.html',
  styleUrls: ['./clientes-list.component.scss']
})
export class ClientesListComponent implements OnInit {
  // Variables
  loading: Boolean;
  abrirForm: Boolean;
  clienteId: any;
  clientes: Array<Cliente> = [];
  searchText: Boolean

  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0;

  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;

  constructor(private route: Router,private clienteService: ClienteService, private toastr: ToastrService, private modalService: BsModalService) {
   }

  ngOnInit() {
    this.loadClientes();
    this.abrirForm = false
    
  }

  //1.1---Traer lista
  loadClientes() {
    this.loading = true;
    this.clienteService.getClientes(this.page, this.pageSize).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.loading = false;
        this.clientes = data.content.content;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  // 1.2 Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.clientes = [];
    this.loadClientes()
  }

  //1.3 Refresh lista
  refresh() {
    this.clientes = [];
    //Paginacion
    this.items = [];
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;

    this.loadClientes();
  }



  //Caja de texto busqueda
  mostrar() {
    this.searchText = true
  }
  ocultar() {
    this.searchText = false;
  }

  // ********************************************************************************************************
  //1.4 BORRAR CLIENTE
  confirm(data) {
    this.clienteId = data
    this.abrirForm = true
  }
  volver() {
    this.modalRef.hide()
    this.abrirForm = false;
  }

  //Borrar id
  delete() {
    this.clienteService.delete(this.clienteId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.clientes = [];
        //Paginacion
        this.items = [];
        this.page = 0;
        this.pageSize = 20;
        this.totalElements = 0;
        this.totalPages = 0;

        this.loadClientes();
        this.abrirForm = false;
        this.toastr.success('Cliente eliminado con éxito');
        this.modalRef.hide()
      } else {
        // mostrar error
      }
    });
  }

  openModal(template: TemplateRef<any>, data) {
    this.clienteId = data
    this.modalRef = this.modalService.show(template,this.config);
  }

  Transportar(event: Event) {
    this.route.navigate(['/panel/clientes/details/'+event]);
 }
}
