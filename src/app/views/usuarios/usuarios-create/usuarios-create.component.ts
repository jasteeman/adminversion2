import { Component, OnInit } from '@angular/core';
import {UsuarioService} from 'src/app/services/usuario.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute,Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { AfipService } from 'src/app/services/afip.service';
import { EmpresaService } from 'src/app/services/empresa.service';

@Component({
  selector: 'app-usuarios-create',
  templateUrl: './usuarios-create.component.html',
  styleUrls: ['./usuarios-create.component.scss']
})
export class UsuariosCreateComponent implements OnInit {

  constructor(private afipService:AfipService,private empresaService:EmpresaService,private route: ActivatedRoute, private usuarioService: UsuarioService,private toastr: ToastrService,private router:Router,private formBuilder: FormBuilder) { }

  usuarioForm: FormGroup;
  submitted = false;
  mostrarPassword:Boolean
  load=false;
  listRoles:[];

  puntosDeVentas=[];
  empresas=[];
  puntoDeVenta=null;
  nombreEmpresa=null;

  ngOnInit() {
      //Validate Form
      this.usuarioForm = this.formBuilder.group({
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        username: ['', Validators.required],
        password: ['', Validators.required],
        empresa:[null],
        puntoDeVenta:[null]
    }
    );
    //Precarga de roles
    this.loadRoles();
    this.mostrarPassword=false;
    this.loadPuntosDeVentas();
    this.loadEmpresas();
  }
    //1.1--Traigo los roles 
    loadRoles() {
      this.usuarioService.roles().pipe(first()).subscribe(data => {
         if (data.type == "OK") {
          this.listRoles = data.content;
        } else {
          // mostrar error
        }
      }, error => {
        // mostrar error
        if(error.status==401){
          this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
        }
      });
    }

  // convenience getter for easy access to form fields
  get f() { return this.usuarioForm.controls; }

  //Typehead
  onSelect(event: TypeaheadMatch): void {
    this.usuarioForm.value.rol = event.item.ciudades;
  }

     // Show password
     mostrar(){
      this.mostrarPassword=true;
    }
    ocultar(){
      this.mostrarPassword=false;
    }

  createUsuario(){
     this.submitted = true;
    this.load=true

    // stop here if form is invalid
    if (this.usuarioForm.invalid) {
      this.load=false;
        return;
    }
    this.buscarEmpresa();
    this.buscarPuntoDeVenta();
    if(this.usuarioForm.value.puntoDeVenta==null){
      this.toastr.error("Debe seleccionar un punto de venta.")
      this.load = false;
      return;
    }
    if(this.usuarioForm.value.empresa==null){
      this.toastr.error("Debe seleccionar una empresa.")
      this.load = false;
      return;
    }
    //Guardo
    this.usuarioService.saveOrUpdate(this.usuarioForm.value).subscribe(data=>{
      if (data.type == "OK") {
         this.toastr.success('Usuario creado con éxito');
         this.load=false;
         this.router.navigate(['/panel/usuarios/details/'+data.content.id]);
       } else {
         console.log(data)
         this.load=false;
        this.toastr.info("ERROR N°"+data.errorNumber+":"+data.message);
        // mostrar error
      }
    })
  }
  // *****************************************************************************************************
  // EMPRESA
  loadPuntosDeVentas() {
    this.afipService.getPuntoDeVentas().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.puntosDeVentas = data.content.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  loadEmpresas() {
    this.empresaService.getEmpresas().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.empresas = data.content.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  buscarEmpresa() {
    if (this.nombreEmpresa != null) {
      this.empresas.forEach(x => {
        if (this.nombreEmpresa == x.nombre) {
          this.usuarioForm.value.empresa = x;
        }
      })
    }
  }
  buscarPuntoDeVenta(){
    if (this.puntoDeVenta != null) {
      this.puntosDeVentas.forEach(x => {
        if (this.puntoDeVenta == x.nombre) {
          this.usuarioForm.value.puntoDeVenta = x;
        }
      })
    }
  }

}
