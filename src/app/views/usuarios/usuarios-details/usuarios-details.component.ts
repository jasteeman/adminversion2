import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AfipService } from 'src/app/services/afip.service';
import { EmpresaService } from 'src/app/services/empresa.service';


@Component({
  selector: 'app-usuarios-details',
  templateUrl: './usuarios-details.component.html',
  styleUrls: ['./usuarios-details.component.scss']
})

export class UsuariosDetailsComponent implements OnInit {
  usuarioForm: FormGroup;
  submitted = false;
  load = false;
  listRoles: [];
  id: number;
  sub: any;
  editMode: Boolean;
  usuario: any;
  mostrarPassword: Boolean;
  mostrarPassword2: Boolean;

  puntosDeVentas=[];
  empresas=[];
  puntoDeVenta=null;
  nombreEmpresa=null;

  constructor(private afipService:AfipService,private empresaService:EmpresaService,private route: ActivatedRoute, private usuarioService: UsuarioService, private toastr: ToastrService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    //Precarga de roles
    this.loadRoles();
    this.loadPuntosDeVentas();
    this.loadEmpresas();
    this.editMode = false;
    //Validate Form
    this.usuarioForm = this.formBuilder.group({
      password: [{value: '', disabled: this.editMode==false}, Validators.required],
      password2: [{value: '', disabled: this.editMode==false}, Validators.required]
    })
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.loadUsuarioDetail();
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  // convenience getter for easy access to form fields
  get f() { return this.usuarioForm.controls; }

  //1.1--Traigo el usuario 
  loadUsuarioDetail() {
    this.usuarioService.getFull(this.id).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.usuario = data.content;
        if(data.content.empresa!=null){
          this.nombreEmpresa=data.content.empresa.nombre;
        }
        if(data.content.puntoDeVenta!=null){
          this.puntoDeVenta=data.content.puntoDeVenta.nombre
        }
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.2--Traigo los roles 
  loadRoles() {
    this.usuarioService.roles().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.listRoles = data.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.3--Habilitar y desabilitar form
  unlockEditMode() {
    this.editMode = true;
    this.usuarioForm.get('password').enable();
    this.usuarioForm.get('password2').enable();
  }
  lockEditMode() {
    this.editMode = false;
    this.usuarioForm.get('password').disable();
    this.usuarioForm.get('password2').disable();
  }

  //1.4-- Guardo los datos actualizados del cliente
  updateUsuario() {
    this.load = true;
    this.buscarEmpresa();
    this.buscarPuntoDeVenta();
    if(this.usuario.puntoDeVenta==null){
      this.toastr.error("Debe seleccionar un punto de venta.")
      this.load = false;
      return;
    }
    if(this.usuario.empresa==null){
      this.toastr.error("Debe seleccionar una empresa.")
      this.load = false;
      return;
    }
    if (navigator.onLine) {
      this.usuarioService.saveOrUpdate(this.usuario).subscribe(data => {
        if (data.type == "OK") {
          this.editMode = false;
          this.load = false;
          this.toastr.success('Usuario editado con éxito');
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
  //1.5 Show password
  mostrar() {
    this.mostrarPassword = true;
  }
  ocultar() {
    this.mostrarPassword = false;
  }
  mostrar2() {
    this.mostrarPassword2 = true;
  }
  ocultar2() {
    this.mostrarPassword2 = false;
  }

  // 1.6 Guardar nuevo rol
  updateRol() {
    this.load = true;
    if (navigator.onLine) {
      this.usuarioService.updateRoles(this.usuario.id, this.usuario.rol).subscribe(data => {
        if (data.type == "OK") {
          this.editMode = false;
          this.load = false;
          this.toastr.success('Rol actualizado');
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  //Typehead
  onSelect(event: TypeaheadMatch): void {
    this.usuarioForm.value.rol = event.item.ciudades;
  }

  updatePassword() {
    this.submitted = true;
    this.load = true

    // stop here if form is invalid
    if (this.usuarioForm.invalid) {
      this.load = false;
      return;
    }
    let temp = {
      userId: this.usuario.id,
      oldPassword: this.f.password.value,
      newPassword: this.f.password2.value
    }
    if (navigator.onLine) {
      this.usuarioService.updatePassword(temp).subscribe(data => {
        if (data.type == "OK") {
          this.editMode = false;
          this.load = false;
          this.toastr.success('Password cambiado con éxito');
          this.usuarioForm.reset()
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
  // *****************************************************************************************************
  // EMPRESA
  loadPuntosDeVentas() {
    this.afipService.getPuntoDeVentas().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.puntosDeVentas = data.content.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  loadEmpresas() {
    this.empresaService.getEmpresas().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.empresas = data.content.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  buscarEmpresa() {
    if (this.nombreEmpresa != null) {
      this.empresas.forEach(x => {
        if (this.nombreEmpresa == x.nombre) {
          this.usuario.empresa = x;
        }
      })
    }
  }
  buscarPuntoDeVenta(){
    if (this.puntoDeVenta != null) {
      this.puntosDeVentas.forEach(x => {
        if (this.puntoDeVenta == x.nombre) {
          this.usuario.puntoDeVenta = x;
        }
      })
    }
  }
}
