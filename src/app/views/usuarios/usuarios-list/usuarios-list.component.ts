import { Component, OnInit,TemplateRef } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import {AuthService} from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Usuario } from 'src/app/models/usuario';
import {first } from "rxjs/operators";
import { Router} from '@angular/router';

@Component({
  selector: 'app-usuarios-list',
  templateUrl: './usuarios-list.component.html',
  styleUrls: ['./usuarios-list.component.scss']
})
export class UsuariosListComponent implements OnInit {
  usuarios: Array<Usuario> = []
  loading: Boolean;
  usuarioId: any;
  abrirForm: Boolean;
  searchText:Boolean;
  user:any

  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0

    //  Modal
    config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;

  constructor(private route: Router,private authService: AuthService,private usuarioService: UsuarioService, private toastr: ToastrService,private modalService: BsModalService) {
   }

  ngOnInit() {
    this.searchText=false;
    this.userLogueado();
    this.loadUsers();
  }

  userLogueado(){
    this.authService.getLoggedUser().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.user=data.content
      } 
    })
  }
  
  loadUsers() {
    this.loading = true;
    this.usuarioService.getUsuarios(this.page, this.pageSize).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.usuarios = data.content.content;
        this.loading = false;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  //Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.usuarios = [];
    this.loadUsers()
  }

    //1.3 Refresh lista
    refresh() {
      this.usuarios = [];
      //Paginacion
      this.items = [];
      this.page = 0;
      this.pageSize = 20;
      this.totalElements = 0;
      this.totalPages = 0;
  
      this.loadUsers();
    }

  //Caja de texto busqueda
  mostrar(){
    this.searchText=true
  }
  ocultar(){
    this.searchText=false;
  }

  // ********************************************************************************************************
  //1.4 BORRAR CLIENTE
  confirm(data) {
    this.usuarioId = data
    this.abrirForm = true
  }
  volver() {
    this.modalRef.hide()
    this.abrirForm = false;
  }

  //Borrar id
  delete() {
    this.usuarioService.delete(this.usuarioId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.usuarios = [];
        //Paginacion
        this.items = [];
        this.page = 0;
        this.pageSize = 20;
        this.totalElements = 0;
        this.totalPages = 0;

        this.loadUsers();
        this.abrirForm = false;
        this.toastr.success('Usuario eliminado con éxito');
        this.modalRef.hide()
      } else {
        // mostrar error
      }
    });
  }

  openModal(template: TemplateRef<any>, data) {
    this.usuarioId = data
    this.modalRef = this.modalService.show(template,this.config);
  }

  Transportar(event: Event) {
    this.route.navigate(['/panel/usuarios/details/'+event]);
 }
}
