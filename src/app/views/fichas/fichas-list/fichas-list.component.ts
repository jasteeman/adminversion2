import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { FichasService } from 'src/app/services/fichas.service';
import { Router } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-fichas-list',
  templateUrl: './fichas-list.component.html',
  styleUrls: ['./fichas-list.component.scss']
})
export class FichasListComponent implements OnInit {
  //  Variables
  loading: Boolean;
  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
  //Paginacion
  tipo="historiaClinica"
  movimientos=[]
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0
  fichaId=null
  constructor(private router: Router, private fichaService: FichasService, private toastr: ToastrService, private modalService: BsModalService) {

  }

  ngOnInit() {
    this.loadmovimientos()
  }

  loadmovimientos() {
    this.loading = true;
    this.fichaService.getTipoDePlantilla(this.tipo).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.loading = false;
        this.movimientos = data.content
        this.items = this.movimientos.slice(this.page, 10);
      } else {
        this.loading = false;
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  buscador() {
    this.movimientos = []
    this.items = [];
    this.loadmovimientos()
  }

   //Paginacion
   pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.items = this.movimientos.slice(startItem, endItem);
  }

    //Borrado del pedido
    openModal(template: TemplateRef<any>, data) {
      this.fichaId = data
      this.modalRef = this.modalService.show(template,this.config);
    }
    delete() {
      this.modalRef.hide()
      if (navigator.onLine) {
        this.fichaService.deletePlantilla(this.fichaId).subscribe(data => {
          if (data.type == "OK") {
            this.toastr.success('Ficha borrada con éxito ');
            this.loadmovimientos()
          } else {
            this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
            // mostrar error
          }
        })
      }
      else {
        this.toastr.info("Sin Conexion a internet. Intente nuevamente");
      }
    }

}
