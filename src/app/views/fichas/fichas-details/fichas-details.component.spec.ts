import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichasDetailsComponent } from './fichas-details.component';

describe('FichasDetailsComponent', () => {
  let component: FichasDetailsComponent;
  let fixture: ComponentFixture<FichasDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichasDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichasDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
