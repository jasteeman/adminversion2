import { Component, OnInit,TemplateRef } from '@angular/core';
import {  FichasService } from 'src/app/services/fichas.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute,Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-fichas-create',
  templateUrl: './fichas-create.component.html',
  styleUrls: ['./fichas-create.component.scss']
})
export class FichasCreateComponent implements OnInit {
  ficha={tipo:null,nombre:null,items:[]}
  pregunta=null
  tempPregunta=null
  load=false;
  tempProducto;
  index=null

  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
  constructor(private modalService: BsModalService,private route: ActivatedRoute, private fichaService: FichasService,private toastr: ToastrService,private router:Router,private formBuilder: FormBuilder) { }

  ngOnInit() {
  }
    //Operaciones de tablas
    cargarTabla() {
      if (this.pregunta == null) {
        this.toastr.error("Debe completar el campo pregunta para continuar")
        return
      }
      this.ficha.items.push({ pregunta: this.pregunta });
      this.pregunta=null
    }
    eliminarItemTabla() {
      for (let i = 0; i < this.ficha.items.length; ++i) {
        if (this.ficha.items[i].pregunta=== this.tempProducto) {
          this.ficha.items.splice(i, 1);
        }
      }
      this.modalRef.hide()
    }

    //Modals
  openModal(template: TemplateRef<any>, data) {
    this.tempProducto = data
    this.modalRef = this.modalService.show(template,this.config);
  }
  openModalEdit(template: TemplateRef<any>,index, data) {
    this.tempPregunta = data
    this.modalRef = this.modalService.show(template,this.config);
  }
  openModalGuardar(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template,this.config);
  }
  

//Guardo la plantilla
     createTemplate(){
      this.load=true
  
      // stop here if form is invalid
      if (this.ficha.nombre==null||this.ficha.tipo==null) {
        this.load=false;
        this.toastr.info("Campos incompletos. Complete y vuelve a intentar");
        this.modalRef.hide()
          return;
      }
      if(navigator.onLine){
      //Guardo
      this.fichaService.saveOrUpdatePlantilla(this.ficha).subscribe(data=>{
        if (data.type == "OK") {
           this.toastr.success('Plantilla creada con éxito');
           this.load=false;
           this.modalRef.hide()
           this.router.navigate(['/panel/fichas/list/']);
         } else {
           console.log(data)
           this.load=false;
           this.modalRef.hide()
          this.toastr.info("ERROR N°"+data.errorNumber+":"+data.message);
          // mostrar error
        }
      })
      }else{
        this.modalRef.hide()
        this.load=false;
        this.toastr.info("Sin Conexion a internet. Intente nuevamente");
      }
  
    }
}
