import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichasCreateComponent } from './fichas-create.component';

describe('FichasCreateComponent', () => {
  let component: FichasCreateComponent;
  let fixture: ComponentFixture<FichasCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichasCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichasCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
