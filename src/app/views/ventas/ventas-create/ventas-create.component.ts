import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { productoVenta } from 'src/app/models/productos';
import { ClienteService } from 'src/app/services/cliente.service';
import { VentasService } from 'src/app/services/ventas.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AfipService } from 'src/app/services/afip.service';

@Component({
  selector: 'app-ventas-create',
  templateUrl: './ventas-create.component.html',
  styleUrls: ['./ventas-create.component.scss']
})
export class VentasCreateComponent implements OnInit {
  editMode = false;
  descuento = 0;
  total = 0;
  venta = {
    clienteId: null,
    items: [],
    pagos: [],
    descuento: 0,
    observacion: null,
    condicionVenta: "CONTADO",
    condicionFrenteAlIva: null
  }

  producto = new productoVenta();
  cargarForm: boolean;
  load: boolean;
  activarVenta = false;
  tempProducto;
  items = [];
  tempCliente = null;
  usuario = null;
  ventaAnonima = false;

  //STOCK
  totalPagos = 0;
  totalVenta = 0;
  totalVentaFinal = 0;

  // AFIP
  puntoDeVenta = null;
  empresa = null;
  tiposDeFacturas = [];
  tipoComprobante = null;

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;

  constructor(private route: ActivatedRoute, private authService: AuthService, private afipService: AfipService, private router: Router, private ventaService: VentasService, private clienteService: ClienteService, private productosService: ProductosService, private toastr: ToastrService, private modalService: BsModalService) { }

  ngOnInit() {
    this.loadUsuarioLogueado();
    this.cargarForm = false;
    this.load = false;
    this.editMode = false;
  }

  //Refrescar carga
  refreshProducto() {
    this.cargarForm = false;
    this.producto = new productoVenta()
  }

  //Actualizar valores
  valuechange(data) {
    this.producto = data;
    let totalSinIva = (this.producto.precioCosto * this.producto.iva) / 100;
    let precioConIVa = (totalSinIva + this.producto.precioCosto);
    let precioConUtilidad = precioConIVa * this.producto.utilidad / 100;
    let resultado = precioConUtilidad + precioConIVa;
    let resultadoFinal = resultado - this.producto.descuento;
    this.producto.precioVenta = Number(resultadoFinal.toFixed(2));
  }

  //Selecciono la venta anonima de clientes
  checkValue(event: any) {
    if (event == true) {
      this.tempCliente = null;
      this.venta.clienteId = null;
      this.tipoComprobante = null;
    }
  }

  //Cargo el producto
  Transportar(event: any) {
    this.productosService.getFull(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.cargarForm = true;
        this.producto = data.content;
        this.producto.cantidad = 1;
        this.producto.descuento = 0;
        this.cargarTabla();
      } else {
        // mostrar error
      }
    })
  }

  //Cargo el cliente
  TransportarCliente(event: any) {
    this.ventaAnonima = false;
    this.tipoComprobante=null;
    this.tiposDeFacturas=[];
    this.clienteService.getFull(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        var temp = data.content.fechaNacimiento;
        data.content.fechaNacimiento = new Date(temp)
        this.venta.clienteId = data.content.id;
        this.tempCliente = data.content;
        if (data.content.tipoFactura != null) {
          this.tipoComprobante = this.tempCliente.tipoFactura.descrWs;
          this.traerCondicionFrenteAlIva(data.content.tipoFactura.nombre);
        }
      }
    })
  }

  //Cargo el pago
  TransportarPago(event) {
    if (event != undefined) {
      this.venta.pagos = event
    }
  }
// ********************************************************************************************************************
  //OPERACIONES DENTRO DE LA TABLA
  cargarTabla() {
    this.items.push({ producto: this.producto, unidadMedida: this.producto.unidadMedida, cantidad: this.producto.cantidad, precioUnitario: this.producto.precioVenta, descuento: this.producto.descuento });
    this.refreshProducto();
    this.cargarForm = false;
  }
  eliminarItem() {
    for (let i = 0; i < this.items.length; ++i) {
      if (this.items[i].id === this.tempProducto) {
        this.items.splice(i, 1);
        this.toastr.info("Producto borrado")
        this.modalRef.hide()
      }
    }
  }

  //Calcular Total
  precioTotalVenta() {
    var total = 0;
    var total2 = 0;
    var total3 = 0
    this.items.forEach(x => {
      total = total + (x.cantidad * x.precioUnitario) - x.descuento;
    });
    total2 = Math.round(total * 100) / 100;
    this.venta.descuento = Math.round(total2 * this.descuento) / 100;
    total3 = total2 - (total2 * this.descuento) / 100;
    return total3;
  }

  precioTotalIVA() {
    var total = 0;
    var total2 = 0;
    var total4 = 0
    this.items.forEach(x => {
      total4 = (x.cantidad * x.precioUnitario) - x.descuento
      total = total + Math.round(total4 * x.producto.iva) / 100;
    });
    total2 = Math.round(total * 100) / 100;
    return total2;
  }
  // FIN OPERACIONES DE TABLA
  // ********************************************************************************************************************

  //5.8.4 Recursiva de consulta de STOCK
  recursivaStock = (index, array) => new Promise((resolve, reject) => {
    if (index < array.length) {
      if (array[index].cantidad < array[index].producto.stock) {
        var temp = (array[index].precioUnitario * array[index].cantidad) - array[index].descuento;
        this.totalVenta += temp
        this.venta.items.push({ productoId: array[index].producto.id, cantidad: array[index].cantidad, precioUnitario: array[index].precioUnitario, descuento: array[index].descuento });
        resolve(this.recursivaStock(index + 1, array));
      } else {
        this.toastr.error("Stock insuficiente del producto " + array[index].producto.nombre + ":STOCK REAL " + array[index].producto.stock);
        this.load = false;
        this.modalRef.hide();
        return;
      }
    } else {
      return resolve("OK");
    }
  });

// ********************************************************************************************************************
  // Guardar venta
  guardarVenta() {
    this.load = true;
    this.buscarCondicionFrenteAlIVA();
    this.venta.items = [];

    //Valido si es anonima o no
    if (this.venta.clienteId == null && this.ventaAnonima == false) {
      this.toastr.info("Debe ingresar un cliente o seleccionar la opción de venta anónima para continuar");
      this.load = false;
      this.modalRef.hide()
      return;
    }

    //Cargo la lista y valido stock
    this.recursivaStock(0, this.items).then(data => {
      console.log(data);
      if (data == "OK") {
        this.totalVentaFinal = this.totalVenta - this.venta.descuento;
        //Sumar Pagos
        this.venta.pagos.forEach(x => {
          x.monto = Number(x.monto);
          this.totalPagos += x.monto
        })

        //Validar montos de pagos
        if (this.venta.clienteId != null) {
          this.activarVenta = true;
        } else {
          if (this.totalPagos == this.totalVentaFinal) {
            this.activarVenta = true;
          } else {
            this.toastr.info("El monto debe ser igual al total de la venta.Ya que es una venta anónima");
            this.load = false;
            this.modalRef.hide()
            return;
          }
        }

        //Guardo en la bd
        if (navigator.onLine && this.activarVenta == true) {
          this.ventaService.saveOrUpdate(this.venta).subscribe(data => {
            if (data.type == "OK") {
              this.toastr.success('Venta creada con éxito');
              this.load = false;
              this.modalRef.hide();
              this.descuento = 0;
              this.total = 0;
              this.venta = {
                clienteId: null,
                items: [],
                pagos: [],
                descuento: 0,
                observacion: null,
                condicionVenta: null,
                condicionFrenteAlIva: null
              }
              this.items = []
              this.router.navigate(['/panel/ventas/list']);
            } else {
              this.load = false;
              this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
              this.venta.items = []
              this.modalRef.hide()
              // mostrar error
            }
          })
        }
        else {
          this.load = false;
          this.modalRef.hide()
          this.toastr.info("Sin Conexion a internet. Intente nuevamente");
        }
      }
    })
  }

  openModal(template: TemplateRef<any>, data) {
    this.tempProducto = data
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalGuardar(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  // *****************************************************************************************************
  // EMPRESA
  loadUsuarioLogueado() {
    this.authService.getLoggedUser().pipe(first()).subscribe(
      data => {
        if (data.type == "OK") {
          this.empresa = data.content.empresa;
          this.puntoDeVenta = data.content.puntoDeVenta;
          this.usuario = data.content;
        }
      })
  }
  traerCondicionFrenteAlIva(nombre) {
    this.tiposDeFacturas = [];
    this.afipService.getTiposDeFacturas().subscribe(data => {
      var temp = data.content.content;
      temp.forEach(element => {
        if (element.nombre == nombre) {
          this.tiposDeFacturas.push(element);
        }
      });
    })
  }

  buscarCondicionFrenteAlIVA() {
    if (this.tipoComprobante != null) {
      this.tiposDeFacturas.forEach(x => {
        if (this.tipoComprobante == x.descrWs) {
          this.venta.condicionFrenteAlIva = x;
        }
      })
    }
  }

}
