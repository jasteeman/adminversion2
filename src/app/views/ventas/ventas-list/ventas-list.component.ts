import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { ClienteService } from 'src/app/services/cliente.service';
import { VentasService } from 'src/app/services/ventas.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import data from 'src/app/models/localidades';
import { TypeaheadMatch } from 'ngx-bootstrap';

@Component({
  selector: 'app-ventas-list',
  templateUrl: './ventas-list.component.html',
  styleUrls: ['./ventas-list.component.scss']
})
export class VentasListComponent implements OnInit {
  //  Variables
  selectedRow = [];
  setClickedRow: Function;
  filtroPagination = {
    size: 20,
    page: 0,
    clienteId: null,
    localidad: null,
    desde: null,
    hasta: null
  }
  resultados = { precioCosto: 0, venta: 0 };
  tempCliente = "";
  tempDetalles: any;
  loading: Boolean;
  localidades = data.data;
  ciudades = [];
  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
  tempArray=[];

  //Reporte
  imprimirVenta = (id) => {
    window.open(`${environment.apiUrl}` + '/report/ventas/' + id, '_blank');
  }
  report = () => {
    window.open(`${environment.apiUrl}` + '/report/ventaList/'+ this.selectedRow , '_blank');
  }

  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0



  constructor(private router: Router, private ventaService: VentasService, private clienteService: ClienteService, private toastr: ToastrService, private modalService: BsModalService) { 
    this.setClickedRow = function (index) {
      if (this.selectedRow.indexOf(index.id) > -1) {
        this.selectedRow.splice(this.selectedRow.indexOf(index.id), 1);
      }
      else {
        this.selectedRow.push(index.id);
      }
    }
  }


  ngOnInit() {
    this.loading = false;
    this.loadVentas();
    //Asignacion de localidad
    this.localidades.forEach(x => {
      if (x.nombre == "Santa Fe") {
        this.ciudades = x.ciudades;
      }
    })

  }

  //Binding Cliente
  TransportarCliente(event: any) {
    this.clienteService.getFull(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.filtroPagination.clienteId = data.content.id;
        this.tempCliente = "" + data.content.nombre + " " + data.content.apellido + "(" + data.content.id + ")";
        this.buscador()
      }
    })
  }
  
  //Lista de pedidos
  loadVentas() {
    this.loading = true;
    this.ventaService.getVentas(this.filtroPagination).pipe(first()).subscribe(data => {
      this.loading = false;
      if (data.type == "OK") {
        this.items = data.content.content;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      }
    })
  }
  cmv(){
    this.resultados.precioCosto=0;
    this.resultados.venta=0;
    this.ventaService.cmv(this.filtroPagination.desde,this.filtroPagination.hasta).pipe(first()).subscribe(data => {
      if(data.type=="OK"){
        this.tempArray=data.content
      }
      this.calcularCMV()
    })
  }

  //Calcular CMV
  calcularCMV(){
    this.tempArray.forEach(element=>{
      element.items.forEach(u => {
          var costo=(u.producto.precioCosto*u.producto.iva/100)+u.producto.precioCosto;
           this.resultados.precioCosto+=u.cantidad*costo;
      });
      this.resultados.venta+=element.totalNeto
    })
  }

  //Paginacion
  pageChanged(event: any): void {
    this.filtroPagination.page = event.page - 1;
    this.items = [];
    this.loadVentas()
  }

  //Refresh lista
  refresh() {
    this.items = [];
    this.resetFiltro();
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.tempCliente = ""
    this.loadVentas();
  }

  //Busqueda Personalizada
  buscador() {
    this.filtroPagination.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.tempArray=[]
    this.loadVentas();

  }

  //Limpiar campos de filtro
  resetFiltro() {
    this.filtroPagination = {
      size: 20,
      page: 0,
      clienteId: null,
      localidad: null,
      desde: null,
      hasta: null
    }
    this.tempArray=[]
  }

  //Capturo select ciudades
  onSelect(event: TypeaheadMatch): void {
    this.filtroPagination.localidad = event.value
    this.tempArray=[]
    this.buscador()
  }

  //Capturo estado de pedidos
  onPercentChange() {
    this.tempArray=[]
    this.buscador()
  }

  //Borrado del pedido
  openModal(template: TemplateRef<any>, data) {
    this.tempDetalles = data
    this.modalRef = this.modalService.show(template,this.config);
  }

}
