import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";

@Component({
  selector: 'app-gestion-stock',
  templateUrl: './gestion-stock.component.html',
  styleUrls: ['./gestion-stock.component.scss']
})
export class GestionStockComponent implements OnInit {
  // Variables
  producto = {
    nombre: null,
    marca: '',
    codigo: null,
    codigoDeBarras: null,
    precioCosto: 0,
    precioVenta: 0,
    precioVentaProfesional: 0,
    precioCostoFinal: 0,
    iva: 0,
    logistica: 0,
    subMarca: "",
    proveedor: null,
    cantidad: 0,
    utilidad: 0
  };
  tabla = [];
  cargarForm: boolean;
  load:boolean;
  tempProducto;

    //  Modal
    config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;


  constructor(private productosService: ProductosService, private toastr: ToastrService, private modalService: BsModalService) { }

  ngOnInit() {
    this.valuechange()
    this.cargarForm = false;
    this.load=false;
  }
  refreshProducto() {
    this.cargarForm=false;
    this.producto = {
      nombre: null,
      marca: '',
      codigo: null,
      codigoDeBarras: null,
      precioCosto: 0,
      precioVenta: 0,
      precioVentaProfesional: 0,
      precioCostoFinal: 0,
      iva: 0,
      logistica: 0,
      subMarca: "",
      proveedor: null,
      cantidad: 0,
      utilidad: 0
    };
  }
  valuechange() {
    let totalSinIva = (this.producto.precioCosto * this.producto.iva) / 100;
    let precioConIVa = (totalSinIva + this.producto.precioCosto);
    let precioConUtilidad = precioConIVa * this.producto.utilidad / 100;
    let resultado = precioConUtilidad + precioConIVa;
    this.producto.precioVenta = Number(resultado.toFixed(2));
  }

  Transportar(event: any) {
    this.productosService.getFull(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.cargarForm = true
        this.producto = data.content
        this.producto.cantidad=null
      } else {
        // mostrar error
      }
    })
  }

  //Operaciones de tablas
  cargarTabla() {
    if(this.producto.cantidad==null){
      this.toastr.error("Ingrese una cantidad al producto")
      return
    }
    if(this.producto.nombre==null){
      this.toastr.error("Ingrese una nombre al producto")
      return
    }
    this.tabla.push(this.producto);
    this.refreshProducto();
    this.cargarForm=false;
  }
  eliminarItem() {
    for (let i = 0; i < this.tabla.length; ++i) {
      if (this.tabla[i].id === this.tempProducto) {
        this.tabla.splice(i, 1);
        this.toastr.info("Producto borrado")
        this.modalRef.hide()
      }
    }
  }
  eliminarItemTabla() {
    for (let i = 0; i < this.tabla.length; ++i) {
      if (this.tabla[i].id === this.tempProducto) {
        this.tabla.splice(i, 1);
      }
    }
  }

  // Guardar valores 
  guardar(data){
    this.load=true;
    let stock={ productoId: data.id,cantidad: Number(data.cantidad)}
    if (navigator.onLine) {
      this.productosService.saveOrUpdate(data).subscribe(data => {
        if (data.type == "OK") {
          // this.toastr.success('', '' + data.message);
          // Guardo el stock
          this.productosService.moveStock(stock).subscribe(data => {
            if (data.type == "OK") {
              this.load = false;
              this.toastr.success('Stock agregado con éxito');
              this.tempProducto=stock.productoId;
              this.eliminarItemTabla()
            }
        })
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  openModal(template: TemplateRef<any>, data) {
    this.tempProducto = data
    this.modalRef = this.modalService.show(template,this.config);
  }
}
