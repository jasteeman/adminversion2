import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { ToastrService } from 'ngx-toastr';
import { Productos } from 'src/app/models/productos';
import { first } from "rxjs/operators";

@Component({
  selector: 'app-productos-precio',
  templateUrl: './productos-precio.component.html',
  styleUrls: ['./productos-precio.component.scss']
})
export class ProductosPrecioComponent implements OnInit {
  productos: Array<Productos> = []
  loading: Boolean;
  productoId: any;
  abrirForm: Boolean;
  searchText: Boolean;
  user: any;
  filtroPagination = {
    sortField: null,
    sortDesc: true,
    razonSocial: null,
    size: 20,
    page: 0,
    nombre: null,
    marca: null,
    codigo: null,
    codigoDeBarras: null,
    subMarca: null,
    proveedorCuit: null,
    proveedorRazonSocial: null
  }

    //Paginacion
    items = [];
    page = 0;
    pageSize = 20;
    totalElements = 0;
    totalPages = 0
  constructor(private productoService: ProductosService, private toastr: ToastrService) { }

  ngOnInit() {
    this.loadProductos();
  }
  loadProductos() {
    this.loading = true;
    this.productoService.getProductos(this.filtroPagination).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.productos = data.content.content;
        this.loading = false;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  //Paginacion
  pageChanged(event: any): void {
    this.filtroPagination.page = event.page - 1;
    this.productos = [];
    this.loadProductos()
  }

  //1.3 Refresh lista
  refresh() {
    this.productos = [];
    //Paginacion
    this.items = [];
    this.resetFiltro();
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadProductos();
  }

  //Busqueda Personalizada
  buscador(){
    this.filtroPagination.page=0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadProductos();

  }

  //Limpiar campos de filtro
  resetFiltro(){
    this.filtroPagination = {
      sortField: null,
      sortDesc: true,
      razonSocial: null,
      size: 20,
      page: 0,
      nombre: null,
      marca: null,
      codigo: null,
      codigoDeBarras: null,
      subMarca: null,
      proveedorCuit: null,
      proveedorRazonSocial: null
    }
  }

  valuechange(data) {
    let totalSinIva = (data.precioCosto * data.iva) / 100;
    let precioConIVa = (totalSinIva + data.precioCosto);
    let precioConUtilidad = precioConIVa * data.utilidad / 100;
    let resultado = precioConUtilidad + precioConIVa;
    data.precioVenta = Number(resultado.toFixed(2));
  }
  guardarProducto(data){

    // //Validate
    if (data.codigo == "") {
      data.codigo = null
    }
    if (data.codigoDeBarras == "") {
     data.codigoDeBarras = null
    }
  
    //Guardo
    if (navigator.onLine) {
      this.productoService.saveOrUpdate(data).subscribe(data2 => {
        if (data2.type == "OK") {
          this.toastr.success('Producto actualizado con éxito');
          this.loadProductos()
        } else {
          this.toastr.info("ERROR N°" + data2.errorNumber + ":" + data2.message);
          // mostrar error
        }
      })
    }
    else {
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

}
