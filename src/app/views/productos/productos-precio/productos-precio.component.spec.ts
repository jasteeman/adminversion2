import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductosPrecioComponent } from './productos-precio.component';

describe('ProductosPrecioComponent', () => {
  let component: ProductosPrecioComponent;
  let fixture: ComponentFixture<ProductosPrecioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductosPrecioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductosPrecioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
