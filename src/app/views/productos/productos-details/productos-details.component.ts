import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import { Productos } from 'src/app/models/productos';
import { environment } from 'src/environments/environment';
import { AfipService } from 'src/app/services/afip.service';

@Component({
  selector: 'app-productos-details',
  templateUrl: './productos-details.component.html',
  styleUrls: ['./productos-details.component.scss']
})
export class ProductosDetailsComponent implements OnInit {
  id: number;
  sub: any;
  editMode = false;
  load = false;
  proveedores = [];
  producto = new Productos();
  productoForm: FormGroup;
  tempProveedor: any;
  submitted = false;
  imageView = "";
  
  tipoIVA=null;
  tipoDeIvas=[]

  constructor(private router: Router,private afipService:AfipService, private route: ActivatedRoute, private proveedoresService: ProveedoresService, private productosService: ProductosService, private toastr: ToastrService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.traerTipoDeIvas();
    this.editMode = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.loadProductoDetail();
    });
    this.loadProveedores()
    this.valuechange()
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  //1.1--Traigo el producto 
  loadProductoDetail() {
    this.productosService.getFull(this.id).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.tempProveedor = data.content.proveedor;
        this.producto = data.content;
        if(this.producto.iva!=null){
          this.tipoIVA=this.producto.iva.toString()
        }
        this.imageView = `${environment.apiUrl}` + this.producto.imgUrl;
        this.producto.proveedor = this.tempProveedor.razonSocial;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.2--Habilitar y desabilitar form
  unlockEditMode() {
    this.editMode = true;
  }
  lockEditMode() {
    this.editMode = false;
  }

  //1.3-- Guardo los datos actualizados del producto
  createProducto() {

    this.submitted = true;
    this.load = true

    // //Validate
    if (this.producto.codigo == "") {
      this.producto.codigo = null
    }
    if (this.producto.codigoDeBarras == "") {
      this.producto.codigoDeBarras = null
    }
    if (this.producto.proveedor == null) {
      this.load = false;
      return;
    }
    this.producto.proveedor = null;
    this.producto.proveedor = this.tempProveedor
    this.producto.iva=parseInt(this.tipoIVA)
    //Guardo
    if (navigator.onLine) {
      this.productosService.saveOrUpdate(this.producto).subscribe(data => {
        if (data.type == "OK") {
          this.editMode = false;
          this.load = false;
          this.toastr.success('Producto editado con éxito');
          this.loadProductoDetail()
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  //1.4--Traigo los proveedores
  loadProveedores() {
    this.proveedoresService.getProveedores({ page: 0, size: 40 }).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.proveedores = data.content.content;
      }
    }, error => {
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.5-- Typehead
  onSelect(event: TypeaheadMatch): void {
    this.tempProveedor = event.item;
    this.producto.proveedor = event.item.razonSocial
  }

  //1.6-- Change Sumatoria
  valuechange() {
    if(this.tipoIVA!=null){
      this.producto.iva=parseInt(this.tipoIVA)
    }
    let totalSinIva = (this.producto.precioCosto * this.producto.iva) / 100;
    let precioConIVa = (totalSinIva + this.producto.precioCosto);
    let precioConUtilidad = precioConIVa * this.producto.utilidad / 100;
    let resultado = precioConUtilidad + precioConIVa;
    this.producto.precioVenta = Number(resultado.toFixed(2));
  }

  // 1.7--File
  Transportar(event: any) {
    var temp = event;
    this.producto.imgUrl = temp;
  }

   // ***********************************************************************************************
  // AFIP
  traerTipoDeIvas(){
    this.afipService.getTiposDeIvas().subscribe(data=>{
     this.tipoDeIvas=data.content.content;
    })
   }
}
