import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Productos } from 'src/app/models/productos';
import { first } from "rxjs/operators";
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-productos-list',
  templateUrl: './productos-list.component.html',
  styleUrls: ['./productos-list.component.scss']
})

export class ProductosListComponent implements OnInit {
  productos: Array<Productos> = []
  loading: Boolean;
  productoId: any;
  abrirForm: Boolean;
  searchText: Boolean;
  user: any;
  filtroPagination = {
    sortField: null,
    sortDesc: true,
    razonSocial: null,
    size: 20,
    page: 0,
    nombre: null,
    marca: null,
    codigo: null,
    codigoDeBarras: null,
    subMarca: null,
    proveedorCuit: null,
    proveedorRazonSocial: null
  }

  //Reporte
      report = () => {
        var precio=1
        window.open(`${environment.apiUrl}` + '/report/productos/' + precio, '_blank');
    }

  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0

  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
  constructor(private productoService: ProductosService, private toastr: ToastrService, private modalService: BsModalService) { }

  ngOnInit() {
    this.loadProductos();
  }
  loadProductos() {
    this.loading = true;
    this.productoService.getProductos(this.filtroPagination).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.productos = data.content.content;
        this.loading = false;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  //Paginacion
  pageChanged(event: any): void {
    this.filtroPagination.page = event.page - 1;
    this.productos = [];
    this.loadProductos()
  }

  //1.3 Refresh lista
  refresh() {
    this.productos = [];
    //Paginacion
    this.items = [];
    this.resetFiltro();
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadProductos();
  }

  //Busqueda Personalizada
  buscador(){
    this.filtroPagination.page=0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadProductos();
  }

  //Limpiar campos de filtro
  resetFiltro(){
    this.filtroPagination = {
      sortField: null,
      sortDesc: true,
      razonSocial: null,
      size: 20,
      page: 0,
      nombre: null,
      marca: null,
      codigo: null,
      codigoDeBarras: null,
      subMarca: null,
      proveedorCuit: null,
      proveedorRazonSocial: null
    }
  }

  //Caja de texto busqueda
  mostrar() {
    this.searchText = true
  }
  ocultar() {
    this.searchText = false;
  }

  // ********************************************************************************************************
  //1.4 BORRAR PRODUCTO
  confirm(data) {
    this.productoId = data
    this.abrirForm = true
  }
  volver() {
    this.modalRef.hide()
    this.abrirForm = false;
  }

  //Borrar id
  delete() {
    this.productoService.bajaProducto(this.productoId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.productos = [];
        //Paginacion
        this.items = [];
        this.filtroPagination.page = 0;
        this.pageSize = 20;
        this.totalElements = 0;
        this.totalPages = 0;

        this.loadProductos();
        this.abrirForm = false;
        this.toastr.success('Producto dado de baja con éxito');
        this.modalRef.hide()
      } else {
        // mostrar error
      }
    });
  }

  openModal(template: TemplateRef<any>, data) {
    this.productoId = data
    this.modalRef = this.modalService.show(template,this.config);
  }


}
