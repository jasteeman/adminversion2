import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import { Productos } from 'src/app/models/productos';
import { AfipService } from 'src/app/services/afip.service';


@Component({
  selector: 'app-productos-create',
  templateUrl: './productos-create.component.html',
  styleUrls: ['./productos-create.component.scss']
})
export class ProductosCreateComponent implements OnInit {
  proveedores = [];
  producto = new Productos();
  productoForm: FormGroup;
  submitted = false;
  load = false;
  tipoIVA=null;
  tipoDeIvas=[]
  constructor(private router: Router,private afipService:AfipService,private toastr: ToastrService, private proveedoresService: ProveedoresService, private productosService: ProductosService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.traerTipoDeIvas();
    this.loadProveedores();
    //Validate Form
    this.productoForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      marca: ['', Validators.required],
      subMarca: ['', [Validators.required]],
      codigo: [''],
      proveedor: ['', [Validators.required]],
      unidadMedida: ['', [Validators.required]],
      codigoDeBarras: [''],
      precioCosto: [0, [Validators.required]],
      iva: [0, Validators.required],
      utilidad: [0, Validators.required],
      precioVenta: [0, Validators.required]
    }
    );
    this.valuechange()
  }

  //1.1--Traigo los proveedores
  loadProveedores() {
    this.proveedoresService.getProveedores({ page: 0, size: 40 }).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.proveedores = data.content.content;
      }
    }, error => {
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.2-- convenience getter for easy access to form fields
  get f() { return this.productoForm.controls; }

  //1.3-- Typehead
  onSelect(event: TypeaheadMatch): void {
    this.productoForm.value.proveedor = event.item;
    this.producto.proveedor = event.item
  }

  //1.4-- Change Sumatoria
  valuechange() {
    if(this.tipoIVA!=null){
      this.productoForm.value.iva=parseInt(this.tipoIVA)
    }
    let totalSinIva = (this.productoForm.value.precioCosto * this.productoForm.value.iva) / 100;
    let precioConIVa = (totalSinIva + this.productoForm.value.precioCosto);
    let precioConUtilidad = precioConIVa * this.productoForm.value.utilidad / 100;
    let resultado = precioConUtilidad + precioConIVa;
    this.producto.precioVenta = Number(resultado.toFixed(2));
  }

  //Guardo producto
  createProducto() {

    this.submitted = true;
    this.load = true

    // // stop here if form is invalid
    if (this.productoForm.invalid) {
      this.load = false;
      return;
    }

    // //Validate
  
    if (this.productoForm.value.codigo == "") {
      this.productoForm.value.codigo = null
    }
    if (this.productoForm.value.codigoDeBarras == "") {
      this.productoForm.value.codigoDeBarras = null
    }
    if (this.productoForm.value.proveedor == null) {
      this.load = false;
      return;
    }
    this.productoForm.value.proveedor = null;
    this.productoForm.value.proveedor = this.producto.proveedor

    this.productoForm.value.precioVenta = this.producto.precioVenta;
    this.productoForm.value.iva=parseInt(this.tipoIVA)
    if(this.producto.iva==null){
      this.toastr.error("Debe seleccionar un tipo de IVA para avanzar")
      return;
    }
    if (navigator.onLine) {
      //Guardo
      this.productosService.saveOrUpdate(this.productoForm.value).subscribe(data => {
        if (data.type == "OK") {
          if (data.content.codigo == null) {
            var temp = "00" + data.content.id
            data.content.proveedor = this.producto.proveedor
            data.content.codigo = temp;
            this.productosService.saveOrUpdate(data.content).subscribe(data => {
              if (data.type == "OK") {
                this.toastr.success('Producto creado con éxito');
                this.load = false;
                this.router.navigate(['/panel/productos/details/' + data.content.id]);
              }
            })
          }
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  // ***********************************************************************************************
  // AFIP
  traerTipoDeIvas(){
    this.afipService.getTiposDeIvas().subscribe(data=>{
     this.tipoDeIvas=data.content.content;
    })
   }
}
