import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { ToastrService } from 'ngx-toastr';
import { first } from "rxjs/operators";
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';

@Component({
  selector: 'app-movimientos-stock',
  templateUrl: './movimientos-stock.component.html',
  styleUrls: ['./movimientos-stock.component.scss']
})
export class MovimientosStockComponent implements OnInit {
  productos=[];
  loading: Boolean;
  productoId: any;

  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0

  constructor(private productoService: ProductosService, private toastr: ToastrService) { }

  ngOnInit() {
  }

  loadProductos() {
    this.loading = true;
    this.productoService.findAllStock(this.productoId,this.page,this.pageSize).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.productos = data.content.content.content;
        this.loading = false;
        this.totalElements = data.content.content.totalElements
        this.totalPages = data.content.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  //Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.productos = [];
    this.loadProductos()
  }

  Transportar(event: any) {
  this.productoId=event;
  this.loadProductos();
  }
  buscar(data){
    var temp;
    data.forEach(x => {
      temp=""+x.producto.nombre+"="+x.producto.stock;
      return;
    });
    return temp;
  }

}
