import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimientosStockComponent } from './movimientos-stock.component';

describe('MovimientosStockComponent', () => {
  let component: MovimientosStockComponent;
  let fixture: ComponentFixture<MovimientosStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimientosStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimientosStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
