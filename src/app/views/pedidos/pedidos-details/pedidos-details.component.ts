import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { productoVenta } from 'src/app/models/productos';
import { ClienteCreate } from 'src/app/models/cliente';
import { ClienteService } from 'src/app/services/cliente.service';
import { PedidosService } from 'src/app/services/pedidos.service';
import { VentasService } from 'src/app/services/ventas.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AfipService } from 'src/app/services/afip.service';

@Component({
  selector: 'app-pedidos-details',
  templateUrl: './pedidos-details.component.html',
  styleUrls: ['./pedidos-details.component.scss']
})
export class PedidosDetailsComponent implements OnInit {
  id: number;
  private sub: any;
  editMode = false;
  descuento = 0;
  total = 0;

  venta = {
    clienteId: null,
    items: [],
    pagos: [],
    descuento: 0,
    observacion: null,
    condicionVenta: "CONTADO",
    condicionFrenteAlIva: null,
    pedidoVinculadoId: null
  }

  pedido = {
    cliente: new ClienteCreate(),
    items: [],
    fecha: new Date(),
    estado: null,
    observacion: null,
    descuento: 0,
    condicionVenta: "CONTADO",
    condicionFrenteAlIva: null
  }

  producto = new productoVenta();
  cargarForm: boolean;
  load: boolean;
  tempProducto;
  tempCliente = "";

  tipoComprobante = null;
  usuario = null;

  // AFIP
  puntoDeVenta = null;
  empresa = null;
  tiposDeFacturas = [];

  //STOCK
  totalPagos = 0;
  totalVenta = 0;
  totalVentaFinal = 0;

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;

  constructor(private authService: AuthService, private afipService: AfipService, private ventaService: VentasService, private route: ActivatedRoute, private router: Router, private pedidosService: PedidosService, private clienteService: ClienteService, private productosService: ProductosService, private toastr: ToastrService, private modalService: BsModalService) { }

  ngOnInit() {
    this.loadUsuarioLogueado();
    this.valuechange()
    this.cargarForm = false;
    this.load = false;
    this.editMode = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.loadPedidoDetail();
  }

  //--Traigo el cliente
  loadPedidoDetail() {
    this.pedidosService.getFull(this.id).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        var temp = data.content.fecha;
        data.content.fecha = new Date(temp)
        this.pedido = data.content;
        this.tempCliente = "" + this.pedido.cliente.nombre + " " + this.pedido.cliente.apellido + "(" + this.pedido.cliente.id + ")";
        if (data.content.condicionFrenteAlIva != null) {
          this.tipoComprobante = data.content.cliente.tipoFactura.descrWs;
          this.traerCondicionFrenteAlIva(data.content.cliente.tipoFactura.nombre);
        }
        if (this.pedido.descuento != null || this.pedido.descuento > 0) {
          this.descuento = this.pedido.descuento;
        }
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  refreshProducto() {
    this.cargarForm = false;
    this.producto = new productoVenta()
  }

  valuechange() {
    let totalSinIva = (this.producto.precioCosto * this.producto.iva) / 100;
    let precioConIVa = (totalSinIva + this.producto.precioCosto);
    let precioConUtilidad = precioConIVa * this.producto.utilidad / 100;
    let resultado = precioConUtilidad + precioConIVa;
    this.producto.precioVenta = Number(resultado.toFixed(2));
  }

  // Traigo el producto
  Transportar(event: any) {
    this.productosService.getFull(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.cargarForm = true
        this.producto = data.content
        this.producto.descuento = 0;
        this.cargarTabla();
      } else {
        // mostrar error
      }
    })
  }
  //Traig el pago
  TransportarPago(event) {
    if (event != undefined) {
      this.venta.pagos = event
    }
  }

  // **************************************************************************************************************
  //Operaciones de tablas
  cargarTabla() {
    this.producto.cantidad = 1;
    this.producto.descuento = 0;
    this.pedido.items.push({ producto: this.producto, unidadMedida: this.producto.unidadMedida, cantidad: this.producto.cantidad, precioUnitario: this.producto.precioVenta, descuento: this.producto.descuento });
    this.refreshProducto();
    this.cargarForm = false;
  }
  eliminarItem() {
    for (let i = 0; i < this.pedido.items.length; ++i) {
      if (this.pedido.items[i].id === this.tempProducto) {
        this.pedido.items.splice(i, 1);
        this.toastr.info("Producto borrado")
        this.modalRef.hide()
      }
    }
  }
  eliminarItemTabla() {
    for (let i = 0; i < this.pedido.items.length; ++i) {
      if (this.pedido.items[i].id === this.tempProducto) {
        this.pedido.items.splice(i, 1);
      }
    }
  }

  //Calcular Total
  precioTotalVenta() {
    var total = 0;
    var total2 = 0;
    var total3 = 0
    this.pedido.items.forEach(x => {
      total = total + (x.cantidad * x.precioUnitario) - x.descuento;
    });
    total2 = Math.round(total * 100) / 100;
    this.venta.descuento = Math.round(total2 * this.descuento) / 100;
    total3 = total2 - (total2 * this.descuento) / 100;
    return total3;
  }

  precioTotalIVA() {
    var total = 0;
    var total2 = 0;
    var total4 = 0
    this.pedido.items.forEach(x => {
      total4 = (x.cantidad * x.precioUnitario) - x.descuento
      total = total + Math.round(total4 * x.producto.iva) / 100;
    });
    total2 = Math.round(total * 100) / 100;
    return total2;
  }
  // Fin de operaciones de tablas 
  // **************************************************************************************************************
  // Guardar valores de pedidos
  guardar() {
    this.load = true;
    if (this.pedido.cliente.nombre == null) {
      this.toastr.info("Debe ingresar un cliente para continuar");
      this.load = false;
      return;
    }
    if (this.pedido.items.length == 0) {
      this.toastr.info("Debe ingresar un producto para continuar");
      return;
    }

    if (navigator.onLine) {
      this.pedidosService.saveOrUpdate(this.pedido).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Pedido editado con éxito');
          this.load = false;
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }


  // Recursiva de consulta de STOCK
  recursivaStock = (index, array) => new Promise((resolve, reject) => {
    if (index < array.length) {
      if (array[index].cantidad < array[index].producto.stock) {
        var temp = (array[index].precioUnitario * array[index].cantidad) - array[index].descuento;
        this.totalVenta += temp
        if (array[index].producto != null) {
          this.venta.items.push({ productoId: array[index].producto.id, cantidad: array[index].cantidad, precioUnitario: array[index].precioUnitario, descuento: array[index].descuento });
          resolve(this.recursivaStock(index + 1, array));
        } else {
          this.venta.items.push({ productoId: array[index].id, cantidad: array[index].cantidad, precioUnitario: array[index].precioUnitario, descuento: array[index].descuento })
          resolve(this.recursivaStock(index + 1, array));
        }
      } else {
        this.toastr.error("Stock insuficiente del producto " + array[index].producto.nombre + ":STOCK REAL " + array[index].producto.stock);
        this.load = false;
        this.modalRef.hide();
        return;
      }
    } else {
      return resolve("OK");
    }
  });

  //Generar Venta
  guardarVenta() {
    this.guardar();
    this.load = true;
    this.buscarCondicionFrenteAlIVA();

    //GETTER DATA PEDIDO
    this.venta.clienteId = this.pedido.cliente.id;
    this.venta.pedidoVinculadoId = this.id;
    this.venta.condicionFrenteAlIva = this.pedido.condicionFrenteAlIva;
    this.venta.observacion = this.pedido.observacion;
    this.venta.items = []

    //VALIDAR STOCK
    this.recursivaStock(0, this.pedido.items).then(data => {
      if (data == "OK") {
        if (navigator.onLine) {
          this.ventaService.saveOrUpdate(this.venta).subscribe(data => {
            if (data.type == "OK") {
              this.toastr.success('Venta creada con éxito');
              this.load = false;
              this.modalRef.hide();
              this.descuento = 0;
              this.total = 0;
              this.venta = {
                clienteId: null,
                pedidoVinculadoId: null,
                items: [],
                pagos: [],
                descuento: 0,
                observacion: null,
                condicionVenta: "CONTADO",
                condicionFrenteAlIva: null
              }
              this.router.navigate(['/panel/ventas/list']);
            } else {
              this.load = false;
              this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
              this.modalRef.hide()
              // mostrar error
            }
          })
        }
      }
    })
  }

  openModal(template: TemplateRef<any>, data) {
    this.tempProducto = data
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalGuardar(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  // *****************************************************************************************************
  // EMPRESA
  loadUsuarioLogueado() {
    this.authService.getLoggedUser().pipe(first()).subscribe(
      data => {
        if (data.type == "OK") {
          this.empresa = data.content.empresa;
          this.puntoDeVenta = data.content.puntoDeVenta;
          this.usuario = data.content;
        }
      })
  }
  traerCondicionFrenteAlIva(nombre) {
    this.tiposDeFacturas = [];
    this.afipService.getTiposDeFacturas().subscribe(data => {
      var temp = data.content.content;
      temp.forEach(element => {
        if (element.nombre == nombre) {
          this.tiposDeFacturas.push(element);
        }
      });
    })
  }

  buscarCondicionFrenteAlIVA() {
    if (this.tipoComprobante != null) {
      this.tiposDeFacturas.forEach(x => {
        if (this.tipoComprobante == x.descrWs) {
          this.pedido.condicionFrenteAlIva = x;
        }
      })
    }
  }

}
