import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { productoVenta } from 'src/app/models/productos';
import { ClienteCreate } from 'src/app/models/cliente';
import { ClienteService } from 'src/app/services/cliente.service';
import { PedidosService } from 'src/app/services/pedidos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AfipService } from 'src/app/services/afip.service';


@Component({
  selector: 'app-pedidos-create',
  templateUrl: './pedidos-create.component.html',
  styleUrls: ['./pedidos-create.component.scss']
})
export class PedidosCreateComponent implements OnInit {
  // Variables
  pedido = {
    cliente: new ClienteCreate(),
    items: [],
    fecha: new Date(),
    estado: "EN_ESPERA",
    observacion: null,
    descuento: 0,
    condicionVenta: "CONTADO",
    condicionFrenteAlIva: null
  }
  descuento = 0;
  producto = new productoVenta()
  cargarForm: boolean;
  load: boolean;
  tempProducto;
  tempCliente = "";
  tipoComprobante = null;
  usuario = null;

  // AFIP
  puntoDeVenta = null;
  empresa = null;
  tiposDeFacturas = [];

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;


  constructor(private authService: AuthService, private afipService: AfipService, private router: Router, private pedidosService: PedidosService, private clienteService: ClienteService, private productosService: ProductosService, private toastr: ToastrService, private modalService: BsModalService) { }

  ngOnInit() {
    this.loadUsuarioLogueado();
    this.valuechange()
    this.cargarForm = false;
    this.load = false;
  }

  refreshProducto() {
    this.cargarForm = false;
    this.producto = new productoVenta();
  }

  valuechange() {
    let totalSinIva = (this.producto.precioCosto * this.producto.iva) / 100;
    let precioConIVa = (totalSinIva + this.producto.precioCosto);
    let precioConUtilidad = precioConIVa * this.producto.utilidad / 100;
    let resultado = precioConUtilidad + precioConIVa;
    this.producto.precioVenta = Number(resultado.toFixed(2));
  }

  //Selecciono la venta anonima de clientes
  checkValue(event: any) {
    if (event == true) {
      this.tempCliente = null;
      this.pedido.cliente = null;
      this.tipoComprobante = null;
    }
  }

  //Traigo el producto
  Transportar(event: any) {
    this.productosService.getFull(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.cargarForm = true
        this.producto = data.content
        this.producto.cantidad = 1;
        this.producto.descuento = 0;
        this.cargarTabla();
      } else {
        // mostrar error
      }
    })
  }

  //Traigo el cliente
  TransportarCliente(event: any) {
    this.tipoComprobante = null;
    this.tiposDeFacturas = [];
    this.clienteService.getFull(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        var temp = data.content.fechaNacimiento;
        data.content.fechaNacimiento = new Date(temp)
        this.pedido.cliente = data.content;
        this.tempCliente = "" + this.pedido.cliente.nombre + " " + this.pedido.cliente.apellido + "(" + this.pedido.cliente.id + ")";
        if (data.content.tipoFactura != null) {
          this.tipoComprobante = data.content.tipoFactura.descrWs;
          this.traerCondicionFrenteAlIva(data.content.tipoFactura.nombre);
        }
      }
    })
  }

  // **************************************************************************************************************
  //Operaciones de tablas
  cargarTabla() {
    this.pedido.items.push(this.producto);
    this.refreshProducto();
    this.cargarForm = false;
  }
  eliminarItem() {
    for (let i = 0; i < this.pedido.items.length; ++i) {
      if (this.pedido.items[i].id === this.tempProducto) {
        this.pedido.items.splice(i, 1);
        this.toastr.info("Producto borrado")
        this.modalRef.hide()
      }
    }
  }
  eliminarItemTabla() {
    for (let i = 0; i < this.pedido.items.length; ++i) {
      if (this.pedido.items[i].id === this.tempProducto) {
        this.pedido.items.splice(i, 1);
      }
    }
  }

  //Calcular Total
  precioTotalVenta() {
    var total = 0;
    var total2 = 0;
    var total3 = 0;
    this.pedido.items.forEach(x => {
      total = total + (x.cantidad * x.precioVenta) - x.descuento;
    });
    total2 = Math.round(total * 100) / 100;
    this.pedido.descuento = Math.round(total2 * this.descuento) / 100;
    total3 = total2 - (total2 * this.descuento) / 100;
    // this.pedido.descuento = Math.round(total2 * this.pedido.descuento) / 100;
    // $scope.total = total2 - (total2 * $scope.descuento) / 100;
    return total3;
  }

  precioTotalIVA() {
    var total = 0;
    var total2 = 0;
    var total4 = 0
    this.pedido.items.forEach(x => {
      total4 = (x.cantidad * x.precioVenta) - x.descuento
      total = total + Math.round(total4 * x.iva) / 100;
    });
    total2 = Math.round(total * 100) / 100;
    return total2;
  }
  // Fin de operaciones de tablas
  // **************************************************************************************************************
  // GUARDAR PEDIDO
  guardar() {
    this.load = true;
    this.modalRef.hide()
    this.buscarCondicionFrenteAlIVA();
    if (this.pedido.cliente.nombre == null) {
      this.toastr.info("Debe ingresar un cliente para continuar");
      this.load = false;
      return;
    }
    if (this.pedido.items.length == 0) {
      this.toastr.info("Debe ingresar un producto para continuar");
      return;
    }

    var tempItems = this.pedido.items
    this.pedido.items = []
    tempItems.forEach(x => {
      this.pedido.items.push({ producto: x, unidadMedida: x.unidadMedida, cantidad: x.cantidad, precioUnitario: x.precioVenta, descuento: x.descuento })
    })

    if (navigator.onLine) {
      this.pedidosService.saveOrUpdate(this.pedido).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Pedido creado con éxito');
          this.load = false;
          this.pedido = {
            cliente: null,
            items: [],
            fecha: new Date(),
            estado: "EN_ESPERA",
            observacion: null,
            descuento: 0,
            condicionVenta: "CONTADO",
            condicionFrenteAlIva: null
          };
          this.tempCliente = ""
          this.router.navigate(['/panel/pedidos/']);
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  openModal(template: TemplateRef<any>, data) {
    this.tempProducto = data
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalGuardar(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  // *****************************************************************************************************
  // EMPRESA
  loadUsuarioLogueado() {
    this.authService.getLoggedUser().pipe(first()).subscribe(
      data => {
        if (data.type == "OK") {
          this.empresa = data.content.empresa;
          this.puntoDeVenta = data.content.puntoDeVenta;
          this.usuario = data.content;
        }
      })
  }

  traerCondicionFrenteAlIva(nombre) {
    this.tiposDeFacturas = [];
    this.afipService.getTiposDeFacturas().subscribe(data => {
      var temp = data.content.content;
      temp.forEach(element => {
        if (element.nombre == nombre) {
          this.tiposDeFacturas.push(element);
        }
      });
    })
  }

  buscarCondicionFrenteAlIVA() {
    if (this.tipoComprobante != null) {
      this.tiposDeFacturas.forEach(x => {
        if (this.tipoComprobante == x.descrWs) {
          this.pedido.condicionFrenteAlIva = x;
        }
      })
    }
  }

}
