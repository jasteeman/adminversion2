import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from "rxjs/operators";
import { ClienteService } from 'src/app/services/cliente.service';
import { PedidosService } from 'src/app/services/pedidos.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import data from 'src/app/models/localidades';
import { TypeaheadMatch } from 'ngx-bootstrap';

@Component({
  selector: 'app-pedidos-list',
  templateUrl: './pedidos-list.component.html',
  styleUrls: ['./pedidos-list.component.scss']
})
export class PedidosListComponent implements OnInit {
  //  Variables
  selectedRow = [];
  setClickedRow: Function;
  filtroPagination = {
    size: 20,
    page: 0,
    clienteId: null,
    localidad: null,
    estado: "EN_ESPERA",
    desde: null,
    hasta: null
  }
  tempCliente = "";
  productos: any;
  loading: Boolean;
  localidades = data.data;
  ciudades = [];
  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;

  //Reporte
  report = () => {
    console.log(this.selectedRow)
    window.open(`${environment.apiUrl}` + '/report/pedidos/' + this.selectedRow, '_blank');
  }


  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0

  constructor(private router: Router, private pedidosService: PedidosService, private clienteService: ClienteService, private toastr: ToastrService, private modalService: BsModalService) {
    this.setClickedRow = function (index) {
      if (this.selectedRow.indexOf(index.id) > -1) {
        this.selectedRow.splice(this.selectedRow.indexOf(index.id), 1);
      }
      else {
        this.selectedRow.push(index.id);
      }
    }
  }


  ngOnInit() {
    this.loading = false;
    this.loadPedidos();

    //Asignacion de localidad
    this.localidades.forEach(x => {
      if (x.nombre == "Santa Fe") {
        this.ciudades = x.ciudades;
      }
    })

  }

  //Binding Cliente
  TransportarCliente(event: any) {
    this.clienteService.getFull(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.filtroPagination.clienteId = data.content.id;
        this.tempCliente = "" + data.content.nombre + " " + data.content.apellido + "(" + data.content.id + ")";
        this.buscador()
      }
    })
  }

  //Lista de pedidos
  loadPedidos() {
    this.loading = true;
    this.pedidosService.getPedidos(this.filtroPagination).pipe(first()).subscribe(data => {
      this.loading = false;
      if (data.type == "OK") {
        this.items = data.content.content;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
        // this.resetFiltro()
      }
    })
  }
  //Paginacion
  pageChanged(event: any): void {
    this.filtroPagination.page = event.page - 1;
    this.items = [];
    this.loadPedidos()
  }

  //Refresh lista
  refresh() {
    this.items = [];
    this.resetFiltro();
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.tempCliente = ""
    this.loadPedidos();
  }

  //Busqueda Personalizada
  buscador() {
    this.filtroPagination.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadPedidos();

  }

  //Limpiar campos de filtro
  resetFiltro() {
    this.filtroPagination = {
      size: 20,
      page: 0,
      clienteId: null,
      localidad: null,
      estado: "EN_ESPERA",
      desde: null,
      hasta: null
    }
  }

  //Capturo select ciudades
  onSelect(event: TypeaheadMatch): void {
    this.filtroPagination.localidad = event.value
    this.buscador()
  }

  //Capturo estado de pedidos
  onPercentChange() {
    this.buscador()
  }

  //Borrado del pedido
  openModal(template: TemplateRef<any>, data) {
    this.productos = data
    this.modalRef = this.modalService.show(template,this.config);
  }
  delete() {
    this.productos.estado = "CANCELADO";
    this.modalRef.hide()
    if (navigator.onLine) {
      this.pedidosService.saveOrUpdate(this.productos).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Pedido Cancelado con éxito ');
          this.loadPedidos()
        } else {
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
}
