import { Component, OnInit, TemplateRef } from '@angular/core';
import { EmpresaService } from 'src/app/services/empresa.service';
import { Empresa } from 'src/app/models/empresa';
import { ToastrService } from 'ngx-toastr';
import { AfipService } from 'src/app/services/afip.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { first } from 'rxjs/operators';
import { ConfiguracionService } from 'src/app/services/configuracion.service';

@Component({
  selector: 'app-configuraciones-list',
  templateUrl: './configuraciones-list.component.html',
  styleUrls: ['./configuraciones-list.component.scss']
})
export class ConfiguracionesListComponent implements OnInit {

  //EMPRESA
  empresa = new Empresa();
  listEmpresas = [];
  empresaId = null;
  conceptoEmpresa = null;

  //PUNTOS DE VENTAS
  puntoDeVenta = { nombre: null, sucursal: null, id: null };
  puntosDeVentas = [];
  puntoDeVentaId = null;

  //GENERALES
  loading = false;
  tipoMonedaPredeterminada = null;
  condicionFiscal = null;
  load = false;
  abrirForm = false;
  abrirFormVenta = false;
  abrirFormAfip=false;

  // AFIP
  condicionesFrenteAlIva = [];
  tipoMonedas = []
  conceptosEmpresa = [{ nombreConcepto: "PRODUCTO", nroConcepto: 1 }, { nombreConcepto: "SERVICIO", nroConcepto: 2 }, { nombreConcepto: "PRODUCTO Y SERVICIO", nroConcepto: 3 }];

  constructor(private configuracionService:ConfiguracionService,private modalService: BsModalService, private empresaService: EmpresaService, private toastr: ToastrService, private afipService: AfipService) { }

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;

  ngOnInit() {
    this.traerCondicionFrenteAlIva();
    this.traerTipoMonedas();
    this.loadEmpresas();
    this.loadPuntosDeVentas();
  }
  // *************************************************EMPRESAS**********************************************
  //1.1---Traer lista
  loadEmpresas() {
    this.loading = true;
    this.empresaService.getEmpresas().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.loading = false;
        this.listEmpresas = data.content.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }
  //1.2 BORRAR EMPRESA
  confirm(data) {
    this.empresaId = data
  }

  volver() {
    this.modalRef.hide()
  }

  //Borrar id
  delete() {
    this.empresaService.delete(this.empresaId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.listEmpresas = [];
        //Paginacion
        this.loadEmpresas();
        this.toastr.success('Empresa eliminada con éxito');
        this.modalRef.hide();
      } else {
        // mostrar error
      }
    });
  }
  openModal(template: TemplateRef<any>, data) {
    this.empresaId = data
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalCreate() {
    this.abrirForm = true;
  }
  openModalCreateVenta() {
    this.abrirFormVenta = true;
  }
  cerrarForm() {
    this.abrirForm = false;
  }
  cerrarFormVenta() {
    this.abrirFormVenta = false;
  }
  createEmpresa() {
    this.load = true;
    if (navigator.onLine) {
        this.buscarConcepto();
        this.buscarCondicionFiscal();
        this.buscarTipoMoneda();
      if (this.empresa.ingresosBrutos == null || this.empresa.fechaInicioActividad == null || this.empresa.direccion == null || this.empresa.cuit == null
        || this.empresa.condicionFrenteAlIva == null || this.empresa.concepto == null || this.empresa.concepto == null || this.empresa.nombre == null
        || this.empresa.password == null || this.empresa.sedeTimbrado == null || this.empresa.telefono == null || this.empresa.tipoMonedaPredeterminada == null) {
          console.log(this.empresa)
        this.toastr.error("Debe completar los campos requeridos para continuar")
        this.load = false;
        return;
      }

      this.buscarConcepto();
      this.buscarCondicionFiscal();
      this.buscarTipoMoneda();
      this.empresaService.saveOrUpdate(this.empresa).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success("Empresa Creada con éxito");
          this.empresa = new Empresa();
          this.load = false;
          this.abrirForm = false;
          this.loadEmpresas()
        }
      })
    } else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
  // ********************************************************************************************************
  // PUNTO DE VENTAS
  //1.1---Traer lista
  loadPuntosDeVentas() {
    this.loading = true;
    this.afipService.getPuntoDeVentas().pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.loading = false;
        this.puntosDeVentas = data.content.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //Borrar id
  deletePuntoDeVenta() {
    this.afipService.deletePuntoDeVenta(this.puntoDeVentaId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.puntosDeVentas = [];
        //Paginacion
        this.loadPuntosDeVentas();
        this.toastr.success('Punto de venta eliminado con éxito');
        this.modalRef.hide()
      } else {
        console.log(data)
        // mostrar error
      }
    });
  }
  openModalPuntoDeVenta(template: TemplateRef<any>, data) {
    this.puntoDeVentaId = data
    this.modalRef = this.modalService.show(template, this.config);
  }
  createPuntoDeVenta() {
    this.load = true;
    if (this.puntoDeVenta.nombre == null || this.puntoDeVenta.sucursal == null) {
      this.toastr.error("Debe completar los campos para continuar");
      this.load = false;
      return;
    }
    if (navigator.onLine) {
      this.afipService.saveOrUpdatePuntoDeVenta(this.puntoDeVenta).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success("Punto de Venta Creado con éxito");
          this.abrirFormVenta = false;
          this.loadPuntosDeVentas();
          this.puntoDeVenta = { nombre: null, sucursal: null, id: null };
          this.load = false;
          console.log(data)
        }
      })
    } else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }
  editar(data) {
    this.empresa = data;
    this.tipoMonedaPredeterminada = data.tipoMonedaPredeterminada.nombre;
    this.condicionFiscal = data.condicionFrenteAlIva.nombre;
    this.empresa.fechaInicioActividad = new Date(data.fechaInicioActividad);
    this.abrirForm = true;
  }
  editarVenta(data) {
    this.puntoDeVenta = data;
    this.abrirFormVenta = true;
  }

  // ******************************************************************************************************
  // AFIP CAMPOS
  traerCondicionFrenteAlIva() {
    this.afipService.getCondicionDeIva().subscribe(data => {
      this.condicionesFrenteAlIva = data.content.content;
    })
  }
  traerTipoMonedas() {
    this.afipService.getTipoMonedas().subscribe(data => {
      this.tipoMonedas = data.content.content;
    })
  }

  //Retornar objetos para afip
  buscarCondicionFiscal() {
    if (this.condicionFiscal != null) {
      this.condicionesFrenteAlIva.forEach(x => {
        if (this.condicionFiscal == x.nombre) {
          this.empresa.condicionFrenteAlIva = x;
        }
      })
    }
  }
  buscarTipoMoneda() {
    if (this.tipoMonedaPredeterminada != null) {
      this.tipoMonedas.forEach(x => {
        if (this.tipoMonedaPredeterminada == x.nombre) {
          this.empresa.tipoMonedaPredeterminada = x;
        }
      })
    }
  }
  buscarConcepto() {
    if (this.conceptoEmpresa != null) {
      this.conceptosEmpresa.forEach(x => {
        if (this.conceptoEmpresa == x.nombreConcepto) {
          this.empresa.concepto = x.nombreConcepto;
        }
      })
    }
  }

  // ********************************************************************************************************
  // AFIP CONFIGS


}
