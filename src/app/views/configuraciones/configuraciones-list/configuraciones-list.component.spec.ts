import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiguracionesListComponent } from './configuraciones-list.component';

describe('ConfiguracionesListComponent', () => {
  let component: ConfiguracionesListComponent;
  let fixture: ComponentFixture<ConfiguracionesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfiguracionesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiguracionesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
