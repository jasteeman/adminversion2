import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeriadoCreateComponent } from './feriado-create.component';

describe('FeriadoCreateComponent', () => {
  let component: FeriadoCreateComponent;
  let fixture: ComponentFixture<FeriadoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeriadoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeriadoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
