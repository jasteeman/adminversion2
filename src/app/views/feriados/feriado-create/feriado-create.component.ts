import { Component, OnInit } from '@angular/core';
import {  FeriadosService } from 'src/app/services/feriados.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute,Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-feriado-create',
  templateUrl: './feriado-create.component.html',
  styleUrls: ['./feriado-create.component.scss']
})
export class FeriadoCreateComponent implements OnInit {
  feriado={desde:null,hasta:null,motivo:null}
  load=false;
  constructor(private route: ActivatedRoute, private feriadoService: FeriadosService,private toastr: ToastrService,private router:Router,private formBuilder: FormBuilder) { }

  ngOnInit() {
  }
   //1.3-- Guardo los datos del cliente
   createFeriado(){
    this.load=true

    // stop here if form is invalid
    if (this.feriado.desde==null||this.feriado.hasta==null||this.feriado.motivo==null) {
      this.load=false;
      this.toastr.info("Campos incompletos. Complete y vuelve a intentar");
        return;
    }
    if(navigator.onLine){
    //Guardo
    this.feriadoService.saveOrUpdate(this.feriado).subscribe(data=>{
      if (data.type == "OK") {
         this.toastr.success('Feriado creado con éxito');
         this.load=false;
         this.router.navigate(['/panel/feriados/details/'+data.content.id]);
       } else {
         console.log(data)
         this.load=false;
        this.toastr.info("ERROR N°"+data.errorNumber+":"+data.message);
        // mostrar error
      }
    })
    }else{
      this.load=false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }

  }
}
