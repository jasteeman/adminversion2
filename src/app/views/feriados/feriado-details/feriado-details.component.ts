import { Component, OnInit } from '@angular/core';
import { FeriadosService } from 'src/app/services/feriados.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-feriado-details',
  templateUrl: './feriado-details.component.html',
  styleUrls: ['./feriado-details.component.scss']
})
export class FeriadoDetailsComponent implements OnInit {
  feriado={desde:new Date(),hasta:new Date(),motivo:null}
  id: number;
  sub: any;
  editMode = false;
  load = false;

  constructor(private route: ActivatedRoute, private feriadoService: FeriadosService, private toastr: ToastrService, private router: Router) { }


  ngOnInit() {
    this.editMode = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.loadFeriadoDetail();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  //1.1--Traigo el cliente 
  loadFeriadoDetail() {
    this.feriadoService.getFull(this.id).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.feriado = data.content;
        this.feriado.desde=new Date(this.feriado.desde)
        this.feriado.hasta=new Date(this.feriado.hasta)
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.2--Habilitar y desabilitar form
  unlockEditMode() {
    this.editMode = true;
  }
  lockEditMode() {
    this.editMode = false;
  }

  //1.3-- Guardo los datos actualizados del cliente
  updateFeriado() {
    this.load = true;
    if (navigator.onLine) {
      this.feriadoService.saveOrUpdate(this.feriado).subscribe(data => {
        if (data.type == "OK") {
          this.editMode = false;
          this.load = false;
          this.toastr.success('Feriado actualizado con éxito');
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

}
