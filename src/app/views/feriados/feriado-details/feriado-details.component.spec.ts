import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeriadoDetailsComponent } from './feriado-details.component';

describe('FeriadoDetailsComponent', () => {
  let component: FeriadoDetailsComponent;
  let fixture: ComponentFixture<FeriadoDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeriadoDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeriadoDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
