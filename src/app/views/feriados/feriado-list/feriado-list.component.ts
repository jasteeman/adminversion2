import { Component, OnInit, TemplateRef,Input, Output } from '@angular/core';
import { FeriadosService } from 'src/app/services/feriados.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router} from '@angular/router';

@Component({
  selector: 'app-feriado-list',
  templateUrl: './feriado-list.component.html',
  styleUrls: ['./feriado-list.component.scss']
})
export class FeriadoListComponent implements OnInit {

   // Variables
   loading: Boolean;
   abrirForm: Boolean;
   feriadoId: any;
   feriados : [];
 
   //Paginacion
   items = [];
   page = 0;
   pageSize = 20;
   totalElements = 0;
   totalPages = 0;
 
   //  Modal
   config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
 
   constructor(private route: Router,private feriadosService: FeriadosService, private toastr: ToastrService, private modalService: BsModalService) {
    }

  ngOnInit() {
    this.feriados=[]
    this.loadFeriados();
  }

  //1.1---Traer lista
  loadFeriados() {
    this.loading = true;
    this.feriadosService.getFeriados(this.page, this.pageSize).pipe(first()).subscribe(data => {
     if (data.type == "OK") {
        this.loading = false;
        this.feriados = data.content.content;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  // 1.2 Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.feriados = [];
    this.loadFeriados()
  }

  //1.3 Refresh lista
  refresh() {
    this.feriados = [];
    //Paginacion
    this.items = [];
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;

    this.loadFeriados();
  }

  // ********************************************************************************************************
  //1.4 BORRAR CLIENTE
  confirm(data) {
    this.feriadoId = data
    this.abrirForm = true
  }
  volver() {
    this.modalRef.hide()
    this.abrirForm = false;
  }

  //Borrar id
  delete() {
    this.feriadosService.delete(this.feriadoId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.feriados = [];
        //Paginacion
        this.items = [];
        this.page = 0;
        this.pageSize = 20;
        this.totalElements = 0;
        this.totalPages = 0;

        this.loadFeriados();
        this.abrirForm = false;
        this.toastr.success('Feriado eliminado con éxito');
        this.modalRef.hide()
      } else {
        // mostrar error
      }
    });
  }

  openModal(template: TemplateRef<any>, data) {
    this.feriadoId = data
    this.modalRef = this.modalService.show(template,this.config);
  }

}
