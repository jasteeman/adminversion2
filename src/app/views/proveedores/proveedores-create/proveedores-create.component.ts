import { Component, OnInit } from '@angular/core';
import {  ProveedoresService } from 'src/app/services/proveedores.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute,Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import data from 'src/app/models/localidades';
import { TypeaheadMatch } from 'ngx-bootstrap';

@Component({
  selector: 'app-proveedores-create',
  templateUrl: './proveedores-create.component.html',
  styleUrls: ['./proveedores-create.component.scss']
})
export class ProveedoresCreateComponent implements OnInit {

  localidades=data.data;
  ciudades:any;
  proveedorForm: FormGroup;
  submitted = false;
  load=false;
  constructor(private route: ActivatedRoute, private proveedorService: ProveedoresService,private toastr: ToastrService,private router:Router,private formBuilder: FormBuilder) { }

  ngOnInit() {
     //Validate Form
     this.proveedorForm = this.formBuilder.group({
      razonSocial: ['', Validators.required],
      cuit: ['', Validators.required],
      provincia: ['', [Validators.required]],
      localidad: ['', Validators.required],
      telefono: ['',Validators.required]
  })
  }

   //1.1-- convenience getter for easy access to form fields
   get f() { return this.proveedorForm.controls; }
  
   //1.2--Traigo las localidades de un provincia
     onSelect(event: TypeaheadMatch): void {
       this.ciudades = event.item.ciudades;
     }
   
      //1.3-- Guardo los datos del cliente
   createProveedor(){
    
    this.submitted = true;
    this.load=true

    // stop here if form is invalid
    if (this.proveedorForm.invalid) {
      this.load=false;
        return;
    }
    if(navigator.onLine){
    //Guardo
    this.proveedorService.saveOrUpdate(this.proveedorForm.value).subscribe(data=>{
      if (data.type == "OK") {
         this.toastr.success('Proveedor creado con éxito');
         this.load=false;
         this.router.navigate(['/panel/proveedores/details/'+data.content.id]);
       } else {
         console.log(data)
         this.load=false;
        this.toastr.info("ERROR N°"+data.errorNumber+":"+data.message);
        // mostrar error
      }
    })
    }else{
      this.load=false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }

  }
 
}
