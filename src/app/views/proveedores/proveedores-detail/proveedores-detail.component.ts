import { Component, OnInit } from '@angular/core';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import data from 'src/app/models/localidades';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { Proveedores } from 'src/app/models/proveedores';

@Component({
  selector: 'app-proveedores-detail',
  templateUrl: './proveedores-detail.component.html',
  styleUrls: ['./proveedores-detail.component.scss']
})
export class ProveedoresDetailComponent implements OnInit {
  proveedor = new Proveedores();
  id: number;
  sub: any;
  editMode = false;
  localidades = data.data;
  ciudades: any;
  load = false;

  constructor(private route: ActivatedRoute, private proveedorService: ProveedoresService, private toastr: ToastrService, private router: Router, private formBuilder: FormBuilder) { }


  ngOnInit() {
    this.editMode = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.loadProveedorDetail();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  //1.1--Traigo el cliente 
  loadProveedorDetail() {
    this.proveedorService.getFull(this.id).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.proveedor = data.content;
        this.traerLocalidades(this.proveedor.provincia);
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.2--Habilitar y desabilitar form
  unlockEditMode() {
    this.editMode = true;
  }
  lockEditMode() {
    this.editMode = false;
  }

  //1.3-- Guardo los datos actualizados del cliente
  updateProveedor() {
    this.load = true;
    if (navigator.onLine) {
      this.proveedorService.saveOrUpdate(this.proveedor).subscribe(data => {
        if (data.type == "OK") {
          this.editMode = false;
          this.load = false;
          this.toastr.success('Proveedor actualizado con éxito');
        } else {
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  //1.4--Traigo las ciudades de la provincia
  traerLocalidades(data) {
    this.localidades.forEach(element => {
      if (element.nombre == data) {
        this.ciudades = element.ciudades;
      }
    });
  }
  onSelect(event: TypeaheadMatch): void {
    this.ciudades = event.item.ciudades;
  }

}
