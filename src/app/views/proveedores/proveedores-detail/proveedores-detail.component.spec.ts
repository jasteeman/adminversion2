import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProveedoresDetailComponent } from './proveedores-detail.component';

describe('ProveedoresDetailComponent', () => {
  let component: ProveedoresDetailComponent;
  let fixture: ComponentFixture<ProveedoresDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProveedoresDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProveedoresDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
