import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Proveedores } from 'src/app/models/proveedores';
@Component({
  selector: 'app-proveedores-list',
  templateUrl: './proveedores-list.component.html',
  styleUrls: ['./proveedores-list.component.scss']
})
export class ProveedoresListComponent implements OnInit {
  
   // Variables
   loading: Boolean;
   abrirForm: Boolean;
   proveedorId: any;
   searchText: Boolean;
   proveedores:Array<Proveedores> = [];
 
   //Paginacion
   items = [];
   page = 0;
   pageSize = 20;
   totalElements = 0;
   totalPages = 0;

   //Filtro paginacion
   filtroPagination = {
    cuit: null,
    razonSocial: null,
    size: 40,
    page: 0
}
 
   //  Modal
   config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
   constructor(private proveedorService: ProveedoresService, private toastr: ToastrService, private modalService: BsModalService) {
   }


  ngOnInit() {
    this.loadProveedor();
    this.abrirForm = false
    this.proveedores=[]
  }

  
  //1.1---Traer lista
  loadProveedor() {
    this.loading = true;
    this.proveedorService.getProveedores(this.filtroPagination).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.loading = false;
        this.proveedores = data.content.content;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  // 1.2 Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.proveedores = [];
    this.loadProveedor()
  }

  //1.3 Refresh lista
  refresh() {
    this.proveedores = [];
    //Paginacion
    this.items = [];
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;

    this.loadProveedor();
  }



  //Caja de texto busqueda
  mostrar() {
    this.searchText = true
  }
  ocultar() {
    this.searchText = false;
  }

  // ********************************************************************************************************
  //1.4 BORRAR CLIENTE
  confirm(data) {
    this.proveedorId = data
    this.abrirForm = true
  }
  volver() {
    this.modalRef.hide()
    this.abrirForm = false;
  }

  //Borrar id
  delete() {
    this.proveedorService.delete(this.proveedorId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.proveedores = [];
        //Paginacion
        this.items = [];
        this.page = 0;
        this.pageSize = 20;
        this.totalElements = 0;
        this.totalPages = 0;

        this.loadProveedor();
        this.abrirForm = false;
        this.toastr.success('Proveedor eliminado con éxito');
        this.modalRef.hide()
      } else {
        // mostrar error
      }
    });
  }

  openModal(template: TemplateRef<any>, data) {
    this.proveedorId = data
    this.modalRef = this.modalService.show(template,this.config);
  }

}
