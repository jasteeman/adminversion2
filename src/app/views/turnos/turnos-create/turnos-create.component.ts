import { Component, OnInit, TemplateRef } from '@angular/core';
import { first } from 'rxjs/operators';
import { TratamientosService } from 'src/app/services/tratamientos.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TurnosService } from 'src/app/services/turnos.service';
import { ContableService } from 'src/app/services/contable.service';
import { objetoTratamiento } from 'src/app/models/turnos';
import { PageChangedEvent } from 'ngx-bootstrap';
import * as moment from 'moment';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-turnos-create',
  templateUrl: './turnos-create.component.html',
  styleUrls: ['./turnos-create.component.scss']
})
export class TurnosCreateComponent implements OnInit {
  setClickedRow: Function;
  setClickedRowHabitaciones: Function;

  load = false;
  loading = false;
  loadTurnos = false;
  loadTurnoFinal = false;
  clienteId = null;
  listaPago: any;
  profesionalId: null;
  usuario=null;

  // VARIABLES DE TRATAMIENTOS
  tratamientoId = null;
  tratamientosComprados = [];
  tratamientoAdarBaja = { id: null, sesiones: null, motivo: null };
  tratamientosPorComprar = [];
  itemsTratamientosComprados = [];
  itemsTurnosOcupados = [];
  motivo = null;
  tempRazon = null;
  turno = null;

  //VARIABLES PARA SESIONES Y TURNOS
  tablaSesiones = [];
  listaTurnosOcupados = [];
  listaDeFechasSesiones = [];
  listaTempTurnos = { disponibilidad: [], fecha: { desde: null, hasta: null }, id: null };
  selectedSesiones = [];
  selectedHabitacion = []
  listaDeFechas = [];
  Objetosrecursivo = { tempHora: [], fecha: {}, listaFinal: [], objeto: {}, temp: null, temp2: null }
  fecha = { desde: new Date(), hasta: new Date() };
  fechaActualizar = { desde: null, hasta: null };
  habitaciones = null;

  //Paginacion
  items = [];
  page = 0;
  pageSize = 5;
  totalElements = 0;
  totalPages = 0;

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;

  openModalNuevoCliente(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }
  TransportarClienteNuevo(event:any){
    if(event!=null){
      this.loadTurnos = true;
      this.modalRef.hide();
      this.contableService.saldoCliente(event).pipe(first()).subscribe(data => {
        if (data.type == "OK") {
          this.clienteId = data.content;
          this.tablaSesiones = [];
          this.listaTurnosOcupados = [];
          this.listaDeFechasSesiones = [];
          this.selectedHabitacion = []
          this.listaTempTurnos = { disponibilidad: [], fecha: { desde: null, hasta: null }, id: null };
          this.selectedSesiones = [];
          this.itemsTurnosOcupados = [];
          this.listaDeFechas = [];
          this.Objetosrecursivo = { tempHora: [], fecha: {}, listaFinal: [], objeto: {}, temp: null, temp2: null }
          this.fecha = { desde: new Date(), hasta: new Date() };
          this.fecha.hasta.setHours(this.fecha.hasta.getHours() + 1);
          this.habitaciones = null;
          this.traerTratamientosComprados(this.clienteId.cliente.id);
          this.loadTurnos = false;
        }
      })
    }
  }

  constructor(private turnoService: TurnosService,private usuarioService:UsuarioService,private contableService: ContableService, private modalService: BsModalService, private route: ActivatedRoute, private tratamientoService: TratamientosService, private toastr: ToastrService, private router: Router) {
    this.setClickedRow = function (index) {
      if (this.selectedSesiones.indexOf(index) > -1) {
        this.selectedSesiones.splice(this.selectedSesiones.indexOf(index), 1);
      }
      else {
        this.selectedSesiones.push(index);
      }
    }
    this.setClickedRowHabitaciones = function (index) {
      if (this.selectedHabitacion.indexOf(index) > -1) {
        this.selectedHabitacion.splice(this.selectedHabitacion.indexOf(index), 1);
      }
      else {
        this.selectedHabitacion.push(index);
      }
    }
  }
  ngOnInit() {

  }

  TransportarCliente(event: any) {
    this.loadTurnos = true;
    this.contableService.saldoCliente(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.clienteId = data.content;
        this.tablaSesiones = [];
        this.listaTurnosOcupados = [];
        this.listaDeFechasSesiones = [];
        this.selectedHabitacion = []
        this.listaTempTurnos = { disponibilidad: [], fecha: { desde: null, hasta: null }, id: null };
        this.selectedSesiones = [];
        this.itemsTurnosOcupados = [];
        this.listaDeFechas = [];
        this.Objetosrecursivo = { tempHora: [], fecha: {}, listaFinal: [], objeto: {}, temp: null, temp2: null }
        this.fecha = { desde: new Date(), hasta: new Date() };
        this.fecha.hasta.setHours(this.fecha.hasta.getHours() + 1);
        this.habitaciones = null;
        this.traerTratamientosComprados(this.clienteId.cliente.id);
        this.loadTurnos = false;
      }
    })
  }

  actualizarSaldoCliente(id) {
    this.contableService.saldoCliente(id).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.clienteId = data.content;
        this.traerTratamientosComprados(this.clienteId.cliente.id)
      }
    })
  }
  // ***************************************************************************************************
  //1-GESTION DE TRATAMIENTOS
  //1.1Lista de tratamiento comprados
  traerTratamientosComprados(id) {
    this.loading = true;
    this.load = true;
    this.tratamientoService.tratamientosComprados(id).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        var temp = [];
        temp = data.content;
        this.tratamientosComprados = temp.reverse();
        this.itemsTratamientosComprados = this.tratamientosComprados.slice(this.page, 10);
        this.loading = false;
        this.load = false;
      }
    })
  }
  //Paginacion de tratamientos comprados
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.itemsTratamientosComprados = this.tratamientosComprados.slice(startItem, endItem);
  }
  pageChangedTurnosOcupados(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.itemsTurnosOcupados = this.listaTempTurnos.disponibilidad.slice(startItem, endItem);
  }

  // 1.2
  //Baja de tratamiento comun(modal)
  openModalBorrarTratamiento(template: TemplateRef<any>, id, sesiones, motivo) {
    this.tratamientoAdarBaja.id = id;
    this.tratamientoAdarBaja.sesiones = sesiones;
    this.tratamientoAdarBaja.motivo = motivo;
    this.modalRef = this.modalService.show(template, this.config);
  }

  darDeBajaTratamiento() {
    var temp = 0;
    this.loading = true;
    this.load = true;
    //Valido las sesiones del tratamiento
    this.tratamientoAdarBaja.sesiones.forEach(x => {
      if (x.enabled == true && x.completa == false && x.turnoId != null) {
        temp++
      }
    });
    if (temp != 0) {
      this.toastr.error("El tratamiento posee turnos pendientes,eliminelo y luego intente nuevamente");
      this.modalRef.hide();
      this.loading = false;
      this.load = false;
      return;
    }
    if (temp == 0) {
      if (navigator.onLine) {
        this.tratamientoService.borrarTratamientoComprado(this.tratamientoAdarBaja.id, this.tratamientoAdarBaja.motivo).subscribe(data => {
          if (data.type == "OK") {
            this.toastr.success('Tratamiento borrado con exito');
            this.tratamientosComprados = [];
            this.itemsTratamientosComprados = [];
            this.motivo = null;
            this.actualizarSaldoCliente(this.clienteId.cliente.id);
            this.traerTratamientosComprados(this.clienteId.cliente.id);
            this.modalRef.hide()
          } else {
            console.log(data)
            this.loading = false;
            this.load = false;
            this.modalRef.hide()
            this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
            // mostrar error
          }
        })
      } else {
        this.loading = false;
        this.load = false;
        this.modalRef.hide()
        this.toastr.info("Sin Conexion a internet. Intente nuevamente");
      }
    }
  }

  // 1.3 Baja con sesiones activas
  //Baja de tratamiento con turnos(modal)
  openModalBorrarTratamientoTotal(template: TemplateRef<any>, id, sesiones, motivo) {
    this.tratamientoAdarBaja.id = id;
    this.tratamientoAdarBaja.sesiones = sesiones;
    this.tratamientoAdarBaja.motivo = motivo;
    this.modalRef = this.modalService.show(template, this.config);
  }

  recursivaBorrarTurnoTratamiento = (index, array) => new Promise((resolve, reject) => {
    if (index < array.length) {
      this.tempRazon = null;
      if (array[index].turnoId == null) {
        resolve(this.recursivaBorrarTurnoTratamiento(index + 1, array));
      } else {
        this.turno = { id: array[index].turnoId, canceladoRazon: array[index].canceladoRazon }
        //Turnos Renovados
        //     if(turno.canceladoRazon!=null){
        //         tempRazon="FALTO CON AVISO "+turno.canceladoRazon
        //     }
        // turno.canceladoRazon=tempRazon
        this.turnoService.cancelarTurnos(this.turno).subscribe(data => {
          if (data.type == "OK") {
            this.turno = {}
            resolve(this.recursivaBorrarTurnoTratamiento(index + 1, array));
          } else {
            console.log(data)
            this.loading = false;
            this.load = false;
            this.modalRef.hide()
            this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
            // mostrar error
          }
        })
      }
    } else {
      return resolve("OK");
    }
  })

  darDeBajaTratamientoTotal() {
    this.loading = true;
    this.load = true;
    this.tratamientosComprados = [];
    this.itemsTratamientosComprados = [];
    //Valido las sesiones del tratamiento
    if (navigator.onLine) {
      var a = this.recursivaBorrarTurnoTratamiento(0, this.tratamientoAdarBaja.sesiones);
      if (a != null) {
        this.tratamientoService.borrarTratamientoComprado(this.tratamientoAdarBaja.id, this.tratamientoAdarBaja.motivo).subscribe(data => {
          if (data.type == "OK") {
            this.toastr.success('Tratamiento borrado con exito');
            this.loading = false;
            this.load = false;
            this.motivo = null;
            this.modalRef.hide()
            this.actualizarSaldoCliente(this.clienteId.cliente.id);
            this.traerTratamientosComprados(this.clienteId.cliente.id);
          } else {
            console.log(data)
            this.load = false;
            this.loading = false;
            this.modalRef.hide()
            this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
            // mostrar error
          }
        })
      }
    } else {
      this.load = false;
      this.loading = false;
      this.modalRef.hide();
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  //1.4 Cerrar modales de tratamientos comprados
  cancelarBajaTratamiento() {
    this.modalRef.hide();
    this.tratamientoAdarBaja = { id: null, sesiones: null, motivo: null };
  }

  // 1.5 Comprar nuevo tratamientos
  cancelarCompraTratamiento() {
    this.modalRef.hide()
  }

  TransportarCompraTratamiento(event) {
    this.tratamientosPorComprar = event;
  }

  openModalComprarTratamiento(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  recursivaComprarTratamiento = (index, array) => new Promise((resolve, reject) => {
    if (index < array.length) {
      this.tratamientoService.comprarTratamiento(array[index].id, this.clienteId.cliente.id, array[index].sesiones, array[index].precioFinal).subscribe(data => {
        if (data.type == "OK") {
          resolve(this.recursivaComprarTratamiento(index + 1, array));
        } else {
          console.log(data)
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.traerTratamientosComprados(this.clienteId.cliente.id);
      this.actualizarSaldoCliente(this.clienteId.cliente.id);
      return resolve("OK");
    }
  });

  comprarTratamientos() {
    this.load = true;
    this.tratamientosComprados = [];
    this.itemsTratamientosComprados = [];
    this.loading = true;
    if (this.tratamientosPorComprar.length == 0) {
      this.toastr.error('Debe cargar al menos un tratamiento para continuar');
      this.load = false;
      return;
    }
    if (navigator.onLine) {
      var a = this.recursivaComprarTratamiento(0, this.tratamientosPorComprar);
      if (a != null) {
        this.toastr.success('Tratamientos comprado con exito');
        this.load = false;
        this.tratamientosPorComprar = [];
        this.cerrarTablaSesiones()
        this.modalRef.hide()
      }
    } else {
      this.load = false;
      this.modalRef.hide()
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  // ***************************************************************************************************
  //2-Funciones de Pago
  // 2.1 Getter List pagos
  TransportarPago(event) {
    this.listaPago = event
  }

  // 2.2 Guardar Pago
  createPago() {
    this.load = true;
    if (navigator.onLine) {
      this.contableService.generarPagoCliente(this.clienteId.cliente.id, this.listaPago).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Pago generado con éxito');
          this.load = false;
          this.actualizarSaldoCliente(this.clienteId.cliente.id);
          this.modalRef.hide()
        } else {
          this.load = false;
          this.modalRef.hide();
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    }
    else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  // 2.3 Modales Pago
  openModalPago(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }
  // ***************************************************************************************************
  //3-Sesiones
  // 3.1 Cargar sesiones del tratamiento seleccionado
  cargarSesiones(dataTratamiento, dataSesiones) {
    this.tablaSesiones = dataSesiones;
    this.habitaciones = dataTratamiento.habitaciones;
    this.tratamientoId = dataTratamiento.id
  }
  // 3.2 Cierro tabla de sesiones
  cerrarTablaSesiones() {
    this.tablaSesiones = [];
    this.habitaciones = [];
    this.tratamientoId = null;
  }
  // ***************************************************************************************************
  //4-Consultar turnos por fechas
  // 4.1 consultar turnos disponibles
  reservarListaDeTurnos() {
    var dias = 0;
    this.load = true;
    this.limpiarRecursivo();
    this.listaDeFechas = []
    for (var i = 0; i < this.selectedSesiones.length; i++) {
      this.Objetosrecursivo.temp = { desde: moment(this.fecha.desde).add(dias, 'days'), hasta: moment(this.fecha.hasta).add(dias, 'days'), }
      this.Objetosrecursivo.fecha = { desde: this.Objetosrecursivo.temp.desde._d, hasta: this.Objetosrecursivo.temp.hasta._d, }
      this.Objetosrecursivo.temp2 = { id: this.selectedSesiones[i].id, fecha: this.Objetosrecursivo.fecha }
      this.listaDeFechasSesiones.push(this.Objetosrecursivo.temp2)
      dias += 7;
    }
    // console.log(this.listaDeFechasSesiones)
    var a = this.recursivaConsultarFechas(0, this.listaDeFechasSesiones);
    if (a != null) {
      this.load = false;
      this.modalRef.hide()
    }
    // this.listaDeFechasSesiones.forEach(x => {
    //   this.turnoService.consultarDisponibilidad(this.tratamientoId, x.fecha).subscribe(data => {
    //     if (data.type == "OK") {
    //       if (data.content != undefined || data.content != null) {
    //         this.listaTurnosOcupados = data.content;
    //       }
    //       this.Objetosrecursivo.objeto = {
    //         disponibilidad: this.listaTurnosOcupados,
    //         fecha: x.fecha,
    //         id: x.id
    //       }
    //       this.Objetosrecursivo.listaFinal.push(this.Objetosrecursivo.objeto);
    //       this.listaDeFechas = this.Objetosrecursivo.listaFinal;
    //       this.load=false;
    //       this.modalRef.hide()
    //     }
    //   })
    // })
  }

  //4.2 Limipiar Consulta de turnos
  limpiarRecursivo() {
    this.Objetosrecursivo = { tempHora: [], fecha: {}, listaFinal: [], objeto: {}, temp: null, temp2: null }
    this.listaDeFechasSesiones = [];
    this.listaTurnosOcupados = [];
    this.listaDeFechas = []
  }

  // 4.3 abrir modal de consultas
  openModalConsultarFechas(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  // 4.4 abrir modal de consultas(renovar)
  openModalRenovarFechas(template: TemplateRef<any>, data) {
    this.listaTempTurnos = { disponibilidad: [], fecha: { desde: null, hasta: null }, id: null };
    this.listaTempTurnos = data;
    this.itemsTurnosOcupados = this.listaTempTurnos.disponibilidad.slice(this.page, 10);
    this.fechaActualizar = { desde: null, hasta: null };
    this.fechaActualizar.desde = this.listaTempTurnos.fecha.desde;
    this.fechaActualizar.hasta = this.listaTempTurnos.fecha.hasta;
    this.modalRef = this.modalService.show(template, this.config);
  }

  // 4.5 Actualizar turnos ocupados
  consultarTurno() {
    this.load = true;
    this.listaTempTurnos.disponibilidad = [];
    this.itemsTurnosOcupados = []
    this.turnoService.consultarDisponibilidad(this.tratamientoId, this.fechaActualizar).subscribe(data => {
      var temp = []
      if (data.type == "OK") {
        if (data.content.length == 0) {
          this.toastr.success("Turno Disponible");
          this.listaDeFechas.forEach(x => {
            if (this.listaTempTurnos.id == x.id) {
              x.id = this.listaTempTurnos.id
              x.fecha = this.fechaActualizar;
              x.disponibilidad = [];
            }
            temp.push(x)
            this.listaDeFechas = [];
            this.listaDeFechas = temp;
          })
          this.load = false;
          this.modalRef.hide()
        } else {
          this.listaTempTurnos.disponibilidad = data.content;
          this.itemsTurnosOcupados = this.listaTempTurnos.disponibilidad.slice(this.page, 10);
          this.toastr.info("Turno Ocupado");
          this.load = false;
        }

      }
    })
  }

  // 4.6 Recursiva de consultar turnos
  recursivaConsultarFechas = (index, array) => new Promise((resolve, reject) => {
    if (index < array.length) {
      this.turnoService.consultarDisponibilidad(this.tratamientoId, array[index].fecha).subscribe(data => {
        if (data.type == "OK") {
          if (data.content != undefined || data.content != null) {
            this.listaTurnosOcupados = data.content;
          }
          this.Objetosrecursivo.objeto = {
            disponibilidad: this.listaTurnosOcupados,
            fecha: array[index].fecha,
            id: array[index].id
          }
          this.Objetosrecursivo.listaFinal.push(this.Objetosrecursivo.objeto);
          this.listaDeFechas = this.Objetosrecursivo.listaFinal;
          resolve(this.recursivaConsultarFechas(index + 1, array));
        }
      })
    } else {
      return resolve("OK");
    }
  });
  // **************************************************************************************************
  // Generar turno
  TransportarUsuario(event: any) {
    this.profesionalId = event;
    this.usuarioService.getFull(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.usuario = data.content;
      } else {
        // mostrar error
      }
    })
  }
  
  openModalTerminarTurnos(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  generarTurno() {
    this.loadTurnoFinal = true;
    if (this.selectedHabitacion.length == 0) {
      this.toastr.error("Debe seleccionar una sala para continuar");
      this.loadTurnoFinal = false;
      this.modalRef.hide();
      return;
    }
    if (this.profesionalId == null) {
      this.toastr.error("Debe seleccionar un profesional a cargo para el turno");
      this.loadTurnoFinal = false;
      this.modalRef.hide();
      return;
    }
    var habitacionId;
    this.selectedHabitacion.forEach(x => {
      habitacionId = x.id;
    })
    if (navigator.onLine) {
      var count = 0;
      this.listaDeFechas.forEach(x => {
        this.turnoService.ocuparTurno(x.id, habitacionId, this.profesionalId, x.fecha).subscribe(data => {
          if (data.errorNumber == "1405" && data.type == "ERROR") {
            this.toastr.error("Sesión ya posee un turno activo..");
            this.loadTurnoFinal = false;
            this.modalRef.hide();
            return;
          }
          if (data.errorNumber == "1403" && data.type == "ERROR") {
            this.toastr.error(""+data.message);
            this.loadTurnoFinal = false;
            this.modalRef.hide();
            return;
          }
          if (data.message == "Turno guardado" && data.type == "OK") {
            count += 1
          }
          if (this.listaDeFechas.length == count) {
            this.loadTurnoFinal = false;
            this.modalRef.hide();
            this.toastr.success("Turno creado con exito");      
            this.router.navigate(['/panel/turnos/list']);
          }
        })
      })

    } else{
      this.loadTurnoFinal = false;
      this.modalRef.hide();
      this.toastr.error('Sin Conexion a internet. Intente nuevamente');
    }

  }

}
