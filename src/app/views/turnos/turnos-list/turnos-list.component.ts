import { Component, OnInit, TemplateRef } from '@angular/core';
import { TurnosService } from 'src/app/services/turnos.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Turnos } from 'src/app/models/turnos';
import { first, count } from "rxjs/operators";
import { environment } from 'src/environments/environment';
import { TypeaheadMatch, PageChangedEvent } from 'ngx-bootstrap';
import { SalasService } from 'src/app/services/salas.service';
import { TratamientosService } from 'src/app/services/tratamientos.service';
import { FichasService } from 'src/app/services/fichas.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import { UploadService } from 'src/app/services/upload.service';
import * as moment from 'moment';

@Component({
  selector: 'app-turnos-list',
  templateUrl: './turnos-list.component.html',
  styleUrls: ['./turnos-list.component.scss']
})
export class TurnosListComponent implements OnInit {

  constructor(private uploadService: UploadService, private fichasService: FichasService, private usuarioService: UsuarioService, private salasService: SalasService, private tratamientoService: TratamientosService, private turnoService: TurnosService, private toastr: ToastrService, private modalService: BsModalService) {
    this.setClickedRowRenovar = function (index) {
       if (this.selectedSesiones.indexOf(index) > -1) {
        this.selectedSesiones.splice(this.selectedSesiones.indexOf(index), 1);
      }
      else {
        this.selectedSesiones.push(index);
      }
    }
    
  }
  ngOnInit() {
    this.loadTurnos();
    this.loadsalas();
    this.traerHistoriasClinicas()
  }

  // Variables Generales
  listaDeTurnos: Array<Turnos> = []
  turnoId: any;
  user: any;
  salas = [];
  selectSala = null;
  setClickedRowRenovar: Function;
  itemsTurnosOcupados = [];
  usuarioObjeto = null;
  dias = null;
  url=`${environment.apiUrl}`

  // Booleanos
  loading = false;
  load = false;
  inicio = false;
  terminarTurnos = false;
  loadFechas = false;
  loadSesiones = true;

  //Renovar turnos
  selectedSesiones = [];
  tempMotivo = null;
  listaTempTurnos = { cliente: null, disponibilidad: [], fecha: null, tratamientoId: null, sesionId: null, habitacionId: null, profesionalId: null, canceladoRazon: null, turnoId: null, falta: false, habitaciones: null };
  listaDeFechas = [];
  Objetosrecursivo = { tempHora: [], fecha: {}, listaFinal: [], objeto: {}, temp: null, temp2: null }
  fechaActualizar = { desde: null, hasta: null };
  listaTurnosOcupados = [];
  tempLista = [];
  turno = null;
  tempRazon;

  //Terminar turnos
  tempavisos = null;
  sesionTemp = [];
  textAvisos = null;
  tablaSesiones = { cliente: null, diferencia: null, enabled: null, fechaCompleto: null, fechaCompra: null, id: null, motivo: null, precio: null, sesiones: [], tratamiento: null };
  fichaHistoriaClinica = [];
  listaDeFichasClinicas = [];
  respuestaObservacion = null;
  loadTablaHistoriaClinica = false;
  tempRespuesta= {pregunta: undefined, respuesta: undefined};
  imageList = [];
  sesionId = null;
  imageTemp=null;

  //Turnos completados
  imagenesClinica = [];
  detallesPaciente = [];
  itemsPerSlide = 5;
  singleSlideOffset = true;
  noWrap = true;

  //Historia Clinica
  historiaClinica = {
    fecha: new Date(),
    fotos: [],
    items: []
  }

  //Filtro de busqueda
  filtroPagination = {
    fecha: new Date(),
    profesionalId: null,
    clienteId: null,
    completado: false,
    habitacionId: null
  }

  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0

  onOptionsSelected(value){
    if(value=="true"){
      this.filtroPagination.completado=true;
    }else{
      this.filtroPagination.completado=false;
    }
    this.buscador()
}

  // Bindings cliente,usuario,habitaciones
  TransportarCliente(event: any) {
    this.filtroPagination.clienteId = event;
    this.buscador()
  }
  TransportarUsuario(event: any) {
    this.filtroPagination.profesionalId = event;
    this.buscador()
  }
  TransportarUsuarioObjeto(event: any) {
    this.listaTempTurnos.profesionalId = event
    this.loadUser(event)
  }
  onSelect(event: TypeaheadMatch): void {
    this.filtroPagination.habitacionId = event.item.id;
    this.buscador()
  }
  onValueChange(value: any) {
    if (value != null) {
      this.filtroPagination.fecha = value;
      this.buscador()
    }
  }
  // ****************************************************************************************************
  //Traigo el user
  loadUser(data) {
    this.usuarioService.getFull(data).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.usuarioObjeto = data.content;
      } else {
        // mostrar error
      }
    })
  }

  //Traigo las salas
  loadsalas() {
    this.salasService.getSalas(0, 100).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.salas = data.content.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;
  valueWidth = true;

  //  1.1 Lista de turnos
  loadTurnos() {
    this.loading = true;
    this.turnoService.search(this.page, this.pageSize, this.filtroPagination).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.listaDeTurnos = data.content.content;
        this.loading = false;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.2Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.listaDeTurnos = [];
    this.loadTurnos()
  }
  pageChangedTurnosOcupados(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.itemsTurnosOcupados = this.listaTempTurnos.disponibilidad.slice(startItem, endItem);
  }

  //1.3 Refresh lista
  refresh() {
    this.listaDeTurnos = [];
    this.selectedSesiones = [];
    //Paginacion
    this.filtroPagination = {
      fecha: new Date(),
      profesionalId: null,
      clienteId: null,
      completado: false,
      habitacionId: null
    }
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadTurnos();
    this.selectSala = null;
  }

  //1.4 Busqueda Personalizada
  buscador() {
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadTurnos();
  }

  //1.5 Limpiar campos de filtro
  resetFiltro() {
    this.listaDeTurnos = [];
    this.selectedSesiones = [];
    //Paginacion
    this.filtroPagination = {
      fecha: null,
      profesionalId: null,
      clienteId: null,
      completado: false,
      habitacionId: null
    }
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadTurnos();
    this.selectSala = null;
  }

  openModalDelete(template: TemplateRef<any>, data) {
    this.turnoId = null;
    this.turnoId = data
    console.log(this.turnoId)
    this.modalRef = this.modalService.show(template, this.config);
  }
  cancelarBorradoTurno() {
    this.turnoId = { falta: "" };
    this.modalRef.hide();
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //**************************RENOVAR TURNOS*********************************************************************
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //4.0 Renovar turnos(cargar lista)
  renovarTurnos() {
    this.loadSesiones = true;
    var temp = this.selectedSesiones;
    var tempSala;
    var count = 0;
    this.turnoId = null;
    this.selectedSesiones = [];
    temp.forEach(x => {
      this.tratamientoService.getFullTratamiento(x.tratamientoTransient.id).subscribe(data => {
        tempSala = data.content;
        var objeto = {
          cliente: x.clienteTransient,
          disponibilidad: [],
          fecha: { desde: x.desde, hasta: x.hasta },
          tratamientoId: x.tratamientoTransient.id,
          sesionId: x.sesion.id,
          habitacionId: x.habitacion.id,
          profesionalId: x.profesional.id,
          canceladoRazon: null,
          turnoId: x.id,
          falta: "false",
          habitaciones: tempSala.habitaciones
        }
        this.selectedSesiones.push(objeto)
        count += 1;
      })
    });
    if (count == this.selectedSesiones.length) {
      this.loadSesiones = false;
    }
  }
  //4.1 Modal de motivos de renovacion
  openModalMotivo(template: TemplateRef<any>, data) {
    this.tempMotivo = data;
    this.modalRef = this.modalService.show(template, this.config);
  }
  cancelarMotivo() {
    this.tempMotivo.canceladoRazon = "";
    this.modalRef.hide()
  }

  //4.2 Mostrar y ocultar forms de renovacion
  cancelarRenovar() {
    this.selectedSesiones = []
    this.inicio = false;
  }
  abrirRenovacion() {
    this.inicio = true;
    this.renovarTurnos()
  }

  // 4.3 Actualizar y cargar motivo de cancelacion
  actualizarMotivo() {
    var temp = this.selectedSesiones;
    this.selectedSesiones = [];
    temp.forEach(x => {
      if (x.id == this.tempMotivo.id && this.tempMotivo.cliente == x.cliente && x.fecha == this.tempMotivo.fecha) {
        x.canceladoRazon = this.tempMotivo.canceladoRazon
      }
      this.selectedSesiones.push(x)
    })
    this.modalRef.hide()
  }
  // **************************************************************************************
  // RENOVAR FECHAS
  // 4.4 abrir modal de consultas(renovar)
  openModalRenovar(template: TemplateRef<any>, data) {
    this.listaTempTurnos = { cliente: null, disponibilidad: [], fecha: null, tratamientoId: null, sesionId: null, habitacionId: null, profesionalId: null, canceladoRazon: null, turnoId: null, falta: false, habitaciones: null };
    this.listaTempTurnos = data;
    this.loadUser(this.listaTempTurnos.profesionalId)
    this.itemsTurnosOcupados = this.listaTempTurnos.disponibilidad.slice(0, 10);
    this.fechaActualizar = { desde: null, hasta: null };
    this.fechaActualizar.desde = new Date(this.listaTempTurnos.fecha.desde);
    this.fechaActualizar.hasta = new Date(this.listaTempTurnos.fecha.hasta);
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalRenovarFechas(template: TemplateRef<any>, data) {
    this.listaTempTurnos = { cliente: null, disponibilidad: [], fecha: null, tratamientoId: null, sesionId: null, habitacionId: null, profesionalId: null, canceladoRazon: null, turnoId: null, falta: false, habitaciones: null };
    this.listaTempTurnos = data;
    console.log(data)
    // this.loadUser(this.listaTempTurnos.profesionalId)
    // this.itemsTurnosOcupados = this.listaTempTurnos.disponibilidad.slice(0, 10);
    // this.fechaActualizar = { desde: null, hasta: null };
    // this.fechaActualizar.desde = this.listaTempTurnos.fecha.desde;
    // this.fechaActualizar.hasta = this.listaTempTurnos.fecha.hasta;
    this.modalRef = this.modalService.show(template, this.config);
  }
  // 4.5 Actualizar turnos ocupados
  consultarTurno() {
    this.load = true;
    this.listaTempTurnos.disponibilidad = [];
    this.itemsTurnosOcupados = []
    this.turnoService.consultarDisponibilidad(this.listaTempTurnos.tratamientoId, this.fechaActualizar).subscribe(data => {
      var temp = []
      if (data.type == "OK") {
        if (data.content.length == 0) {
          this.toastr.success("Turno Disponible");
          this.selectedSesiones.forEach(x => {
            if (this.listaTempTurnos.sesionId == x.sesionId) {
              x.fecha = this.fechaActualizar;
              x.tratamientoId = this.listaTempTurnos.tratamientoId;
              x.disponibilidad = [];
              x.sesionId = this.listaTempTurnos.sesionId;
              x.habitacionId = this.listaTempTurnos.habitacionId;
              x.profesionalId = this.listaTempTurnos.profesionalId,
                x.cliente = this.listaTempTurnos.cliente,
                x.canceladoRazon = this.listaTempTurnos.canceladoRazon
              x.turnoId = this.listaTempTurnos.turnoId
            }
          })
          this.load = false;
          this.modalRef.hide()
        } else {
          this.selectedSesiones.forEach(x => {
            if (this.listaTempTurnos.sesionId == x.sesionId) {
              x.fecha = this.fechaActualizar;
              x.tratamientoId = this.listaTempTurnos.tratamientoId;
              x.disponibilidad = data.content;
              x.sesionId = this.listaTempTurnos.sesionId;
              x.habitacionId = this.listaTempTurnos.habitacionId;
              x.profesionalId = this.listaTempTurnos.profesionalId,
                x.cliente = this.listaTempTurnos.cliente,
                x.canceladoRazon = this.listaTempTurnos.canceladoRazon
              x.turnoId = this.listaTempTurnos.turnoId
            }
          })
          this.listaTempTurnos.disponibilidad = data.content;
          this.itemsTurnosOcupados = this.listaTempTurnos.disponibilidad.slice(0, 10);
          this.toastr.info("Turno Ocupado");
          this.load = false;
        }

      }
    })
  }

  // 4.6 Borrar turnos
  eliminarItem() {
    for (let i = 0; i < this.selectedSesiones.length; ++i) {
      if (this.selectedSesiones[i] === this.turnoId) {
        this.selectedSesiones.splice(i, 1);
        this.modalRef.hide()
      }
    }
  }
  eliminarItemTurnos(data) {
    for (let i = 0; i < this.selectedSesiones.length; ++i) {
      if (this.selectedSesiones[i] === data) {
        this.selectedSesiones.splice(i, 1);
        this.load = false;
        this.modalRef.hide()
      }
    }
  }
  deleteTurno() {
    if (navigator.onLine) {
      this.load = true;
      var turno = null;
      var tempRazon = null;
      turno = { id: this.turnoId.turnoId, canceladoRazon: this.turnoId.canceladoRazon }
      if (this.turnoId.falta != "false") {
        // Turnos eliminados
        console.log("entro");
        var estado = true;
        this.historiaClinica = {
          fecha: new Date(),
          fotos: [],
          items: [{ pregunta: "Observaciones:", respuesta: "Sesión terminada:FALTO SIN AVISO" }]
        }
        this.tratamientoService.marcarSesionCompleta(this.turnoId.sesionId, estado, this.historiaClinica).subscribe(data => {
          if (data.type == "OK") {
            this.historiaClinica = {
              fecha: new Date(),
              fotos: [],
              items: []
            }
            turno = {};
            this.load = false;
            this.eliminarItem()
            this.refresh();
            this.toastr.success("Turno terminado y computado con exito");
          }
          else {
            console.log(data)
          }

        })
      }
      if (this.turnoId.falta == "false") {
        //Turnos Renovados
        if (turno.canceladoRazon != null) {
          tempRazon = "FALTO CON AVISO " + turno.canceladoRazon
        } else {
          tempRazon = "FALTO CON AVISO "
        }
        turno.canceladoRazon = tempRazon
        this.turnoService.cancelarTurnos(turno).subscribe(data => {
          if (data.type == "OK") {
            this.toastr.success("Turno borrado con exito.(Sesión sigue activa)");
            turno = {};
            this.load = false;
            this.eliminarItem()
            this.refresh()
          } else {
            console.log(data)
          }

        })
      }
    } else {
      this.toastr.info('Sin Conexion a internet. Intente nuevamente');
    }
  }

  //4.7 Modal de dias para renovar
  openModalDia(template: TemplateRef<any>, data) {
    if (this.selectedSesiones.length == 0) {
      this.toastr.error('Lista vacia de turnos. Debe haber al menos un turno para continuar');
      return;
    } else {
      this.tempMotivo = data;
      this.modalRef = this.modalService.show(template, this.config);
    }
  }
  cancelarRenovarDia() {
    this.dias = null;
    this.modalRef.hide()
  }

  //4.8 Actualizar automaticamente las fechas y consultar disponibilidad a dias variables
  renovarLista(data) {
    this.loadFechas = true;
    this.tempLista = []
    var Objetosrecursivo = { temp: { desde: null, hasta: null }, fecha: {}, temp2: {}, listaNueva: [] };
    for (var i = 0; i < this.selectedSesiones.length; i++) {
      Objetosrecursivo.temp = { desde: moment(this.selectedSesiones[i].fecha.desde).add(data, 'days'), hasta: moment(this.selectedSesiones[i].fecha.hasta).add(data, 'days'), }
      Objetosrecursivo.fecha = { desde: Objetosrecursivo.temp.desde._d, hasta: Objetosrecursivo.temp.hasta._d, }
      Objetosrecursivo.temp2 = {
        sesionId: this.selectedSesiones[i].sesionId,
        fecha: Objetosrecursivo.fecha,
        tratamientoId: this.selectedSesiones[i].tratamientoId,
        habitacionId: this.selectedSesiones[i].habitacionId,
        profesionalId: this.selectedSesiones[i].profesionalId,
        disponibilidad: this.selectedSesiones[i].disponibilidad,
        cliente: this.selectedSesiones[i].cliente,
        canceladoRazon: this.selectedSesiones[i].canceladoRazon,
        turnoId: this.selectedSesiones[i].turnoId
      }
      Objetosrecursivo.listaNueva.push(Objetosrecursivo.temp2)
      data + 7;
    }
    Objetosrecursivo.listaNueva.forEach(y => {
      this.turnoService.consultarDisponibilidad(y.tratamientoId, y.fecha).subscribe(data => {
        this.tempLista = this.selectedSesiones
        this.selectedSesiones = []
        this.tempLista.forEach(x => {
          if (y.sesionId == x.sesionId) {
            x.fecha = y.fecha;
            x.tratamientoId = y.tratamientoId;
            if (data.content.length > 0) {
              x.disponibilidad = data.content
            } else {
              x.disponibilidad = [];
            }
            x.sesionId = y.sesionId;
            x.habitacionId = y.habitacionId;
            x.profesionalId = y.profesionalId;
            x.canceladoRazon = y.canceladoRazon;
            x.turnoId = y.turnoId
          }
          this.selectedSesiones.push(x)
        })
      })
    })
    this.loadFechas = false;
    this.cancelarRenovarDia()
  }

  //4.9 Recursiva de renovacion de turnos
  recursivaBorrarTurno = (index, array) => new Promise((resolve, reject) => {
    if (index < array.length) {
      this.turno = { id: array[index].turnoId, canceladoRazon: array[index].canceladoRazon }
      if (array[index].falta != "false") {
        // Turnos eliminados
        console.log("entro")
        var estado = true
        this.historiaClinica = {
          fecha: new Date(),
          fotos: [],
          items: [{ pregunta: "Observaciones:", respuesta: "Sesión terminada:FALTO SIN AVISO" }]
        }
        this.tratamientoService.marcarSesionCompleta(array[index].sesionId, estado, this.historiaClinica).subscribe(data => {
          if (data.type == "OK") {
            this.historiaClinica = {
              fecha: new Date(),
              fotos: [],
              items: []
            }
            this.turno = {}
            resolve(this.recursivaBorrarTurno(index + 1, array));
          }
        })
      }
      if (array[index].falta == "false") {
        //Turnos Renovados
        if (this.turno.canceladoRazon != null) {
          this.tempRazon = "FALTO CON AVISO " + this.turno.canceladoRazon
        } else {
          this.tempRazon = "FALTO CON AVISO "
        }
        this.turno.canceladoRazon = this.tempRazon;
        this.turnoService.cancelarTurnos(this.turno).subscribe(data => {
          if (data.type == "OK") {
            this.turno = {};
            resolve(this.recursivaBorrarTurno(index + 1, array))
          }
        })
      }
    } else {
      return resolve("OK");
    }
  });

  //  4.10 Guardar datos renovados
  terminarRenovacion() {
    if (navigator.onLine) {
      this.load = true;
      var temTurno = null;
      this.recursivaBorrarTurno(0, this.selectedSesiones).then(temp => {
        if (temp == "OK") {
          var countasd = 0;
          for (var i = 0; i < this.selectedSesiones.length; i++) {
            temTurno = this.selectedSesiones[i];

            this.turnoService.ocuparTurno(this.selectedSesiones[i].sesionId, this.selectedSesiones[i].habitacionId, this.selectedSesiones[i].profesionalId, this.selectedSesiones[i].fecha).subscribe(data => {
              if (data.errorNumber == "1405" && data.type == "ERROR") {
                this.eliminarItemTurnos(temTurno);
                countasd += 1;
              }

              if (data.errorNumber == "1403" && data.type == "ERROR") {
                this.toastr.error('Fue dado de baja porque la habitacion seleccionada ya se encuentra ocupada en el horario seleccionado.Vaya a crear nuevamente un turno y consulte nuevamente', 'El turno del cliente ' + temTurno.cliente.nombre + ' ' + temTurno.cliente.apellido, {
                  timeOut: 10000
                });
                countasd += 1;
              }

              if (data.message == "Turno guardado" && data.type == "OK") {
                countasd += 1;
              }

              if (this.selectedSesiones.length == countasd) {
                this.load = false;
                this.cancelarRenovar();
                this.modalRef.hide();
                this.toastr.success('Turnos renovados con exito');
                this.refresh();
              }
            })
          }
        }
      });
    } else {
      this.load = false
      this.toastr.info('Sin Conexion a internet. Intente nuevamente');
    }
  }

  openModalRenovaciones(template: TemplateRef<any>) {
    if (this.selectedSesiones.length == 0) {
      this.toastr.error('Lista vacia de turnos. Debe haber al menos un turno para continuar');
      return;
    } else {
      this.modalRef = this.modalService.show(template, this.config);
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //*************TERMINAR TURNOS/////////////////////////////****************************************************
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // 5.0 abrir pantalla
  abrirTerminarTurno(sesionData, clienteData, tratamientoData) {
    this.avisosDeSesiones(clienteData, sesionData.id);

    //Precarga de historia clinica
    if (sesionData.historiaClinica != undefined) {
      this.sesionTemp = sesionData.historiaClinica.items;
    }
    this.sesionId = sesionData.id;
    this.autoObservacion();
    this.respuestaObservacion = tratamientoData;
    this.terminarTurnos = true;
  }
  cancelarTerminarTurno() {
    this.sesionId = null;
    this.tempavisos = null;
    this.sesionTemp = [];
    this.textAvisos = null;
    this.tablaSesiones = { cliente: null, diferencia: null, enabled: null, fechaCompleto: null, fechaCompra: null, id: null, motivo: null, precio: null, sesiones: [], tratamiento: null };
    this.fichaHistoriaClinica = [];
    this.terminarTurnos = false;
    this.loadTablaHistoriaClinica = false;
    this.tempRespuesta= {pregunta: undefined, respuesta: undefined};
  }

  //5.1 Calcular aviso de sesiones 
  avisosDeSesiones(param1, param2) {
    var tempLista2 = { cliente: null, diferencia: null, enabled: null, fechaCompleto: null, fechaCompra: null, id: null, motivo: null, precio: null, sesiones: [], tratamiento: null };
    this.tempavisos = [];

    this.tratamientoService.tratamientosComprados(param1).pipe(first()).subscribe(data => {
      if (data.type == "OK") {

        this.tempavisos = data.content;

        this.tempavisos.forEach(x => {
          if (x.enabled == true) {
            x.sesiones.forEach(y => {
              if (y.id == param2) {
                tempLista2 = x
              }
            });
          }
        });
        this.tablaSesiones = tempLista2
        // var pos = this.tablaSesiones.sesiones.map(function (e) { return e.id; }).indexOf(param2);
        // console.log(pos)
        var countTerminado = 0;
        var countConTurno = 0;
        var countSinTurno = 0;
        this.tablaSesiones.sesiones.forEach(u => {
          if (u.enabled == true && u.completa == false && u.turnoId != null) {
            countConTurno += 1;
          }
          if (u.enabled == false && u.completa == true) {
            countTerminado += 1;
          }
          if (u.enable == false && u.completa == false && u.turnoId == null) {
            countSinTurno += 1;
          }
        })
        if (countConTurno == 1 || countSinTurno == 1) {
          this.toastr.info('Ultimo turno del tratamiento comprado', 'INFORME DE TURNOS', {
            timeOut: 10000
          });
        }
      }
    })
  }
  // 5.2 Historias Clinicas
  traerHistoriasClinicas() {
    var temp = "historiaClinica";
    this.fichasService.getTipoDePlantilla(temp).subscribe(data => {
      this.listaDeFichasClinicas = data.content
    })
  }

  // 5.3 Seleccion de ficha
  cargarPreguntas(x) {
    var temp = {}
    this.loadTablaHistoriaClinica = true;
    this.fichasService.getPlantilla(x).subscribe(data => {
      data.content.items.forEach(a => {
        if (a.pregunta == "Observaciones:") {
          temp = { pregunta: a.pregunta, respuesta: this.respuestaObservacion }
          this.fichaHistoriaClinica.push(temp)
          temp = {}
        } else {
          temp = { pregunta: a.pregunta, respuesta: null }
          this.fichaHistoriaClinica.push(temp)
          temp = {}
        }
        this.fichaHistoriaClinica = this.eliminarObjetosDuplicados(this.fichaHistoriaClinica, 'pregunta')
      });

      //Cargo lo q estaba guardado
      this.sesionTemp.forEach(x => {
        temp = { pregunta: x.pregunta, respuesta: x.respuesta }
        this.fichaHistoriaClinica.push(temp)
        temp = {}
      })
      this.sesionTemp = [];
      this.loadTablaHistoriaClinica = false;
    })
  }


  //5.4 Carga automatica de las observaciones
  autoObservacion() {
    this.listaDeFichasClinicas.forEach(x => {
      if (x.nombre == "Observación") {
        this.cargarPreguntas(x.id)
      }
    })
  }

  //5.5 Eliminar duplicados de array
  eliminarObjetosDuplicados(arr, prop) {
    var nuevoArray = [];
    var lookup = {};
    for (var i in arr) {
      lookup[arr[i][prop]] = arr[i];
    }
    for (i in lookup) {
      nuevoArray.push(lookup[i]);
    }
    return nuevoArray;
  }

  //5.6 Modal de respuestas de historias clinicas
  openModalRespuestas(template: TemplateRef<any>, data) {
    this.tempRespuesta = data;
    this.modalRef = this.modalService.show(template, this.config);
  }
  cancelarRespuesta() {
    this.tempRespuesta= {pregunta: undefined, respuesta: undefined};
    this.modalRef.hide()
  }

  // 5.7 Actualizar y cargar respuesta
  actualizarRespuesta(data) {
    var temp = this.fichaHistoriaClinica;
    this.fichaHistoriaClinica = [];
    this.loadTablaHistoriaClinica = true;
    temp.forEach(x => {
      if (x.pregunta == data.pregunta) {
        x.respuesta = data.respuesta
      }
      this.fichaHistoriaClinica.push(x);
      this.loadTablaHistoriaClinica = false;
    })
    this.modalRef.hide()
  }

  // 5.8 IMAGENES

  // 5.8.1 Modal de  imagenes
  openModalBorrarImage(template:TemplateRef<any>,data){
    this.imageTemp=null;
    this.imageTemp=data;
    this.modalRef = this.modalService.show(template, this.config);
  }
  openModalImg(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  // 5.8.2 Binding imagenes
  TransportarImages(event: any) {
    this.load=true;
    this.imageList = []
    this.imageList = event;
    if(this.imageList.length>0){
      this.load=false;
    }
  }

  // 5.8.3 Eliminar imagen antes de subir
  deleteImage(){
    this.load=true;
    for (let i = 0; i < this.imageList.length; ++i) {
      if (this.imageList[i] === this.imageTemp) {
        this.imageList.splice(i, 1);
        this.modalRef.hide();
        this.load=false;
      }
    }
  }

  //5.8.4 Uploads Imagenes
  recursivaImages = (index, array) => new Promise((resolve, reject) => {
    if (index < array.length) {
      this.uploadService.histociaClinicas(array[index].compressedImage.imageDataUrl).subscribe(data => {
        if (data.type == "OK") {
          this.historiaClinica.fotos.push(data.content);
          resolve(this.recursivaImages(index + 1, array));
        }

      })
    } else {
      return resolve("OK");
    }
  });

  //5.9 Terminar y guardar turno como completo
  terminarTurno() {
    if(this.fichaHistoriaClinica.length==0){
      this.toastr.error('Debe tener al menos una observación del turno','ERROR EN HISTORIA CLINICA');
      return;
    }
   this.historiaClinica.items = this.fichaHistoriaClinica;
    this.load = true;
    if (navigator.onLine) {
      var estado = true;
      if (this.imageList.length == 0) {
        this.tratamientoService.marcarSesionCompleta(this.sesionId, estado, this.historiaClinica).subscribe(data => {
           if (data.type == "OK") {
            this.toastr.success('Sesión terminado con éxito');
            this.load = false;
            this.refresh();
            this.cancelarTerminarTurno();
          } else {
            this.load = false;
            this.toastr.error('' + data.message)
          }
        })
      } else {
        this.recursivaImages(0, this.imageList).then(data => {
          if (data == "OK") {
            this.tratamientoService.marcarSesionCompleta(this.sesionId, estado, this.historiaClinica).subscribe(data => {
              if (data.type == "OK") {
                this.toastr.success('Sesión terminado con éxito(Imagenes guardadas)');
                this.load = false;
                this.refresh();
                this.cancelarTerminarTurno();
              } else {
                this.load = false;
                this.toastr.error('' + data.message)
              }
            })
          }
        })
      }
    } else {
      this.load = false;
      this.toastr.error('Sin Conexion a internet. Intente nuevamente')
    }
  }
// *****************************************************************************************************************
  // TURNOS TERMINADOS
// *******************************************************************************************************************

  openModalClinica(template: TemplateRef<any>, data) {
    this.imagenesClinica = [];
    this.detallesPaciente = [];
    if(data.fotos){
      this.imagenesClinica = data.fotos;
    }
    this.detallesPaciente = data.items
    this.modalRef = this.modalService.show(template, this.config);
  }

  //Retornar Observacion por sesion
  observaciones(data) {
    var total=null;
    var total2="Sin observaciones"
    if(data!=undefined){
      data.forEach(x => {
        if (x.pregunta == "Observaciones:") {
          total = x.respuesta;
        }
      });
      if(total==null){
        return total2;
      }else{
        return total;
      }
    }else{
      return total2;
    }
  };
}
