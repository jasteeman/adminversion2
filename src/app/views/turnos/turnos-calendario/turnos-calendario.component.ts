// Calendario
import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { isSameDay, isSameMonth } from 'date-fns';
import { Observable } from 'rxjs';
import { CalendarEvent, CalendarView } from 'angular-calendar';
import { TurnosService } from 'src/app/services/turnos.service';
import { SalasService } from 'src/app/services/salas.service';
import { TypeaheadMatch, BsModalRef } from 'ngx-bootstrap';
import { UsuarioService } from 'src/app/services/usuario.service';
import { first, map } from 'rxjs/operators';
import * as moment from 'moment';
const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  }
};

@Component({
  selector: 'app-turnos-calendario',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './turnos-calendario.component.html',
  styleUrls: ['./turnos-calendario.component.scss']
})
export class TurnosCalendarioComponent implements OnInit {

  constructor(private turnoService: TurnosService, private salasService: SalasService, private usuarioService: UsuarioService) {
  }

  ngOnInit(): void {
    this.loadTurnos();
    this.loadsalas();
  }
  
  filtroPagination = {
    fecha: new Date(),
    profesionalId: null,
    clienteId: null,
    completado: false,
    habitacionId: null
  }

  //Paginacion
  items = [];
  page = 0;
  pageSize = 500;
  totalElements = 0;
  totalPages = 0
  salas = []

  // Calendario
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  events$: Observable<any>;
  activeDayIsOpen: boolean = false;
  modalRef: BsModalRef;
  valueWidth = true;
  selectSala = null;

  // Bindings cliente,usuario,habitaciones
  TransportarCliente(event: any) {
    this.filtroPagination.clienteId = event;
    this.buscador()
  }
  TransportarUsuario(event: any) {
    this.filtroPagination.profesionalId = event;
    this.buscador()
  }

  onSelect(event: TypeaheadMatch): void {
    this.filtroPagination.habitacionId = event.item.id;
    this.buscador()
  }
  onValueChange(value: any) {
    if (value != null) {
      this.filtroPagination.fecha = value;
      this.buscador()
    }
  }
  // ****************************************************************************************************

  //Traigo las salas
  loadsalas() {
    this.salasService.getSalas(0, 20).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.salas = data.content.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
      }
    });
  }
  //  Modal
  config = {
    ignoreBackdropClick: true
  };

  //  1.1 Lista de turnos
  loadTurnos() {
    this.events$ = this.turnoService.search(this.page, this.pageSize, this.filtroPagination).pipe(
      map((results) => {
        var list=[]
        results.content.content.forEach(u => {
          var inicio = moment(u.desde).format("YYYY-MM-DD HH:mm:ss");
          var final = moment(u.hasta).format("YYYY-MM-DD HH:mm:ss");
          if(this.filtroPagination.completado==false){
            var temp= {
              title: '' + u.clienteTransient.nombre + ',' + u.clienteTransient.apellido + ':' + u.tratamientoTransient.nombre + '(' + u.habitacion.nombre + ').HORA(Desde:' + inicio + '-Hasta:' + final + ')',
              start: new Date(u.desde),
              end: new Date(u.hasta),
              color: colors.red,
              resizable: {
                beforeStart: true,
                afterEnd: true
              }
            };
          }else{
            var temp= {
              title: '' + u.clienteTransient.nombre + ',' + u.clienteTransient.apellido + ':' + u.tratamientoTransient.nombre + '(' + u.habitacion.nombre + ').HORA(Desde:' + inicio + '-Hasta:' + final + ')',
              start: new Date(u.desde),
              end: new Date(u.hasta),
              color: colors.blue,
              resizable: {
                beforeStart: true,
                afterEnd: true
              }
            };
          }

          list.push(temp);
        });
        return list;
      })
    )
  }

  //1.3 Refresh lista
  refresh() {
    //Paginacion
    this.resetFiltro();
    this.pageSize = 500;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadTurnos();
  }

  //1.4 Busqueda Personalizada
  buscador() {
    this.page = 0;
    this.pageSize = 500;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadTurnos();
  }

  //1.5 Limpiar campos de filtro
  resetFiltro() {
    this.filtroPagination = {
      fecha: null,
      profesionalId: null,
      clienteId: null,
      completado: false,
      habitacionId: null
    }
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventClicked(event: CalendarEvent<any>): void {
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }
}
