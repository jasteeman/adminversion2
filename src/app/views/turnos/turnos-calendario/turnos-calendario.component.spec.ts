import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnosCalendarioComponent } from './turnos-calendario.component';

describe('TurnosCalendarioComponent', () => {
  let component: TurnosCalendarioComponent;
  let fixture: ComponentFixture<TurnosCalendarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnosCalendarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnosCalendarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
