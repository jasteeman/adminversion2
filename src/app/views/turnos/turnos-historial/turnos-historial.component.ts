import { Component, OnInit, TemplateRef } from '@angular/core';
import { TurnosService } from 'src/app/services/turnos.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Turnos } from 'src/app/models/turnos';
import { first, count } from "rxjs/operators";
import { environment } from 'src/environments/environment';
import { TypeaheadMatch, PageChangedEvent } from 'ngx-bootstrap';
import { SalasService } from 'src/app/services/salas.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-turnos-historial',
  templateUrl: './turnos-historial.component.html',
  styleUrls: ['./turnos-historial.component.scss']
})
export class TurnosHistorialComponent implements OnInit {

  constructor(private usuarioService: UsuarioService, private salasService: SalasService, private turnoService: TurnosService, private toastr: ToastrService, private modalService: BsModalService) {
    this.setClickedRowRenovar = function (index) {
      if (this.selectedSesiones.indexOf(index) > -1) {
        this.selectedSesiones.splice(this.selectedSesiones.indexOf(index), 1);
      }
      else {
        this.selectedSesiones.push(index);
      }
    }
  }
  ngOnInit() {
    this.loadTurnos();
    this.loadsalas();
  }

  // Variables Generales
  listaDeTurnos: Array<Turnos> = []
  turnoId: any;
  user: any;
  salas = [];
  selectSala = null;
  setClickedRowRenovar: Function;
  itemsTurnosOcupados = [];
  usuarioObjeto = null;
  dias = null;
  url=`${environment.apiUrl}`

  // Booleanos
  loading = false;
  load = false;
  inicio = false;
  terminarTurnos = false;
  loadFechas = false;
  loadSesiones = true;

  //Renovar turnos
  selectedSesiones = [];
  tempMotivo = null;
  listaTempTurnos = { cliente: null, disponibilidad: [], fecha: null, tratamientoId: null, sesionId: null, habitacionId: null, profesionalId: null, canceladoRazon: null, turnoId: null, falta: false, habitaciones: null };
  listaDeFechas = [];
  Objetosrecursivo = { tempHora: [], fecha: {}, listaFinal: [], objeto: {}, temp: null, temp2: null }
  fechaActualizar = { desde: null, hasta: null };
  listaTurnosOcupados = [];
  tempLista = [];
  turno = null;
  tempRazon;

  //Terminar turnos
  tempavisos = null;
  sesionTemp = [];
  textAvisos = null;
  tablaSesiones = { cliente: null, diferencia: null, enabled: null, fechaCompleto: null, fechaCompra: null, id: null, motivo: null, precio: null, sesiones: [], tratamiento: null };
  fichaHistoriaClinica = [];
  listaDeFichasClinicas = [];
  respuestaObservacion = null;
  loadTablaHistoriaClinica = false;
  tempRespuesta = "";
  imageList = [];
  sesionId = null;
  imageTemp=null;

  //Turnos completados
  imagenesClinica = [];
  detallesPaciente = [];
  itemsPerSlide = 5;
  singleSlideOffset = true;
  noWrap = true;

  //Historia Clinica
  historiaClinica = {
    fecha: new Date(),
    fotos: [],
    items: []
  }

  //Filtro de busqueda
  filtroPagination = {
    fecha: new Date(),
    profesionalId: null,
    findAll:true,
    clienteId: null,
    completado: false,
    habitacionId: null
  }

  //Paginacion
  items = [];
  page = 0;
  pageSize = 20;
  totalElements = 0;
  totalPages = 0

  // Bindings cliente,usuario,habitaciones
  TransportarCliente(event: any) {
    this.filtroPagination.clienteId = event;
    this.buscador()
  }
  TransportarUsuario(event: any) {
    this.filtroPagination.profesionalId = event;
    this.buscador()
  }
  TransportarUsuarioObjeto(event: any) {
    this.listaTempTurnos.profesionalId = event
    this.loadUser(event)
  }
  onSelect(event: TypeaheadMatch): void {
    this.filtroPagination.habitacionId = event.item.id;
    this.buscador()
  }
  onValueChange(value: any) {
    if (value != null) {
      this.filtroPagination.fecha = value;
      this.buscador()
    }
  }
  // ****************************************************************************************************
  //Traigo el user
  loadUser(data) {
    this.usuarioService.getFull(data).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.usuarioObjeto = data.content;
      } else {
        // mostrar error
      }
    })
  }

  //Traigo las salas
  loadsalas() {
    this.salasService.getSalas(0, 100).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.salas = data.content.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //  Modal
  config = {
    ignoreBackdropClick: true
  };
  modalRef: BsModalRef;
  valueWidth = true;

  //  1.1 Lista de turnos
  loadTurnos() {
    this.loading = true;
    this.turnoService.search(this.page, this.pageSize, this.filtroPagination).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.listaDeTurnos = data.content.content;
        this.loading = false;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.2Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.listaDeTurnos = [];
    this.loadTurnos()
  }
  pageChangedTurnosOcupados(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.itemsTurnosOcupados = this.listaTempTurnos.disponibilidad.slice(startItem, endItem);
  }

  //1.3 Refresh lista
  refresh() {
    this.listaDeTurnos = [];
    this.selectedSesiones = [];
    //Paginacion
    this.resetFiltro();
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadTurnos();
    this.selectSala = null;
  }

  //1.4 Busqueda Personalizada
  buscador() {
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;
    this.loadTurnos();
  }

  //1.5 Limpiar campos de filtro
  resetFiltro() {
    this.filtroPagination = {
      fecha: new Date(),
      profesionalId: null,
      findAll:true,
      clienteId: null,
      completado: false,
      habitacionId: null
    }
  }

  //Retornar Observacion por sesion
  observaciones(data) {
    var total=null;
    var total2="Sin observaciones"
    if(data!=undefined){
      data.forEach(x => {
        if (x.pregunta == "Observaciones:") {
          total = x.respuesta;
        }
      });
      if(total==null){
        return total2;
      }else{
        return total;
      }
    }else{
      return total2;
    }
  };

}
