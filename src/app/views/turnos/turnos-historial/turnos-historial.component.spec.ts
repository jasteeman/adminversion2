import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnosHistorialComponent } from './turnos-historial.component';

describe('TurnosHistorialComponent', () => {
  let component: TurnosHistorialComponent;
  let fixture: ComponentFixture<TurnosHistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnosHistorialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnosHistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
