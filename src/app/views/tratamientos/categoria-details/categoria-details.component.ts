import { Component, OnInit, TemplateRef } from '@angular/core';
import { first } from 'rxjs/operators';
import { TratamientosService } from 'src/app/services/tratamientos.service';
import { SalasService } from 'src/app/services/salas.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-categoria-details',
  templateUrl: './categoria-details.component.html',
  styleUrls: ['./categoria-details.component.scss']
})
export class CategoriaDetailsComponent implements OnInit {
  id: number;
  setClickedRow: Function;
  sub: any;
  editMode = false;
  categoria = { id: null, tipo: null, nombre: null, tratamientos: [] }
  pregunta = null
  tempTratamiento = { nombre: "",sesiones: null,precio: null,habitaciones: []}
  load = false;
  tempProducto;
  index = null
  salas: [];
  selectedRow = [];
  //  Modal
  config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;
  constructor(private salasService: SalasService, private modalService: BsModalService, private route: ActivatedRoute, private tratamientoService: TratamientosService, private toastr: ToastrService, private router: Router) {
    this.setClickedRow = function (index) {
      if (this.selectedRow.indexOf(index) > -1) {
        this.selectedRow.splice(this.selectedRow.indexOf(index), 1);
      }
      else {
        this.selectedRow.push(index);
      }
    }
   }

  ngOnInit() {
    this.editMode = false;
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.loadsalas();
    this.loadCategoria();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  // 1--Traigo las salas
  loadsalas() {
    this.salasService.getSalas(0, 100).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.salas = data.content.content;
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.1--Traigo el cliente 
  loadCategoria() {
    this.tratamientoService.getFullCategoriaTratamiento(this.id).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.categoria = data.content;
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if (error.status == 401) {
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  //1.2--Habilitar y desabilitar form
  unlockEditMode() {
    this.editMode = true;
  }
  lockEditMode() {
    this.editMode = false;
  }

  // 1.3 Actualizar categoria
  actualizarCategoria() {
    this.load = true
    // stop here if form is invalid
    if (this.categoria.nombre == null) {
      this.load = false;
      this.toastr.info("Campos incompletos. Complete y vuelve a intentar");
      return;
    }
    if (navigator.onLine) {
      //Guardo
      this.tratamientoService.saveOrUpdateCategoria(this.categoria).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Categoria actualizada con éxito');
          this.load = false;
          this.editMode = false;
          this.loadCategoria()
        } else {
          console.log(data)
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  // 1.4 Eliminar categoria
  deletecategoria() {
    this.load = true
    if (navigator.onLine) {
      //Guardo
      this.tratamientoService.deleteCategoria(this.tempProducto).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('categoria eliminado con éxito');
          this.load = false;
          this.editMode = false;
          this.loadCategoria()
          this.modalRef.hide()
        } else {
          console.log(data)
          this.modalRef.hide()
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.modalRef.hide()
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

  // 1.5 Actualizar tratamiento
  actualizarTratamiento() {
    this.load = true
    // stop here if form is invalid
    if (this.tempTratamiento.nombre == null) {
      this.load = false;
      this.toastr.info("Nombre vacio. Complete y vuelve a intentar");
      return;
    }
    if (this.tempTratamiento.precio == null) {
      this.load = false;
      this.toastr.info("Precio vacio. Complete y vuelve a intentar");
      return;
    }
    if (this.tempTratamiento.sesiones == null) {
      this.load = false;
      this.toastr.info("Cantidad de sesiones vacias. Complete y vuelve a intentar");
      return;
    }
    if(this.selectedRow.length>0){
      this.tempTratamiento.habitaciones=this.selectedRow;
    }
    if(this.selectedRow.length==0&&this.tempTratamiento.habitaciones.length==0){
      this.load = false;
      this.toastr.info("Debe seleccionar una sala para continuar");
      return;
    }
    if (navigator.onLine) {
      //Guardo
      this.tratamientoService.saveOrUpdateTratamiento(this.categoria.id, this.tempTratamiento).subscribe(data => {
        if (data.type == "OK") {
          this.toastr.success('Tratamiento actualizado con éxito');
          this.load = false;
          this.editMode = false;
          this.selectedRow=[]
          this.tempTratamiento = { nombre: "",sesiones: null,precio: null,habitaciones: []}
          this.modalRef.hide()
          this.loadCategoria()
        } else {
          console.log(data)
          this.modalRef.hide()
          this.load = false;
          this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
          // mostrar error
        }
      })
    } else {
      this.load = false;
      this.modalRef.hide()
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }
  }

    // 1.4 Eliminar tratamiento
    deleteTratamiento() {
      this.load = true
      if (navigator.onLine) {
        //Guardo
        this.tratamientoService.deleteTratamiento(this.tempProducto).subscribe(data => {
          if (data.type == "OK") {
            this.toastr.success('tratamiento eliminado con éxito');
            this.load = false;
            this.editMode = false;
            this.loadCategoria()
            this.modalRef.hide()
          } else {
            console.log(data)
            this.modalRef.hide()
            this.load = false;
            this.toastr.info("ERROR N°" + data.errorNumber + ":" + data.message);
            // mostrar error
          }
        })
      } else {
        this.modalRef.hide()
        this.load = false;
        this.toastr.info("Sin Conexion a internet. Intente nuevamente");
      }
    }

  //Operaciones de tablas
  cargarTabla() {
    if (this.pregunta == null) {
      this.toastr.error("Debe completar el campo pregunta para continuar")
      return
    }
    this.pregunta = null
  }
  eliminarItemTabla() {
    for (let i = 0; i < this.categoria.tratamientos.length; ++i) {
      if (this.categoria.tratamientos[i].id === this.tempProducto) {
        this.categoria.tratamientos.splice(i, 1);
      }
    }
    this.modalRef.hide()
  }

  //Modals
  openModal(template: TemplateRef<any>, data) {
    this.tempProducto = data
    this.modalRef = this.modalService.show(template,this.config);
  }
  openModalEdit(template: TemplateRef<any>, data) {
    this.tempTratamiento = data
    this.modalRef = this.modalService.show(template,this.config);
  }
  openModalGuardar(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,this.config);
  }
  openModalCreateTratamiento(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,this.config);
  }
  limpiarModal(){
    this.tempTratamiento = { nombre: "",sesiones: null,precio: null,habitaciones: []}
    this.tempProducto=null;
    this.modalRef.hide()
  }


  //Guardo la plantilla
  createTemplate() {
    this.load = true

    // stop here if form is invalid
    if (this.categoria.nombre == null || this.categoria.tipo == null) {
      this.load = false;
      this.toastr.info("Campos incompletos. Complete y vuelve a intentar");
      this.modalRef.hide()
      return;
    }
    if (navigator.onLine) {
      //Guardo
      // this.Se.saveOrUpdatecategoria(this.categoria).subscribe(data=>{
      //    if (data.type == "OK") {
      //      this.toastr.success('Plantilla actualizada con éxito');
      //      this.load=false;
      //      this.modalRef.hide()
      //      this.router.navigate(['/panel/tratamientos/list/']);
      //    } else {
      //      console.log(data)
      //      this.load=false;
      //      this.modalRef.hide()
      //     this.toastr.info("ERROR N°"+data.errorNumber+":"+data.message);
      //     // mostrar error
      //   }
      // })
    } else {
      this.modalRef.hide()
      this.load = false;
      this.toastr.info("Sin Conexion a internet. Intente nuevamente");
    }

  }

}
