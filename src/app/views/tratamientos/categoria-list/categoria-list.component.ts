import { Component, OnInit, TemplateRef,Input, Output } from '@angular/core';
import { TratamientosService } from 'src/app/services/tratamientos.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router} from '@angular/router';

@Component({
  selector: 'app-categoria-list',
  templateUrl: './categoria-list.component.html',
  styleUrls: ['./categoria-list.component.scss']
})
export class CategoriaListComponent implements OnInit {
 // Variables
 loading: Boolean;
 abrirForm: Boolean;
 tratamientoId: any;
 tratamientos = [];
 searchText: Boolean;
 tratamiento={nombre:null}
load=false;
 //Paginacion
 items = [];
 page = 0;
 pageSize = 20;
 totalElements = 0;
 totalPages = 0;

 //  Modal
 config = {
    ignoreBackdropClick: true
  };                                 
 modalRef: BsModalRef;

 constructor(private route: Router,private tratamientoService: TratamientosService, private toastr: ToastrService, private modalService: BsModalService) {
  }
  ngOnInit() {
    this.loadTratamientos();
    this.abrirForm = false
    
  }

  //1.1---Traer lista
  loadTratamientos() {
    this.loading = true;
    this.tratamientoService.getCategorias(this.page, this.pageSize).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.loading = false;
        this.tratamientos = data.content.content;
        this.totalElements = data.content.totalElements
        this.totalPages = data.content.totalPages
      } else {
        // mostrar error
      }
    }, error => {
      // mostrar error
      if(error.status==401){
        this.toastr.info("ERROR N°" + error.status + ". No tiene permisos para ingresar a esta sección");
      }
    });
  }

  // 1.2 Paginacion
  pageChanged(event: any): void {
    this.page = event.page - 1;
    this.tratamientos = [];
    this.loadTratamientos()
  }

  //1.3 Refresh lista
  refresh() {
    this.tratamientos = [];
    //Paginacion
    this.items = [];
    this.page = 0;
    this.pageSize = 20;
    this.totalElements = 0;
    this.totalPages = 0;

    this.loadTratamientos();
  }



  //Caja de texto busqueda
  mostrar() {
    this.searchText = true
  }
  ocultar() {
    this.searchText = false;
  }

  // ********************************************************************************************************
  //1.4 BORRAR CLIENTE
  confirm(data) {
    this.tratamientoId = data
    this.abrirForm = true
  }
  volver() {
    this.modalRef.hide()
    this.abrirForm = false;
  }

  //Borrar id
  delete() {
    this.tratamientoService.deleteCategoria(this.tratamientoId).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.tratamientos = [];
        //Paginacion
        this.items = [];
        this.page = 0;
        this.pageSize = 20;
        this.totalElements = 0;
        this.totalPages = 0;
        this.loadTratamientos();
        this.abrirForm = false;
        this.toastr.success('Categoria eliminada con éxito');
        this.modalRef.hide()
      } else {
        // mostrar error
      }
    });
  }
    //Crear categoria
    createCategoria() {
      this.load=true
      // stop here if form is invalid
      if (this.tratamiento.nombre==null) {
        this.load=false;
        this.toastr.info("Campos incompletos. Complete y vuelve a intentar");
          return;
      }
      if(navigator.onLine){
      //Guardo
      this.tratamientoService.saveOrUpdateCategoria(this.tratamiento).subscribe(data=>{
        if (data.type == "OK") {
           this.toastr.success('Categoria creada con éxito');
           this.load=false;
           this.tratamientos = [];
           //Paginacion
           this.items = [];
           this.page = 0;
           this.pageSize = 20;
           this.totalElements = 0;
           this.totalPages = 0;
   
           this.loadTratamientos();
           this.abrirForm = false;
           this.modalRef.hide()
         } else {
           console.log(data)
           this.load=false;
           this.modalRef.hide()
          this.toastr.info("ERROR N°"+data.errorNumber+":"+data.message);
          // mostrar error
        }
      })
      }else{
        this.load=false;
        this.modalRef.hide()
        this.toastr.info("Sin Conexion a internet. Intente nuevamente");
      }
    }

  openModalDelete(template: TemplateRef<any>, data) {
    this.tratamientoId = data
    this.modalRef = this.modalService.show(template,this.config);
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,this.config);
  }

  Transportar(event: Event) {
    this.tratamientoService.getFullTratamiento(event).pipe(first()).subscribe(data => {
      if (data.type == "OK") {
        this.route.navigate(['/panel/tratamientos/details/'+data.content.transientCategoriaId]);
      }
    }
    )
 }

}
