export class Proveedores {
    id: Number;
    razonSocial: String;
    cuit: String;
    localidad:Date;
    provincia:String;
    telefono: String;
}
