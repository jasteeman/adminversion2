
export class Empresa {
    id: Number;
    cuit: String;
    concepto: String;
    nombre:String;
    telefono: String;
    codigoPostal:String;
    direccion:String;
    correo:String;
    ingresosBrutos:String;
    fechaInicioActividad:Date;
    sedeTimbrado:string;
    password:string;
    tipoMonedaPredeterminada:object;
    condicionFrenteAlIva:object;
}
