export class Productos {
        id: number;
        nombre: string;
        proveedor: object;
        marca: string;
        subMarca: string;
        codigoDeBarras: string;
        codigo: string;
        stock: number;
        precioCosto: number;
        iva: number;
        unidadMedida:string;
        logistica: number;
        precioCostoFinal: number;
        utilidad: number;
        precioVenta: number;
        precioVentaProfesional: number;
        fechaCreado: Date;
        fechaActualizado: Date;
        enabled: true;
        imgUrl:string
}
export class ProductoStock{
        nombre: string;
        marca: string;
        codigo: string;
        unidadMedida:string;
        codigoDeBarras: string;
        precioCosto: number;
        precioVenta: number;
        precioVentaProfesional:number;
        precioCostoFinal: null;
        iva: number;
        logistica: number;
        subMarca: string;
        proveedor: object;
        cantidad: number;
        utilidad: number;
}
export class productoVenta{
        nombre: string;
        marca: string;
        codigo: string;
        unidadMedida:string;
        codigoDeBarras: string;
        precioCosto: number;
        precioVenta: number;
        precioVentaProfesional:number;
        precioCostoFinal: null;
        iva: number;
        logistica: number;
        subMarca: string;
        proveedor: object;
        cantidad: number;
        utilidad: number;
        descuento:number;
}
