export class Cliente {
    id: Number;
    nombre: String;
    apellido: String;
    fechaNacimiento:Date;
    celular1:String;
    dni: String;
}
export class ClienteCreate {
    id: Number;
    nombre: String;
    apellido: String;
    fechaNacimiento:Date;
    celular1:String;
    dni: String;
    celular2:String;
    direccion:String;
    email:String;
    estadoCivil:String;
    hijos:BigInteger;
    localidad:String;
    provincia:String;
    profesion:String;
    whatsapp:String;
    condicionFrenteAlIva:object;
    tipoDocumento:object;
    tipoFactura:object;
}
