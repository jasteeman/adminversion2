export class Turnos {
    clienteTransient: Object;
    desde: Date;
    enabled: Boolean;
    habitacion: Object;
    hasta: Date;
    id: Number;
    profesional: Object;
    sesion: Object;
    tratamientoTransient: Object;
};
export class objetoTratamiento {
    sesionesTratamiento: Number;
    totalTratamiento: Number;
    costoTratamiento: Number;
    descuentoTratamiento: Number; 
}
