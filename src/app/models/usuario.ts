export class Usuario {
    id: Number;
    nombre: String;
    apellido: String;
    empresa:Object;
    puntoDeVenta:Object;
    enabled: Boolean;
    rol: String;
}
